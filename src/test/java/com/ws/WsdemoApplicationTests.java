package com.ws;

import com.ws.bean.People;
import com.ws.service.TestService;
import com.ws.util.security.AESUtil;
import com.ws.util.security.SecurityUtil;
import org.jasypt.encryption.StringEncryptor;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@SpringBootTest
public class WsdemoApplicationTests {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private TestService testService;

    @Test
    void contextLoads() {
    }

    @Test
    void test(){
        System.out.println("666");
    }

    @Test
    void testBase64(){
        String result = SecurityUtil.encodeByBase64("AAaaa我");
        System.out.println("======"+result);
        System.out.println("======"+SecurityUtil.decodeByBase64(result));
    }

    @Test
    void testBase641(){
        String result = SecurityUtil.encodeByBase64("88888888");
        System.out.println("======"+result);
        System.out.println("======"+SecurityUtil.decodeByBase64(result));
        System.out.println("======"+SecurityUtil.decodeByBase64("ODg4ODg4ODg=8794654981eww"));
    }

    @Test
    void testAES(){
        String key = "123456789abcdfgt";//必须16位
        System.out.println("---------加密---------");
        String aesResult = AESUtil.encrypt("88888888", key);
        System.out.println("aes加密结果:" + aesResult);
        System.out.println();

        System.out.println("---------解密---------");
        String decrypt = AESUtil.decrypt(aesResult, key);
        System.out.println("aes解密结果:" + decrypt);
    }

    @Test
    void testService(){
        System.out.println("======");
        testService.testService();
        System.out.println("======");
    }

    /**
     * 测试redis
     */
    @Test
    public void testRedis() {
        stringRedisTemplate.opsForValue().set("keyid", "111");
        System.out.println(stringRedisTemplate.opsForValue().get("keyid"));
    }

    /**
     * 测试存储对象，redis 需要对对象进行序列化，取出对象数据后比对，又要进行反序列化
     * 所以注册了 RedisTemplate ，专门处理这类情况
     */
    @Test
    public void testRedisObj() throws Exception {
        People people = new People();
        people.setUsername("aa");
        people.setPassword("123");
        ValueOperations<String, People> operations = redisTemplate.opsForValue();
        operations.set("com.neox", people);
        //设置超时时间为120秒
        operations.set("com.neo.f", people, 120, TimeUnit.SECONDS);
        Thread.sleep(1000);
        //redisTemplate.delete("com.neo.f");
        boolean exists = redisTemplate.hasKey("com.neo.f");
        if(exists){
            System.out.println("exists is true");
        }else{
            System.out.println("exists is false");
        }
        // Assert.assertEquals("aa", operations.get("com.neo.f").getUserName());
    }

    @Resource
    StringEncryptor encryptor;

    @Test
    public void encrypt() {
        String url = encryptor.encrypt("jdbc:mysql://127.0.0.1:3306/wsdemo?useUnicode=true&characterEncoding=utf-8&serverTimezone=Asia/Shanghai&useSSL=true&allowMultiQueries=true");
        String username = encryptor.encrypt("root");
        String pwd = encryptor.encrypt("123456");
        String redisPwd = encryptor.encrypt("88888888");
        System.out.println("url = " + url);
        System.out.println("username = " + username);
        System.out.println("pwd = " + pwd);
        System.out.println("redisPwd = " + redisPwd);
    }

    @Test
    public void decrypt() {
        String url = encryptor.decrypt("TW1ZZTK0uuu/yiAcbP9+3ZyCcPHpbGe8+UmtC+3a4CIbUgbrVT/u39YWjFBtkXGJXJa0rj6mZURxEufoKLqkAtGJqES/N8O7JP1PHa9FmCBsOK+kY0EZm8F4sDzQQJ72FxpRtXd4ka6FnBCS9U2Y+Iaacoi+HQkBLJWBr7Upv/m3ZyhPuNPenzbqu8QY5huNXlBjULvyFqW9D7tFiSILug==");
        String username = encryptor.decrypt("KjeTdKxc4nQk8gzO38gtuU5UHrS5TLNp");
        String pwd = encryptor.decrypt("1hAn1PLx+Qc/nzTPV2lFnZCXDwmBhSfb");
        String redisPwd = encryptor.decrypt("760n24bXba5GRSz0tK2mgsaZBfuUMfscp67/ZTO9RyQ=");
        System.out.println("url = " + url);
        System.out.println("username = " + username);
        System.out.println("pwd = " + pwd);
        System.out.println("redisPwd = " + redisPwd);

        System.out.println("test = " + encryptor.decrypt("FxgYrWO647A54LEn6ggBtGzZ19lDP3JU"));
        System.out.println("test = " + encryptor.decrypt("5CMGna5HwDLj/tWq02sJzJ1QyQtOdOgh"));
    }

}
