package com.ws.config;

import com.ws.bean.po.Schedule;
import com.ws.service.ScheduleService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.scheduling.support.CronTrigger;

import java.time.LocalDateTime;
import java.util.List;

@Configuration //1.主要用于标记配置类，兼备Component的效果。
@EnableScheduling // 2.开启定时任务，如果在XXXApplication类中添加了此注解这里可以不写
public class ScheduleConfig implements SchedulingConfigurer {

    @Autowired
    ScheduleService scheduleService; //注入从数据库查询cron的service

    /**
     * 执行定时任务.
     */
    @Override
    public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {

        List<Schedule> schedules = scheduleService.findAll();
        for(Schedule schedule : schedules){
            taskRegistrar.addTriggerTask(
                //1.添加任务内容(Runnable)
                () -> ScheduleConfig.startSchedule(),
                //2.设置执行周期(Trigger)
                triggerContext -> {
                    //2.1 从数据库获取执行周期
                    String cron = schedule.getCrons();
                    //2.2 合法性校验.
                    if (StringUtils.isEmpty(cron)) {
                        // Omitted Code ..
                    }
                    //2.3 返回执行周期(Date)
                    return new CronTrigger(cron).nextExecutionTime(triggerContext);
                }
            );
        }
    }
    public static void startSchedule(){
        System.out.println("执行动态定时任务: " + LocalDateTime.now().toLocalTime());
    }
}
