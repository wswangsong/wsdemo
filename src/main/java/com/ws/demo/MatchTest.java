package com.ws.demo;

import com.ws.util.ContentUtil;
import com.ws.util.PdfUtil;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MatchTest {

    public static void main(String[] a){
        String stringInfo = "{infoNum='10' EdwardBlog=‘http://hi.baidu.com/Edwardworld’ topicLength='20' titleShow='yes' EdwardMotto='I am a man,I am a true man!这是，中文。' /}";
        System.out.println("待处理的字符串：" + stringInfo);
        //所有特殊字符
        //String regex = "[`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]";
        //所有英文字符
        String regex = "[`~!@#$%^&*()+=|{}':;',\\[\\].<>/?]";
        //所有中文字符
        //String regex="[！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]";
        Pattern p = Pattern.compile(regex);//增加对应的标点


        //替换
        Matcher m = p.matcher(stringInfo);
        String first=m.replaceAll(""); //把英文标点符号替换成空，即去掉英文标点符号
        System.out.println("去掉英文标点符号后的字符串：" + first);


        //个数
        int count = 0;
        char c[] = stringInfo.toCharArray();
        for (int i = 0; i < c.length; i++) {
            Matcher matcher = p.matcher(String.valueOf(c[i]));
            if (matcher.matches()) {
                count++;
            }
        }
        System.out.println("包含英文标点符号的个数：" + count);
    }

    public static void getMatchContent(String content, String regex){
        Matcher matcher = Pattern.compile(regex).matcher(content);
        if(matcher.find()){
            System.out.println("========="+matcher.group());
        }
    }

    public static String replaceMatchContent(String content, String regex){
        Matcher matcher = Pattern.compile(regex).matcher(content);
        //替换
        return matcher.replaceAll(""); //把匹配的替换成空
    }

    @Test
    public void match0(){
        String content = "—1—\r\n" +
                "普通事项\r\n" +
                "1\r\n" +
                "国网重庆市电力公司党委会议纪要\r\n" +
                "(27)\r\n" +
                "国网重庆市电力公司办公室 2023 年 10 月 26 日\r\n" +
                "2023 年第 27 次党委（扩大）会纪要\r\n" +
                "10 月 23 日，党委书记、董事长周雄主持召开公司 2023 年第\r\n" +
                "27 次党委（扩大）会，传达学习贯彻习近平总书记在 9 月 27 日\r\n" +
                "中央政治局会议上的重要讲话精神，传达学习《中央企业合规管\r\n" +
                "理办法》《国家电网有限公司合规管理办法》，研究了公司本部“三\r\n" +
                "重一大”决策管理办法和 2个清单董事会授权决策方案修订、共\r\n" +
                "青团国网重庆市电力公司第二次代表大会筹备工作等。现将会议\r\n" +
                "纪要如下：\r\n" +
                "一、传达学习贯彻习近平总书记在 9月 27日中央政治局会议\r\n" +
                "—2—\r\n" +
                "上的重要讲话精神" +
                "";
        System.out.println(replaceMatchContent(replaceMatchContent(content, "—[\\d]+—\\r\\n"),"[\\d]+\\r\\n"));
    }

    @Test
    public void match1(){
        String content = "1.传达学习习近平总书记近期重要讲话精神和国资委有关会议精神（欧阳昌裕总法律顾问、办公室、研究室、发展部、财务部、营销部、科技部、互联网部、物资部、产业部、国际部、审计部、法律部、人资部、党建部、组织部、 宣传部、巡视办、体改办，驻公司纪检监察组参加）（1）习近平总书记在中央政治局第三十四次集体学习时的重要讲话精神（2）国资委党委习近平总书记全国国有企业党的建设工作会议重要讲话发表五周年学习座谈会精神" +
                "（3）国资委中央企业董事会建设研讨班精神2.听取公司党组 2021 年第二轮巡视综合情况汇报（巡 视办汇报，办公室、研究室、财务部、营销部、物资部、产 业部、国际部、审计部、人资部、党建部、组织部，驻公司 纪检监察组参加） 3.审议国网福建电力省管产业单位福建宏顺融资租赁 公司专业化整合事项（产业部汇报，欧阳昌裕总法律顾问、 办公室、研究室、发展部、财务部、审计部、法律部、人资 部、体改办，驻公司纪检监察组，国网福建电力、英大集团 参加）4.审议公司在英大证券试点推行职业经理人制度工作 方案（组织部汇报，办公室、研究室、人资部，驻公司纪检 监察组，英大集团参加） 5.审议公司部分单位外部董事选配事项（组织部汇报， 办公室，驻公司纪检监察组参加）";
        Pattern test_ptn = Pattern.compile(">(.+?)</pre>");
        Matcher m1 = test_ptn.matcher(content);
        while (m1.find()) {
            System.out.println("text=" + m1.group(1));
        }
    }

    @Test
    public void match2(){
        String content = "dfdds<pre class=\"java\">士大夫士大夫士大夫</pre>你好<pre class=\"koko\">门口没开门</pre>";
        Pattern test_ptn = Pattern.compile(">(.+?)</pre>");
        Matcher m1 = test_ptn.matcher(content);
        while (m1.find()) {
            System.out.println("====" + m1.group(1));
        }
    }

    @Test
    public void match3(){
        String timeRegex = "[0-9〇一二三四五六七八九十零]{4}年[0-9〇一二三四五六七八九十零]{1,2}月[0-9〇一二三四五六七八九十零]{1,3}日";
        System.out.println("2022年10月12日".matches(timeRegex));
        getMatchContent("放松放松二零二二年十一月二十九日发生发顺丰", timeRegex);
    }

    @Test
    public void match4(){
        String content = "ad2bs士大9士大夫士a大34夫b你好门a口b没开门";
        String pattern = "大([\\d{1,2}])";
        getMatchContent(content, pattern);
    }

    @Test
    public void match5(){
        String content = "ad2bs士大夫士大夫士a大夫b你好门a口b没开门";
        String pattern = "(a.*b?)";
        getMatchContent(content, pattern);
    }

    @Test
    public void match6(){
        String content = "ad2bs士大夫士a45大夫士a大夫b你士好门a口b没开门";
        String pattern = "大([\\s\\S]*?)士a";
        getMatchContent(content, pattern);
    }

    @Test
    public void match7(){
        String input = "议题1、 讨论了有关议题)2、的提案，而)议题3则被推迟到)下次议题10、会议提案)。";
        String start = "(\\d+)、";
        String end = ")".replaceAll("([,:()!.。、])","([$1])");
        String regex = start + "([\\s\\S]*?)" + end;

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(input);

        while (matcher.find()) {
            System.out.println(matcher.group());
        }
    }

    @Test
    public void match8(){
        String input = "议题一、 讨论了有关)议题二、的提案，而)议题三则被推迟到下次议题十一、会议提案)。";
        String start = "([一二三四五六七八九十零]+)、";
        String end = ")".replaceAll("([,:()!.。、])","([$1])");
        String regex = start + "([\\s\\S]*?)" + end;

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(input);

        while (matcher.find()) {
            System.out.println(matcher.group());
        }
    }

    @Test
    public void match9(){
        String content = "ad2bs（士大夫）士a45大夫议通知士a大夫b你士好门a口b没开门";
        String pattern = "[\\s\\S]+[议][通][知]";
        getMatchContent(content, pattern);
    }

    @Test
    public void match11(){
        String input = "This is the start of the text. The text continues here end. start省间开发end. And ends here. starthjkhjend。The rest of the text continues.";
        String start = "start";
        String end = "end";

        Pattern pattern = Pattern.compile(start + "([\\s\\S]*?)" + end);
        Matcher matcher = pattern.matcher(input);

        while (matcher.find()) {
            System.out.println(matcher.group(1));
        }
    }

    @Test
    public void match12(){
        String input = "This is the start: of the。) text. The text continues here end. start:省间。开发)end. And ends here. start:hj)khjend。The rest of the text continues.";
        String start = "start:";
        String end = ")".replaceAll("([,:()!.。、])","([$1])");

        String regex = start + "([\\s\\S]*?)" + end;
        System.out.println("regex="+regex);
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(input);

        while (matcher.find()) {
            System.out.println(matcher.group(1));
        }
    }

    @Test
    public void match13(){
        String input = "1一、审议公司 2021 年综合计划、预算调整及 2022 年总控目标安排（发展部、\n" +
                "财务部汇报，单业才总经理助理、赵庆波总经理助理、张宁副总工程师、张磊副总\n" +
                "工程师、周安春安全总监、王继业副总信息师、办公室、研究室、安监部、设备部、\n" +
                "营销部、科技部、基建部、互联网部、物资部、产业部、国际部、人资部、后勤部、\n" +
                "宣传部、特高压部、水新部、国调中心，驻公司纪检监察组，英大集团参加)\n" +
                "2.二、审议公司深化纪检监察体制改革实施方案（办公室、研究室、审计部、法律\n" +
                "部、人资部、党建部、组织部、宣传部、巡视办，驻公司纪检监察组参加)\n" +
                "请 相 关 部 门 、 机 构 、 单 位 于 10 月 29 日 19:00 前 将 报 名 回 执 发 至\n" +
                "sp-msyc@sgcc.com.cn（63413511）。有关会议材料请按照《国家电网有限公司总\n" +
                "部重要决策会议文件材料管理规定》，于 10 月 31 日 14:00 前送文印室。";
        String start = "n.";
        String end = ")".replaceAll("([,:()!.。、])","([$1])");
        if(start.contains("n")){
            start = start.replaceAll("n","(\\\\d+)");
        } else if(start.contains("N")){
            start = start.replaceAll("N","([一二三四五六七八九十零]+)");
        }

        //start = "(\\d+).";
        start = "([一二三四五六七八九十零]+)、审议公司";

        String regex = start + "([\\s\\S]*?)" + end;

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(input);

        while (matcher.find()) {
            System.out.println(matcher.group(1));
        }
    }

    @Test
    public void match14(){
        //String input = "1.议题一、审议公司 2021 年综合计划、预算调整及 2022 年总控目标安排（发展部、财务部汇报，单业才总经理助理、赵庆波总经理助理、张宁副总工程师、张磊副总工程师、周安春安全总监、王继业副总信息师、办公室、研究室、安监部、设备部、营销部、科技部、基建部、互联网部、物资部、产业部、国际部、人资部、后勤部、宣传部、特高压部、水新部、国调中心，驻公司纪检监察组，英大集团参加)2.议题二、审议公司深化纪检监察体制改革实施方案（办公室、研究室、审计部、法律部、人资部、党建部、组织部、宣传部、巡视办，驻公司纪检监察组参加)请 相 关 部 门 、 机 构 、 单 位 于 10 月 29 日 19:00 前 将 报 名 回 执 发 至sp-msyc@sgcc.com.cn（63413511）。有关会议材料请按照《国家电网有限公司总部重要决策会议文件材料管理规定》，于 10 月 31 日 14:00 前送文印室。";
        String input = ContentUtil.replaceBlank(ContentUtil.conversion(PdfUtil.getTextByPdfbox("D:\\opt\\文档\\会议通知纪要文档\\党组会议通知2021年11月1日.pdf")));
        //String start = "议题N、";
        String start = "n.";
        String end = ")";
        start = start.replaceAll("([,:()!.。、])","([$1])");
        end = end.replaceAll("([,:()!.。、])","([$1])");
        if(start.contains("n")){
            start = start.replaceAll("n","([0-9]+)");
        } else if(start.contains("N")){
            start = start.replaceAll("N","([零一二三四五六七八九十]+)");
        }

        //start = "(\\d+).";
        //start = "([一二三四五六七八九十零]+)、";

        String regex = start + "([\\s\\S]*?)" + end;

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(input);

        List<String> list = new ArrayList();
        while (matcher.find()) {
            System.out.println(matcher.group());
            list.add(matcher.group());
        }

        System.out.println("++++++++++++++++++++++++++++");

        for(String a : list){
            Matcher m1 = Pattern.compile(start).matcher(a);
            String na = m1.replaceAll("");
            System.out.println(na);
            Matcher m2 = Pattern.compile(end).matcher(na);
            System.out.println(m2.replaceAll(""));
        }
    }

    @Test
    public void match21(){
        String content = PdfUtil.getTextByTika("D:\\opt\\文档\\会议通知纪要文档\\党组会议通知2021年10月25日2.pdf");
        String input = ContentUtil.replaceBlank(content);

    }

    @Test
    public void match22(){
        //System.out.println(PdfUtil.getTextByPdfbox("D:\\opt\\pdf\\重庆电力公司\\07.2023年第27次党委（扩大）会纪要.pdf"));
        //System.out.println(PdfUtil.getLayoutTextByPdfbox("D:\\opt\\pdf\\重庆电力公司\\07.2023年第27次党委（扩大）会纪要.pdf"));
        String result = PdfUtil.getLayoutTextByPdfbox("D:\\opt\\pdf\\重庆电力公司\\07.2023年第27次党委（扩大）会纪要.pdf");
        String list = ContentUtil.replaceMatchContent(result, "—[ ]*[\\d]+[ ]*—[ ]*\\n");
        System.out.println(list);
//        String[] lines = result.split("\n[ ]+\n");
//        for(String line : lines){
//            if(line.matches("^[ ]{27}[\\S]{1}.*")){
//                System.out.println(line);
//            }
//			if(ContentUtil.isMatchContent(line, "([零一二三四五六七八九十]+)([、])")){
//				System.out.println(line);
//			}
//        }

    }
}
