package com.ws.demo.tess4j;

import com.recognition.software.jdeskew.ImageDeskew;
import net.sourceforge.tess4j.ITessAPI.TessPageIteratorLevel;
import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.ITesseract.RenderedFormat;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.Word;
import net.sourceforge.tess4j.util.ImageHelper;
import net.sourceforge.tess4j.util.LoggHelper;
import net.sourceforge.tess4j.util.Utils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.ResourceLoader;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Tess4JTest1 {

    private static final Logger logger = LoggerFactory.getLogger(new LoggHelper().toString());

    /**
     * 根据图片文件进行识别
     */
    @Test
    public void testDoOCR_File() throws Exception {
        ClassPathResource imageResource = new ClassPathResource("tessimages");
        File imageFile = new File(imageResource.getFile(), "ocr.png");

        ITesseract instance = new Tesseract();
        //设置语言库所在的文件夹位置，最好是绝对的，不然加载不到就直接报错了
        instance.setDatapath("D:\\opt\\tessdata");
        //设置使用的语言库类型：chi_sim 中文简体
        instance.setLanguage("chi_sim");

        String result = instance.doOCR(imageFile);
        logger.info(result);
    }

    /**
     * 根据图片流进行识别
     */
    @Test
    public void testDoOCR_BufferedImage() throws Exception {
        ClassPathResource imageResource = new ClassPathResource("tessimages");
        File imageFile = new File(imageResource.getFile(), "111.png");
        BufferedImage bi = ImageIO.read(imageFile);

        ITesseract instance = new Tesseract();
        //设置语言库所在的文件夹位置，最好是绝对的，不然加载不到就直接报错了
        instance.setDatapath("D:\\opt\\tessdata");
        //设置语言,默认是英文（识别字母和数字），如果要识别中文(数字 + 中文），需要制定语言包
        instance.setLanguage("chi_sim");

        String result = instance.doOCR(bi);
        logger.info(result);
    }

    /**
     * 根据图片流进行识别
     */
    @Test
    public void testDoOCR_BufferedImage1() throws Exception {
        String tempImage = "D:\\opt\\img\\333918730277800.png";
        File file = new File(tempImage);
        BufferedImage bi = ImageIO.read(file);

        ITesseract instance = new Tesseract();
        //设置语言库所在的文件夹位置，最好是绝对的，不然加载不到就直接报错了
        instance.setDatapath("D:\\opt\\tessdata");
        //设置使用的语言库类型：chi_sim 中文简体
        instance.setLanguage("chi_sim");

        String result = instance.doOCR(bi);
        System.out.println(result);
    }

}
