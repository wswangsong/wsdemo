package com.ws.demo.tess4j;


import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageTree;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.apache.pdfbox.text.PDFTextStripper;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;

/**
 * pdf文件OCR识别功能
 * @author
 */
public class Tess4JTest2 {

    @Test
    public void testPdfOCR() throws Exception {
        String dir = "D:\\opt\\pdf";
        //要读取的pdf文档位置
        String path = dir+"/纪要-党委会纪要〔2023〕11号.pdf";
        File file = new File(path);
        //加载pdf文件，创建PDDocument对象
        PDDocument document = PDDocument.load(file);

        //读取pdf图片信息
        PDFRenderer renderer = new PDFRenderer(document);
        PDPageTree list = document.getPages();
        for(int i=0; i<list.getCount(); i++){
            BufferedImage image = renderer.renderImage(i);
            if(image != null){
                executeOCR(image);
            }
        }
    }

    // 执行OCR识别
    private void executeOCR(BufferedImage targetImage) {
        try {
            String tempImage = "D:\\opt\\img\\" + System.nanoTime() + ".png";
            File tempFile = new File(tempImage);
            if (tempFile == null) {
                tempFile.mkdirs();
            }
            ImageIO.write(targetImage, "jpg", tempFile);
            File file = new File(tempImage);
            BufferedImage bi = ImageIO.read(file);

            ITesseract instance = new Tesseract();
            // 设置语言库位置
            instance.setDatapath("D:\\opt\\tessdata");
            // 设置语言,默认是英文（识别字母和数字），如果要识别中文(数字 + 中文），需要制定语言包
            instance.setLanguage("chi_sim");

            String result = instance.doOCR(bi);
            System.out.println(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
