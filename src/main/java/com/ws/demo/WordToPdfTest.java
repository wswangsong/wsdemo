package com.ws.demo;

import com.documents4j.api.DocumentType;
import com.documents4j.api.IConverter;
import com.documents4j.job.LocalConverter;
import com.ws.util.WordUtil;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class WordToPdfTest {

    @Test
    public void wordToPdf1() throws TemplateException, IOException {
        //ClassPathResource resource = new ClassPathResource("static/docx4j/业务申请单.docx");
        ClassPathResource resource = new ClassPathResource("static/word/测试1689230743660.docx");
        InputStream wordIs = resource.getInputStream();
        String pdfPath = "E:" + File.separator + "测试"+System.currentTimeMillis()+".pdf";
        OutputStream pdfOs = new FileOutputStream(pdfPath);
        //WordUtil.wordToPdfByDocx4j(wordIs, pdfOs);
        WordUtil.wordToPdfByDocuments4j(wordIs, pdfOs);
        //WordUtil.wordToPdfByPoi(wordIs, pdfOs);
        pdfOs.close();
        wordIs.close();
    }
}

