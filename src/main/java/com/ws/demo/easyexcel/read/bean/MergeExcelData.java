package com.ws.demo.easyexcel.read.bean;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MergeExcelData {
    @ExcelIgnore
    private Integer lineNo;

    @ExcelProperty(value = "学生姓名", index = 0)
    private String name;
    @ExcelProperty(value = "年龄", index = 1)
    private int age;
    @ExcelProperty(value = "性别", index = 2)
    private String gender;

    @ExcelProperty(value = {"课程", "课程名称"}, index = 3)
    private String courseName;

    @ExcelProperty(value = {"课程", "分数"}, index = 4)
    private double score;
}
