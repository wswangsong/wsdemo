package com.ws.demo.tika;

import net.sourceforge.tess4j.util.LoggHelper;
import org.apache.tika.Tika;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.pdf.PDFParser;
import org.apache.tika.sax.BodyContentHandler;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;

import java.io.InputStream;

public class TikaDemo {

    private static final Logger logger = LoggerFactory.getLogger(new LoggHelper().toString());

    /**
     * 解析pdf文件方式一
     */
    @Test
    public void analysisPdf1() throws Exception {
        ClassPathResource resource = new ClassPathResource("static/pdf/三重一大系统对接协同办公系统接口文档.pdf");
        InputStream inputStream = resource.getInputStream();

        Tika tika = new Tika();
        tika.setMaxStringLength(-1);
        String filecontent = tika.parseToString(inputStream);
        logger.info("Extracted Content:" + filecontent);
    }

    /**
     * 解析pdf文件方式二
     */
    @Test
    public void analysisPdf2() throws Exception {
        ClassPathResource resource = new ClassPathResource("static/pdf/三重一大系统对接协同办公系统接口文档.pdf");
        InputStream inputStream = resource.getInputStream();

        BodyContentHandler handler = new BodyContentHandler();
        //元数据对象
        Metadata metadata = new Metadata();
        ParseContext parseContext = new ParseContext();

        PDFParser pdfParser = new PDFParser();
        pdfParser.parse(inputStream, handler, metadata, parseContext);

        logger.info("文件属性信息：");
        for(String name: metadata.names()){
            logger.info(name+":"+metadata.get(name));
        }
        logger.info("pdf文件内容：");
        logger.info(handler.toString());
    }

    /**
     * 解析txt文件
     */
    @Test
    public void analysisTxt() throws Exception {
    	
    	ClassPathResource resource = new ClassPathResource("static/txt/test.txt");
        InputStream stream = resource.getInputStream();
        
//        Parser parser = new AutoDetectParser();
//        ContentHandler handler = new BodyContentHandler();
//        Metadata metadata = new Metadata();
//        ParseContext context = new ParseContext();
//
//        parser.parse(stream, handler, metadata, context);
//        
//        String content = handler.toString();
//        
//        System.out.println("Contents of the document:" + content);
//        System.out.println("Metadata of the document:");
//        String[] metadataNames = metadata.names();
//      
//        for(String name : metadataNames) {
//        	System.out.println(name + ": " + metadata.get(name));
//        }
        
        Tika tika = new Tika();
        String content = tika.parseToString(stream);
        System.out.println("Extracted Content: " + content);
    }

}
