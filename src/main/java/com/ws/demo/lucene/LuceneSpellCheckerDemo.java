package com.ws.demo.lucene;

import org.apache.lucene.search.spell.LevensteinDistance;

public class LuceneSpellCheckerDemo {

    public static void main(String[] args) {

        LevensteinDistance levensteinAlgorithm = new LevensteinDistance();

        System.out.println("結果①:" + levensteinAlgorithm.getDistance("resolution", "revolution"));
        System.out.println("結果②:" + levensteinAlgorithm.getDistance("take", "sake"));
        System.out.println("結果③:" + levensteinAlgorithm.getDistance("teacher", "teach"));
        System.out.println("結果④:" + levensteinAlgorithm.getDistance("let it go", "let's and go"));
        System.out.println("結果⑤:" + levensteinAlgorithm.getDistance("mountaingorilla", "fish"));

    }
}
