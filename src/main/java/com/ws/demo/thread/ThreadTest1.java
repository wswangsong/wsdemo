package com.ws.demo.thread;

public class ThreadTest1 extends Thread{
    private int c = 10;
    private int titcket = 100;
    Object obj = new Object();

    public ThreadTest1(String name){
        super(name);
    }

    @Override
    public void run() {
        System.out.println("this："+this);
        System.out.println("启动一个线程，线程名称为："+Thread.currentThread().getName());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        int a = 1;
        a = a + 10;
        c = c + a;
        System.out.println("a="+a+"-----c="+c);
        while(true){
            synchronized (ThreadTest1.class){
                if(titcket>0){
                    titcket--;
                    System.out.println("titcket="+titcket);
                }
            }
        }

    }

    public static void main(String[] args) {
        //for (int i=0; i<3;i++){
            //启动一个线程
            new ThreadTest1("我是子线程001").start();
            new ThreadTest1("我是子线程002").start();
            new ThreadTest1("我是子线程003").start();
        //}
    }
}
