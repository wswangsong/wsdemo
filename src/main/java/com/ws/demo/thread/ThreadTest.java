package com.ws.demo.thread;

public class ThreadTest extends Thread{

    public ThreadTest(String name){
        super(name);
    }

    @Override
    public void run() {
        System.out.println("启动一个子线程，线程名称为："+Thread.currentThread().getName());

        //直接调用方法
        runSub();
    }

    public void runSub(){
        System.out.println("在run方法里调用，线程名称为："+Thread.currentThread().getName());

        //直接调用方法
        runSubToSub();
    }

    public void runSubToSub(){
        System.out.println("在 runSub里面被调用，线程名称为："+Thread.currentThread().getName());
    }

    public static void mainSub(){
        System.out.println("直接在主方法里被调用，线程名称为："+Thread.currentThread().getName());
    }

    public static void main(String[] args) {
        System.out.println("当前主线程名称为："+Thread.currentThread().getName());

        //直接调用方法
        mainSub();

        //启动一个线程
        new ThreadTest("我是子线程001").start();
    }
}
