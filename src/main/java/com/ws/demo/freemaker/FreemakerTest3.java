package com.ws.demo.freemaker;

import com.ws.util.date.DateUtil;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FreemakerTest3 {
    public static void main(String[] args) throws TemplateException, IOException {

        ClassPathResource imageResource = new ClassPathResource("templates/ftl");

        //1.创建配置类
        Configuration configuration = new Configuration(Configuration.getVersion());
        //2.设置模板所在的目录
        configuration.setDirectoryForTemplateLoading(imageResource.getFile());
        //configuration.setDirectoryForTemplateLoading(new File("D:\\workspaces\\idea\\demo\\src\\main\\resources"));
        //3.设置字符集
        configuration.setDefaultEncoding("utf-8");
        //4.加载模板;
        Template template = configuration.getTemplate("meetingNoticeDS.ftl");
        //5.创建数据模型
        Map map = new HashMap();
        map.put("meetingNoticeName", "名称");
        map.put("yearNum","2021");
        map.put("orderNum","101");
        map.put("meetingTime","2021-10-25 08:00:00");
        map.put("meetingPlace","202室");
        map.put("moderator","张飞");
        map.put("attendees","刘备");
        map.put("leave","");
        map.put("attendances","曹操，张辽");
        map.put("meetingRecord","");
        map.put("time", DateUtil.toChineseDate("2021-10-25"));

        List<Map<String,String>> userList = new ArrayList<Map<String,String>>();
        Map<String,String> map1 = new HashMap<String,String>();
        map1.put("num","1");
        map1.put("name","辐射防护士大夫士大夫了");
        map1.put("approve","曹操");
        map1.put("attendances","张辽，荀彧");
        map1.put("approveOrg","曹仁");
        map1.put("time","1993-03-03");
        Map<String,String> map2 = new HashMap<String,String>();
        map2.put("num","2");
        map2.put("name","范德萨范德萨范德萨");
        map2.put("approve","孙权");
        map2.put("attendances","周瑜，鲁肃，吕蒙");
        map2.put("approveOrg","陆逊，周泰，黄盖");
        map2.put("time","1994-04-04");
        userList.add(map1);
        userList.add(map2);
        map.put("subjectList", userList);
        //6.创建 Writer 对象
        Writer out = new FileWriter(new File("E:\\3.doc"));
        //7.
        template.process(map, out);
        //8.关闭 Writer 对象
        out.close();
    }
}

