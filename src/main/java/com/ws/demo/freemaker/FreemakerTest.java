package com.ws.demo.freemaker;


import com.ws.bean.UserInfo;
import com.ws.util.ImageUtil;
import com.ws.util.WordUtil;
import org.springframework.core.io.ClassPathResource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FreemakerTest {

    public static void main(String[] args) throws Exception {

        /**
         * 自动生成Word文档
         * 注意：生成的文档的后缀名需要为doc，而不能为docx，否则生成的Word文档会出错
         */
        WordUtil.generateWord(getWordData(), "E:/People.doc");
    }

    /**
     * 获取生成Word文档所需要的数据
     */
    private static Map<String, Object> getWordData() throws Exception{
        /*
         * 创建一个Map对象，将Word文档需要的数据都保存到该Map对象中
         */
        Map<String, Object> dataMap = new HashMap<>();

        /*
         * 直接在map里保存一个用户的各项信息
         * 该用户信息用于Word文档中FreeMarker普通文本处理
         * 模板文档占位符${name}中的name即指定使用这里的name属性的值"用户1"替换
         */
        dataMap.put("name", "用户1");
        dataMap.put("sex", "男");
        dataMap.put("birthday", "1991-01-01");

        /**
         * 将用户的各项信息封装成对象，然后将对象保存在map中，
         * 该用户对象用于Word文档中FreeMarker表格和图片处理
         * 模板文档占位符${userObj.name}中的userObj即指定使用这里的userObj属性的值(即user2对象)替换
         */
        UserInfo user2 = new UserInfo();
        user2.setName("用户2");
        user2.setSex("女");
        user2.setBirthday("1992-02-02");
        // 使用FreeMarker在Word文档中生成图片时，需要将图片的内容转换成Base64编码的字符串
        ClassPathResource imageResource = new ClassPathResource("static/1.jpg");
        user2.setPhoto(ImageUtil.getImageBase64String(imageResource.getFile()));
        //user2.setPhoto(ImageUtil.getImageBase64String("E:/1.jpg"));
        dataMap.put("userObj", user2);

        /*
         * 将多个用户对象封装成List集合，然后将集合保存在map中
         * 该用户集合用于Word文档中FreeMarker表单处理
         * 模板文档中使用<#list userList as user>循环遍历集合，即指定使用这里的userList属性的值(即userList集合)替换
         */
        List<UserInfo> userList = new ArrayList<>();
        UserInfo user3 = new UserInfo();
        user3.setName("用户3");
        user3.setSex("男");
        user3.setBirthday("1993-03-03");
        UserInfo user4 = new UserInfo();
        user4.setName("用户4");
        user4.setSex("女");
        user4.setBirthday("1994-04-04");
        userList.add(user3);
        userList.add(user4);
        dataMap.put("userList", userList);

        return dataMap;
    }

}

