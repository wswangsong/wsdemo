package com.ws.demo.freemaker;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.core.io.ClassPathResource;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FreemakerTest1 {
    public static void main(String[] args) throws TemplateException, IOException {
        ClassPathResource imageResource = new ClassPathResource("templates/ftl");

        //1.创建配置类
        Configuration configuration = new Configuration(Configuration.getVersion());
        //2.设置模板所在的目录
        configuration.setDirectoryForTemplateLoading(imageResource.getFile());
        //configuration.setDirectoryForTemplateLoading(new File("D:\\workspaces\\idea\\demo\\src\\main\\resources"));
        //3.设置字符集
        configuration.setDefaultEncoding("utf-8");
        //4.加载模板;
        Template template = configuration.getTemplate("toWord.ftl");
        //5.创建数据模型
        Map map = new HashMap();
        map.put("name", "甄士隐 ");
        map.put("password","123");
        //6.创建 Writer 对象
        Writer out = new FileWriter(new File("E:\\1.doc"));
        //7.
        template.process(map, out);
        //8.关闭 Writer 对象
        out.close();
    }
}

