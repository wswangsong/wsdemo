package com.ws.demo.freemaker;

import com.ws.util.WordUtil;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.core.io.ClassPathResource;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class FreemakerTest4 {
    public static void main(String[] args) throws TemplateException, IOException {
        ClassPathResource imageResource = new ClassPathResource("templates/ftl");

        //1.创建配置类
        Configuration configuration = new Configuration(Configuration.getVersion());
        //2.设置模板所在的目录
        configuration.setDirectoryForTemplateLoading(imageResource.getFile());
        //configuration.setDirectoryForTemplateLoading(new File("D:\\workspaces\\idea\\demo\\src\\main\\resources"));
        //3.设置字符集
        configuration.setDefaultEncoding("utf-8");
        //4.加载模板;
        Template template = configuration.getTemplate("BusinessForm.ftl");
        //5.创建数据模型
        Map map = new HashMap();
        map.put("applyUser", "张三");//主申请人
        map.put("sex", "男");//性别
        map.put("applyDept", "财政部");//申请部门
        map.put("contract","18603272014");//联系方式
        map.put("applyUnit", "中电启明星");//单位名称
        map.put("idCard","131026199101234567");//身份证号
        map.put("depLeader", "李四");//业务部门责任人
        map.put("depLeaderMobile","13931668888");//联系电话
        map.put("depLeaderMail", "123@sgcc.com");//内网邮箱
        map.put("depLeaderAccount","张三");//门户帐号
        map.put("businessType", "3");//业务类型选择
        map.put("applyReason","工作需要");//业务申请原因（业务说明）
        String expireDate = "2023年7月14日";
        //String expireDate = "一个月";
        String expireDateFlag = null;
        switch (expireDate) {
            case "一个月":
                expireDateFlag = "1";
                expireDate = "      年   月    日";
                break;
            case "三个月":
                expireDateFlag = "2";
                expireDate = "      年   月    日";
                break;
            default :
                expireDateFlag = "3";
        }
        map.put("expireDateFlag", expireDateFlag);//账号有效期
        map.put("expireDate", expireDate);//账号有效期
        map.put("depOpinion","同意");//业务部门意见
        //6.创建 Writer 对象
        String wordPath = "E:\\1.docx4j";
        Writer out = new FileWriter(new File(wordPath));
        //7.
        template.process(map, out);
        //8.关闭 Writer 对象
        out.close();

        //9.word转pdf
        //String pdfPath = "E:/" + "测试"+System.currentTimeMillis()+".pdf";
        //WordUtil.wordToPdfByDocuments4j(wordPath, pdfPath);
    }
}

