package com.ws.demo.freemaker;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

public class FreemakerTest5 {
    public static void main(String[] args) throws TemplateException, IOException {
        ClassPathResource imageResource = new ClassPathResource("templates/ftl");

        //1.创建配置类
        Configuration configuration = new Configuration(Configuration.getVersion());
        //2.设置模板所在的目录
        configuration.setDirectoryForTemplateLoading(imageResource.getFile());
        //3.设置字符集
        configuration.setDefaultEncoding("utf-8");
        //4.加载模板;
        Template template = configuration.getTemplate("BusinessApplyForm.ftl");
        //5.创建数据模型
        Map map = new HashMap();
        map.put("name", "张三");//主申请人
        map.put("baseOrgName", "财政部");//申请部门
        map.put("unitName", "中电启明星");//单位名称
        //6.创建 Writer 对象
        String wordPath = "E:\\1.docx4j";
        Writer out = new FileWriter(new File(wordPath));
        //7.
        template.process(map, out);
        //8.关闭 Writer 对象
        out.close();

        //9.word转pdf
        //String pdfPath = "E:/" + "测试"+System.currentTimeMillis()+".pdf";
        //WordUtil.wordToPdfByDocuments4j(wordPath, pdfPath);
    }
}

