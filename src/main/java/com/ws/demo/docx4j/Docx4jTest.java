package com.ws.demo.docx4j;

import com.ws.util.WordUtil;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class Docx4jTest {

    @Test
    public void replaceVariable1() {
        try {
            //模板文件
            ClassPathResource resource = new ClassPathResource("templates/word/请假条_template.docx");
            InputStream is = resource.getInputStream();

            //生成的文件名
            String fileName = "测试"+System.currentTimeMillis() + ".docx";
            //生成的文件输出流
            OutputStream os = new FileOutputStream("D:"+ File.separator+"opt"+File.separator+fileName);

            //要替换的变量集合
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("reason", "疫情");
            map.put("startYear", "2019");
            map.put("startMonth", "1");
            map.put("startDate", "10");
            map.put("endYear", "2019");
            map.put("endMonth", "1");
            map.put("endDate", "15");
            map.put("countDays", "5");
            map.put("name", "小李");
            map.put("createDate", "2019-12-12");

            //根据模板文件替换变量
            WordUtil.replaceVariable(is, map, os);

            os.flush();
            is.close();
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void replaceVariable2() {
        try {
            //模板文件
            ClassPathResource resource = new ClassPathResource("templates/word/BusinessApplyForm.docx");
            InputStream is = resource.getInputStream();

            //生成的文件名
            String fileName = "测试"+System.currentTimeMillis() + ".docx";
            //生成的文件输出流
            OutputStream os = new FileOutputStream("D:"+ File.separator+"opt"+File.separator+fileName);

            //要替换的变量集合
            Map map = new HashMap();
            map.put("name", "张三");//主申请人
            map.put("baseOrgName", "财政部");//申请部门
            map.put("unitName", "中电启明星");//单位名称

            //根据模板文件替换变量
            WordUtil.replaceVariable(is, map, os);

            os.flush();
            is.close();
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
