package com.ws.demo.docx4j;

import org.docx4j.XmlUtils;
import org.docx4j.jaxb.Context;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;
import org.docx4j.wml.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * docx4j 模板变量字符工具类
 *
 * @author wangsong
 * @date 2020-02-18 17:21:36
 */
public final class Docx4jUtil {
    /**
     * cleanDocumentPart
     * @param documentPart
     */
    public static boolean cleanDocumentPart(MainDocumentPart documentPart) throws Exception {
        if (documentPart == null) {
            return false;
        }
        Document document = documentPart.getContents();
        String wmlTemplate = XmlUtils.marshaltoString(document, true, false, Context.jc);
        document = (Document) XmlUtils.unwrap(DocxVariableClearUtils.doCleanDocumentPart(wmlTemplate, Context.jc));
        documentPart.setContents(document);
        return true;
    }

    /**
     * 清扫 docx4j 模板变量字符,通常以${variable}形式
     * XXX: 主要在上传模板时处理一下, 后续
     */
    private static class DocxVariableClearUtils {
        /**
         * 去任意XML标签
         */
        private static final Pattern XML_PATTERN = Pattern.compile("<[^>]*>");

        private DocxVariableClearUtils() {
        }

        /**
         * start符号
         */
        private static final char PREFIX = '$';
        /**
         * 中包含
         */
        private static final char LEFT_BRACE = '{';
        /**
         * 结尾
         */
        private static final char RIGHT_BRACE = '}';

        /**
         * 未开始
         */
        private static final int NONE_START = -1;
        /**
         * 未开始
         */
        private static final int NONE_START_INDEX = -1;

        /**
         * 开始
         */
        private static final int PREFIX_STATUS = 1;
        /**
         * 左括号
         */
        private static final int LEFT_BRACE_STATUS = 2;
        /**
         * 右括号
         */
        private static final int RIGHT_BRACE_STATUS = 3;

        /**
         * doCleanDocumentPart
         *
         * @param wmlTemplate
         * @param jc
         * @return
         * @throws JAXBException
         */
        public static Object doCleanDocumentPart(String wmlTemplate, JAXBContext jc) throws JAXBException {
            System.out.println("======执行清除方法=====");
            // 进入变量块位置
            int curStatus = NONE_START;
            // 开始位置
            int keyStartIndex = NONE_START_INDEX;
            // 当前位置
            int curIndex = 0;
            char[] textCharacters = wmlTemplate.toCharArray();
            StringBuilder documentBuilder = new StringBuilder(textCharacters.length);
            documentBuilder.append(textCharacters);
            // 新文档
            StringBuilder newDocumentBuilder = new StringBuilder(textCharacters.length);
            // 最后一次写位置
            int lastWriteIndex = 0;
            for (char c : textCharacters) {
                switch (c) {
                    case PREFIX:
                        // TODO 不管其何状态直接修改指针,这也意味着变量名称里面不能有PREFIX
                        keyStartIndex = curIndex;
                        curStatus = PREFIX_STATUS;
                        break;
                    case LEFT_BRACE:
                        if (curStatus == PREFIX_STATUS) {
                            curStatus = LEFT_BRACE_STATUS;
                        }
                        break;
                    case RIGHT_BRACE:
                        if (curStatus == LEFT_BRACE_STATUS) {
                            // 接上之前的字符
                            newDocumentBuilder.append(documentBuilder.substring(lastWriteIndex, keyStartIndex));
                            // 结束位置
                            int keyEndIndex = curIndex + 1;
                            // 替换
                            String rawKey = documentBuilder.substring(keyStartIndex, keyEndIndex);
                            System.out.println("-----需要替换的标签:" + rawKey);
                            // 干掉多余标签
                            String mappingKey = XML_PATTERN.matcher(rawKey).replaceAll("");
                            System.out.println("-----替换完成的字符:" + mappingKey);

//                        if (!mappingKey.equals(rawKey)) {
//                            char[] rawKeyChars = rawKey.toCharArray();
//                            // 保留原格式
//                            StringBuilder rawStringBuilder = new StringBuilder(rawKey.length());
//                            // 去掉变量引用字符
//                            for (char rawChar : rawKeyChars) {
//                                if (rawChar == PREFIX || rawChar == LEFT_BRACE || rawChar == RIGHT_BRACE) {
//                                    continue;
//                                }
//                                rawStringBuilder.append(rawChar);
//                            }
//                            // FIXME 要求变量连在一起
//                            String variable = mappingKey.substring(2, mappingKey.length() - 1);
//                            System.out.println("-----替换完成的变量variable:"+variable);
//                            System.out.println("-----rawStringBuilder:"+rawStringBuilder);
//                            int variableStart = rawStringBuilder.indexOf(variable);
//                            if (variableStart > 0) {
//                                rawStringBuilder = rawStringBuilder.replace(variableStart, variableStart + variable.length(), mappingKey);
//                                System.out.println("-----最后rawStringBuilder:"+rawStringBuilder);
//                            }
//                            newDocumentBuilder.append(rawStringBuilder.toString());
//                        } else {
                            newDocumentBuilder.append(mappingKey);
//                        }
                            lastWriteIndex = keyEndIndex;

                            curStatus = NONE_START;
                            keyStartIndex = NONE_START_INDEX;
                        }
                    default:
                        break;
                }
                curIndex++;
            }
            // 余部
            if (lastWriteIndex < documentBuilder.length()) {
                newDocumentBuilder.append(documentBuilder.substring(lastWriteIndex));
            }
            return XmlUtils.unmarshalString(newDocumentBuilder.toString(), jc);
        }
    }
}
