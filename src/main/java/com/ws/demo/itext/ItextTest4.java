package com.ws.demo.itext;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.draw.LineSeparator;
import com.ws.bean.MeetingNoticeVo;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class ItextTest4 {

    public static void main(String[] args){
        MeetingNoticeVo mnv = new MeetingNoticeVo();
        List<Map<String,Object>> orgNameList = new ArrayList<Map<String,Object>>();
        createPdf(mnv, "", orgNameList, false, "666");
    }

    public static boolean createPdf(MeetingNoticeVo mnv, String outputPath, List<Map<String,Object>> orgNameList, boolean userlsSgcc, String provinceCodeByIsc){
        String meetingName = ""; String typeName = ""; String huizhiName = "";
        //SzydUsers user = (SzydUsers)Users.current();
        if ("DZ".equals(mnv.getType())) {
            //if (provinceCodeByIsc.equals(user.getProvinceId())) {
            if (provinceCodeByIsc.equals("666")) {
                meetingName = "中共国濠电网有限公司党组会及通知";
                typeName = "党组会通";
                huizhiName ="党组会议报名回执";
            } else {
                meetingName = ((Map) orgNameList.get(0)).get("orgName") + "党委会议通知";
                typeName ="党委会通";
                huizhiName ="党委会议报名回执";
            }
        } else if ("DS".equals(mnv.getType())) {
            meetingName ="董嘉会议通知";
            typeName ="董磐会通";
            huizhiName ="董事会议报名回执";
        } else if ("DSB".equals(mnv.getType())) {
            meetingName ="董嘉长办公会及通知";
            typeName ="董办会通";
            huizhiName ="董期长办公会议报名回执";
        } else if ("JL".equals(mnv.getType())) {
            meetingName = "总经理办公会及通知";
            typeName = "总办会通";
            huizhiName = "总经理办公会议报名回执";
        }

        Document document = new Document(PageSize.A4, 50.0F, 50.0F, 50.0F, 50.0F);
        FileOutputStream fos = null;
        boolean ok = true;
        try {
            fos = new FileOutputStream(outputPath);
            PdfWriter writer = PdfWriter.getInstance(document, fos);
            writer.setPageEvent(new PdfExport());
            document.open();

            Font titleFont = new Font (BaseFont.createFont("/ttf/simhei.ttf", "Identity-H", true), 25.0F);
            titleFont.setColor(("work".equals(mnv.getType())) ? BaseColor.BLUE : BaseColor.RED);
            Paragraph title = new Paragraph(meetingName, titleFont);
            title.setAlignment(1);
            document.add(title);
            document.add(new Paragraph("\n", new Font(BaseFont.createFont("/ttf/simhei.ttf", "Identity-H", false), 10.0F)));

            Paragraph bianhao = new Paragraph (typeName + " [" + mnv.getYearNum() + "] " + mnv.getOrderNum() + "号",new Font (BaseFont.createFont("/ttf/fangsong_GB2312.ttf", "Identity-H", true), 15.0F));
            bianhao.setPaddingTop(0F);
            bianhao.setAlignment(1);
            document.add(bianhao);


            LineSeparator lineSeparator = new LineSeparator(0.5F, 90.0F, BaseColor.RED, 1, -5.0F);
            document.add(lineSeparator);

            document.add(new Paragraph("\n", new Font(BaseFont.createFont("/ttf/simhei.ttf", "Identity-H", false), 10.0F)));
            PdfPTable table = new PdfPTable(new float[] { 15.0F, 150.0F });
            Font celllFont = new Font (BaseFont.createFont("/ttf/simhei.ttf", "Identity-H", true), 10.0F);
            Font cell2Font = new Font (BaseFont.createFont("/ttf/fangsong_GB2312.ttf", "Identity-H", true), 10.0F);
            for (int i = 0; i < 12; ++i) {
                String value = "";
                if (i == 0) value ="时间：";
                else if (i == 1) value = mnv.getMeetingTime();
                else if (i == 2) value ="地点：";
                else if (i == 3) value =mnv.getMeetingPlace();
                else if (i == 4) value ="主持：";
                else if (i == 5) value =mnv.getModerator();
                else if (i == 6) value ="出席：";
                else if (i == 7) value =mnv.getAttendUsers ();
                else if (i == 8) value ="列席：";
                else if (i == 9) value =mnv.getAttendancePersonne1();
                else if (i == 10) value="议题：";
                else if (i == 11) value="";

                PdfPCell cell = new PdfPCell();
                cell.addElement(new Paragraph(value, cell2Font));
                cell.setBorder(0);
                cell.setBorderWidth(0F);
                table.addCell(cell);
            }
            document.add(table);

            BaseFont bfChinese = BaseFont.createFont("/ttf/fangsong_GB2312.ttf", "Identity-H", false);
            Font nameFont = new Font (bfChinese, 12.0F, 1);
            Font detailFont = new Font(bfChinese, 12.0F, 0);

            List subjectList = mnv.getSubjectDetailsArray();
            Paragraph p = null;
            for (int i = 0; i < subjectList. size (); ++i) {
                p = new Paragraph ();
                p.setIndentationLeft(50.0F);
                p.setIndentationRight(50.0F);
                p.setFirstLineIndent(20.0F);
                Chunk chunk1 = new Chunk((i + 1) + "." + ((Map)subjectList.get(i)).get("subjectName"), nameFont);
                Chunk chunk2 = new Chunk(((Map)subjectList.get(i)).get("subjectDecails") + "", detailFont);
                p.add(chunk1);
                p.add(chunk2);
                document.add(p);
            }








//    document.add(new Paragraph ("\n", new Font (Base Font, crea teFont ('Vttf/simhei. "Identity-H", false),
//            if (userlsSgcc)
//10.0F)));
//    p = new Paragraph("请各部门、单位于"+ mnv.getSendMailTime() + "前将报名回执发至sp-msyc@sgcc.com.cn (66598151)・有关会议材料请按照0关于加强党组会' 总经度办公会等重要会议文件、材料保密辔璟的规定》,于"+ mnv.
//
//            ()+ "前送办公厅文印室・",detailFont);
//}
//    elselse
//            (
//                    p = new Paragraph("请各部门、单位于"+ mnv.getSendMailTiiae() + "前将报名回执发全会议组织联系人拆箱‘有关会议材料请按朦《关于加强党组会、总经璟办公会等童藜会议文件、材料保密管度的规定》,
//        ()+ "前送文印室.", deCailFont);
//        }
//        p.setIndenCationLeft(50.0F);
//        p.s e tlndencati onRi ght(50.0F);
//        p.setFirstLineIndent(20.0F);
//        document.add(p);



//        document.add(new Paragraph new Font (Base Font, crea teFont ('Vttf/simhei. ttf", "Identity-H", false), 10. 0F)));
//        if if (userlsSgcc)
//        p = new Paragraph("办 公 厅",detailFont);
//        elselse
//        p = new Paragraph("办 公室",detailFont);
//
//        p.setIndenCationLeft(370.0F);
//        document.add(p);
//
//        p = new Paragraph(mnv.getCreateNoticeTime(), detailFont);
//        p.setIndenCationLeft(360.0F);
//        document.add(p);
//
//        document.newPage();
//
//        Font timeFont = new Font(BaseFont.createFont('Vttf/simhei. "Identity-H", false), 18. 0F);
//        Paragraph time = new Paragraph("参 会 时 间",time Font);
//        time.setAlignment(1);
//        document.add(time);
//        document.add(new Paragraph new Font (Base Font, crea teFont ('Vttf/simhei. ttf", "Identity-H", false), 10. 0F)));
//
//        p = new Paragraph("请参会人员参朦以下时间参加会议,并提前至"+ mnv. getSiibjectPlace () + "等候.", deCailFont);
//        p.setIndenCationLeft(50.0F);
//        p.s e tlndencati onRi ght(5 0.0 F);
//        document.add(p);
//        String口 timeArr = mnv. getSiifajectTime ();
//        for (int for (int i = 0; i < timeArr.length; length; ++i) {
//        p = new Paragraph("第"+ (i + 1) + "坟题："+ tiiaeArr[i] + "; ", detailFont);
//        p.setIndenCationLefc(50. 0F);
//        p.s e tlndencati onRi ght(5 0.0 F);
//        document.add(p);
//        }
//
//        document.newPage();
//        Font huizhiFont = new Font (Base Font, area teFont ('Vttf/siiahei. ttf", "Identity-H", false), 18. 0F);
//        Paragraph huizhi = new Paragraph(huizhiName, huizhiFont);
//        huizhi. setAligniaent(1);
//        document, add(huizhi);
//
//
//        document, add (new Paragraph new Font (Base Font, crea teFont ('Vttf/simhei. ttf", "Identity-H", false), 10. 0F)));
//        + mnv.gecToUorkTime
//        PdfPTable hzTable = new PdfPTable(new float[] { 25.0F, 140.0F });
//        Font hzTab1eFont = new Font(BaseFont.createFont("/ttf/fangsong_GB2312.ttf", "Identity-H'
//        for (int i = 0; i < 12; ++i) {
//        ,true)f 10.0F);
//        String value =
//        if (i == 0) value ="会议名称";
//        else if (i == 1)
//        else if (i == 2)
//        else if (i == 3)
//        else if (i == 4)
//        else if (i == 5)
//        else if (i == 6)
//        else if (i == 7)
//        else if (i == 8)
//        else if (i == 9)
//        value = mnv.ge tRe c e ip CMe e tingName();
//        value ="部 ri\n (单位)
//        value =
//        value ="参会人员";
//        value =
//        value ="职 务";
//        value =
//        value ="主要负责人不参会期由";
//        value =
//        else if (i == 10) value ="经办人";
//        PdfPCell cell = new PdfPCell();
//        cell.setUseAscender(true);
//        cell.setVerticalAlignment(5);
//        cell.setFixedHeight(20.0F);
//        if ((i == 2) I I (i == 3)) cell.setFixedHeight(50.0F);
//        if ((i == 8) I I (i == 9)) cell.setFixedHeight(l00.0F);
//        if (i == 11) {
//        cell.setPadding(0F);
//        PdfPTable operatorTable = new PdfPTable(new float[] { 90.0F, 30.0F, 70.0F });
//        operatorTable.setPaddingTop(0F);
//        Paragraph element = new Paragraph(,r 电话",hzTableFont);
//        element.setAlignment(1);
//        PdfPCell phoneCell = new PdfPCell(element);
//        phoneCell.setFixedHeight(20.0F);
//        phoneCell.setUseAscender(true);
//        phoneCell.setVerticalAlignment(5);
//        phoneCell.s e tHo r i z ontalAli gnment(5);
//        PdfPCell celll = new PdfPCell。;
//        celll.setBorderUidth(0F);
//        operatorTable.addCell(celll);
//        operatorTable.addCell(phoneCell);
//        operatorTable.addCell(celll);
//        cell.addE1ement(op e r ato rTab1e);
//        } else {
//        Paragraph element = new Paragraph(value, hzTableFont);
//        element.setAlignment(1);
//        cell.addElement(element);
//        cell.setBorderWidth(0.5F);
//        cell.setUseAscender(true};
//        cell.setVerticalAligniaent(5);
//        cell.setHorizontalAlignment(1);
//        }
//        hzTable.addCell(cell);
//        }
//
//        document.add(hzTable};

            document.close();
            fos.close();


        } catch (IOException e) {
            ok = false;
        } catch (DocumentException e) {
            ok = false;
        } finally {
            if (fos != null)
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }

        return ok;
    }



    private static class PdfExport extends PdfPageEventHelper {
        public PdfTemplate tpl;
        public BaseFont bf;

        private PdfExport() {
            this.tpl = null;
            this.bf = null;
        }

        public void onEndPage(PdfWriter writer, Document document) {
            PdfContentByte cb = writer.getDirectContent();
            String text = writer.getPageNumber() + "";
            float textSize = this.bf.getWidthPoint(text, 9.0F);
            float textBase = document.bottom();

            cb.beginText();
            cb.setFontAndSize(this.bf, 9.0F);
            cb.setTextMatrix(300.0F, textBase);
            cb.showText(text);
            cb.endText();
            cb.addTemplate(this.tpl, document.left() + textSize, textBase);
        }

        public void onOpenDocument(PdfWriter writer, Document document){
            this.tpl = writer.getDirectContent().createTemplate(100.0F, 100.0F);
            try {
                this.bf = BaseFont.createFont("/ttf/simhei.ttf", "Identity-H", false);
            } catch (DocumentException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void onCloseDocument(PdfWriter writer, Document document){

        }
    }

}
