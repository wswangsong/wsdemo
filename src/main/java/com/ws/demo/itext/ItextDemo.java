package com.ws.demo.itext;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import org.junit.jupiter.api.Test;

import java.io.*;

public class ItextDemo {

    //生成PDF文档
    @Test
    public void createPdfDocument() {
        try {
            //用iText生成PDF文档需要5个步骤：
            //①建立com.itextpdf.text.Document对象的实例。
            Document document = new Document();
            //②建立一个书写器(Writer)与document对象关联，通过书写器(Writer)可以将文档写入到磁盘中。
            PdfWriter.getInstance(document,new FileOutputStream("D:\\opt\\pdf\\ITextTest.pdf"));
            //③打开文档。
            document.open();
            //④向文档中添加内容。
            document.add(new Paragraph("IText  Test"));
            //⑤关闭文档。
            document.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void createPdfDocument1() {
        try {
            Document document = new Document(PageSize.A4, 60, 60, 120, 80);
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("D:\\opt\\pdf\\111.pdf"));
            HeaderAndFooter event = new HeaderAndFooter();
            writer.setPageEvent(event);
            document.open();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

class HeaderAndFooter extends PdfPageEventHelper {

    @Override
    public void onEndPage(PdfWriter writer, Document document) {
        Rectangle rect = writer.getBoxSize("art");
        switch (writer.getPageNumber() % 2) {
            case 0:
                ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_RIGHT, new Phrase("even header"),
                        rect.getBorderWidthRight(), rect.getBorderWidthTop(), 0);
                break;
            case 1:
                ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER,
                        new Phrase(String.format("%d", writer.getPageNumber())), 300f, 62f, 0);
                break;
        }
    }
}