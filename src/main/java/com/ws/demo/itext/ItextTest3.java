package com.ws.demo.itext;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.draw.LineSeparator;
import com.ws.bean.MeetingNoticeVo;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ItextTest3 {

    public static void main(String[] args){
        MeetingNoticeVo mnv = new MeetingNoticeVo();
        mnv.setType("DS");
        mnv.setYearNum("2021");
        mnv.setOrderNum("12");
        mnv.setMeetingTime("2020-09-12");
        mnv.setMeetingPlace("302室");

        List<Map<String, Object>> subjectDetailsArray = new ArrayList<Map<String, Object>>();
        Map<String, Object> map1 = new HashMap<String, Object>();
        map1.put("subjectName","议题一");
        map1.put("subjectDecails","议题打开了的拉开了大领导卡");
        Map<String, Object> map2 = new HashMap<String, Object>();
        map2.put("subjectName","议题二");
        map2.put("subjectDecails","更丰富很舒服搜索时看到吗");
        subjectDetailsArray.add(map1);
        subjectDetailsArray.add(map2);
        mnv.setSubjectDetailsArray(subjectDetailsArray);

        createPdf(mnv, "e:\\testPdf2.pdf");
    }

    public static boolean createPdf(MeetingNoticeVo mnv, String outputPath){
        String meetingName = "";
        String typeName = "";
        String huizhiName = "";
        if ("DZ".equals(mnv.getType())) {
        } else if ("DS".equals(mnv.getType())) {
            meetingName ="董嘉会议通知";
            typeName ="董磐会通";
            huizhiName ="董事会议报名回执";
        } else if ("DSB".equals(mnv.getType())) {
            meetingName ="董嘉长办公会及通知";
            typeName ="董办会通";
            huizhiName ="董期长办公会议报名回执";
        } else if ("JL".equals(mnv.getType())) {
            meetingName = "总经理办公会及通知";
            typeName = "总办会通";
            huizhiName = "总经理办公会议报名回执";
        }

        Document document = new Document(PageSize.A4, 50.0F, 50.0F, 50.0F, 50.0F);
        FileOutputStream fos = null;
        boolean ok = true;
        try {
            fos = new FileOutputStream(outputPath);
            PdfWriter writer = PdfWriter.getInstance(document, fos);
            // writer.setPageEvent(new PdfExport());
            document.open();


            //Font titleFont = new Font (BaseFont.createFont("/ttf/simhei.ttf", "Identity-H", true), 25.0F);
            Font titleFont = new Font (BaseFont.createFont("STSongStd-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED), 22.0F, 1);
            Font contentFont = new Font (BaseFont.createFont("STSongStd-Light", "UniGB-UCS2-H", true), 16.0F);
            Font headerFont = new Font(BaseFont.createFont("STSongStd-Light", "UniGB-UCS2-H", true), 15.0F, 1);
            Font cellFont = new Font (BaseFont.createFont("STSongStd-Light", "UniGB-UCS2-H", true), 12.0F);


            Paragraph title = new Paragraph(meetingName, titleFont);
            //段落的对齐方式
            title.setAlignment(Element.ALIGN_CENTER);
            document.add(title);


            document.add(new Paragraph("\n", contentFont));


            Paragraph bianhao = new Paragraph (typeName + " [" + mnv.getYearNum() + "] " + mnv.getOrderNum() + "号", contentFont);
            //段落的对齐方式
            bianhao.setAlignment(Element.ALIGN_CENTER);
            document.add(bianhao);


            document.add(new Paragraph("\n", contentFont));

            Paragraph p = null;

            p = new Paragraph();
            //设置该段左缩进
            p.setIndentationLeft(50.0F);
            //设置段落前后的间距
            p.setSpacingAfter(5);
            p.setSpacingBefore(5);
            p.add(new Chunk("1 .时  间："+mnv.getMeetingTime(), contentFont));
            document.add(p);

            p = new Paragraph();
            //设置该段左缩进
            p.setIndentationLeft(50.0F);
            //设置段落前后的间距
            p.setSpacingAfter(5);
            p.setSpacingBefore(5);
            p.add(new Chunk("2 .地  点："+mnv.getMeetingPlace(), contentFont));
            document.add(p);

            p = new Paragraph();
            //设置该段左缩进
            p.setIndentationLeft(50.0F);
            //设置段落前后的间距
            p.setSpacingAfter(5);
            p.setSpacingBefore(5);
            p.add(new Chunk("3 .主持人："+mnv.getModerator(), contentFont));
            document.add(p);

            p = new Paragraph();
            //设置该段左缩进
            p.setIndentationLeft(50.0F);
            //设置段落前后的间距
            p.setSpacingAfter(5);
            p.setSpacingBefore(5);
            p.add(new Chunk("7 .会议内容：", contentFont));
            document.add(p);

            String[] tableHeader= { "会 议 内 容", "汇报人", "列席人", "汇报部门"};
            List subjectList = mnv.getSubjectDetailsArray();
            PdfPTable datatable = new PdfPTable(5);
            //表格的宽度百分比
            //datatable.setWidthPercentage(90);
            //表格每列分别设置宽度
            datatable.setTotalWidth(new float[] { 20, 160, 70, 180, 70 });
            //表格的水平对齐方式
            datatable.setHorizontalAlignment(Element.ALIGN_CENTER);
            //设置表格的底色
            //datatable.getDefaultCell().setBackgroundColor(BaseColor.GREEN);
            //表格单元格填充
            //datatable.getDefaultCell().setPadding(1);
            //表格单元格水平对齐方式
            datatable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            //表格单元格垂直对齐方式
            datatable.getDefaultCell().setVerticalAlignment(Element.ALIGN_CENTER);
            for (int i = 0; i < 4; ++i) {
                PdfPCell cell = new PdfPCell(new Paragraph(tableHeader[i], headerFont));
                if(i == 0){
                    //合并单元格
                    cell.setColspan(2);
                }
                //单元格水平对齐方式
                cell.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                //单元格垂直对齐方式
                cell.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE);
                //单元格行高
                cell.setFixedHeight(50F);
                datatable.addCell(cell);
            }
            for (int i = 0; i < subjectList.size(); i++) {
                Map<String, Object> map = (Map<String, Object>) subjectList.get(i);
                int num = i+1;
                datatable.addCell(new Paragraph(num+"", cellFont));
                datatable.addCell(new Paragraph((String) map.get("subjectName"), cellFont));
                datatable.addCell(new Paragraph("", cellFont));
                datatable.addCell(new Paragraph((String) map.get("subjectDecails"), cellFont));
                datatable.addCell(new Paragraph("", cellFont));
            }

            document.add(datatable);

            p = new Paragraph();
            //设置该段左缩进
            p.setIndentationLeft(50.0F);
            //设置段落前后的间距
            p.setSpacingAfter(5);
            p.setSpacingBefore(5);
            p.add(new Chunk("8 . 会议记录：***", contentFont));
            document.add(p);

            p = new Paragraph();
            //设置该段左缩进
            p.setIndentationLeft(250.0F);
            //设置段落前后的间距
            p.setSpacingAfter(5);
            p.setSpacingBefore(5);
            p.add(new Chunk("二○二一年四月十二日", contentFont));
            document.add(p);

            document.add(new LineSeparator(0.5F, 90.0F, BaseColor.BLACK, 1, -5.0F));

            PdfPTable table = new PdfPTable(new float[] { 50.0F, 150.0F });
            //表格的宽度百分比
            table.setWidthPercentage(80);
            for (int i = 0; i < 14; ++i) {
                String value = "";
                if (i == 0) value ="1.时间：";
                else if (i == 1) value = mnv.getMeetingTime();
                else if (i == 2) value ="2.地点：";
                else if (i == 3) value =mnv.getMeetingPlace();
                else if (i == 4) value ="3.主持：";
                else if (i == 5) value =mnv.getModerator();
                else if (i == 6) value ="4.出席：";
                else if (i == 7) value =mnv.getAttendUsers();
                else if (i == 8) value ="5.请假：";
                else if (i == 9) value ="";
                else if (i == 10) value="6.列席：";
                else if (i == 11) value=mnv.getAttendancePersonne1();
                else if (i == 12) value="7.会议内容：";
                else if (i == 13) value="";

                PdfPCell cell = new PdfPCell();
                cell.addElement(new Paragraph(value, contentFont));
                //设置单元格的边框样式和宽度
                cell.setBorder(0);
                cell.setBorderWidth(0F);
                //设置单元格的左填充
                cell.setPaddingLeft(20f);
                table.addCell(cell);
            }
            document.add(table);


            document.close();
            fos.close();
        } catch (IOException e) {
            ok = false;
        } catch (DocumentException e) {
            ok = false;
        } finally {
            if (fos != null)
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }

        return ok;
    }

}
