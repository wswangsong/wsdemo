package com.ws.demo.itext;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.parser.PdfReaderContentParser;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import com.itextpdf.text.pdf.parser.SimpleTextExtractionStrategy;
import com.itextpdf.text.pdf.parser.TextExtractionStrategy;

import java.io.*;
import java.nio.charset.Charset;

public class ItextUtil {

    public static void main(String[] args) {
        String pdfPath = "D:\\opt\\pdf\\三重一大系统对接协同办公系统接口文档.pdf";
        System.out.println(readPdfOne(pdfPath));
        System.out.println(readPdfTwo(pdfPath));

        String txtPath = "D:\\opt\\txt\\readPdfToTxt.txt";
        readPdfToText(pdfPath, txtPath);

        textToPdf("D:\\opt\\txt\\test.txt", "D:\\opt\\txt\\test1.pdf");
    }

    /**
     * 读取PDF文件内容
     * @param pdfPath
     */
    private static String readPdfOne(String pdfPath) {
        StringBuffer buf = new StringBuffer();
        try {
            PdfReader reader = new PdfReader(pdfPath);
            PdfReaderContentParser parser = new PdfReaderContentParser(reader);
            int num = reader.getNumberOfPages();// 获得页数
            TextExtractionStrategy strategy;
            for (int i = 1; i <= num; i++) {
                strategy = parser.processContent(i, new SimpleTextExtractionStrategy());
                buf.append(strategy.getResultantText());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return buf.toString();
    }

    /**
     * 读取PDF文件内容
     * @param pdfPath
     * @return
     * @throws IOException
     */
    public static String readPdfTwo(String pdfPath) {
        StringBuilder result = new StringBuilder();
        File file = new File(pdfPath);
        FileInputStream in = null;
        try {
            in = new FileInputStream(file);
            // 新建一个PDF解析器对象
            PdfReader reader = new PdfReader(in);
            reader.setAppendable(true);
            // 对PDF文件进行解析，获取PDF文档页码
            int countPage = reader.getNumberOfPages();
            //一页页读取PDF文本
            for(int i=1; i<=countPage; i++){
                result.append(PdfTextExtractor.getTextFromPage(reader, i));
            }
            reader.close();
        } catch (Exception e) {
            System.out.println("读取PDF文件" + file.getAbsolutePath() + "生失败！" + e);
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e1) {
                }
            }
        }
        return result.toString();
    }

    /**
     * 读取PDF文件内容到txt文件
     * @param pdfPath
     */
    private static void readPdfToText(String pdfPath, String txtPath) {
        // 读取pdf所使用的输出流
        PrintWriter writer = null;
        PdfReader reader = null;
        try {
            writer = new PrintWriter(new FileOutputStream(txtPath));
            reader = new PdfReader(pdfPath);
            int num = reader.getNumberOfPages();// 获得页数
            String content = ""; // 存放读取出的文档内容
            for (int i = 1; i <= num; i++) {
                // 读取第i页的文档内容
                content += PdfTextExtractor.getTextFromPage(reader, i);
            }
            String[] arr = content.split("/n");
            for (int i = 0; i < arr.length; i++) {
                System.out.println(arr[i]);
            }
            writer.write(content);// 写入文件内容
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * txt文本文件转pdf文件
     * @param text   F:/data/te616.txt
     * @param pdf  F:/data/aet618.pdf
     * @throws DocumentException
     * @throws IOException
     */
    public static void textToPdf(String text, String pdf){
        try {
            Document doc = new Document();
            OutputStream os = new FileOutputStream(new File(pdf));
            PdfWriter.getInstance(doc, os);
            doc.open();
            //指定 使用内置的中文字体
            BaseFont baseFont = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.EMBEDDED);
            Font font = new Font(baseFont,12,Font.NORMAL);
            //指定输出编码为UTF-8
            InputStreamReader isr = new InputStreamReader(new FileInputStream(new File(text)), Charset.forName("UTF-8"));
            BufferedReader br = new BufferedReader(isr);
            String str = "";
            while((str = br.readLine()) != null){
                doc.add(new Paragraph(str,font));
            }
            isr.close();
            br.close();
            doc.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

