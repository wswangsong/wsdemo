package com.ws.demo;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.extra.compress.CompressUtil;
import cn.hutool.extra.compress.extractor.Extractor;
import net.sourceforge.tess4j.util.LoggHelper;
import org.apache.commons.compress.archivers.ArchiveStreamFactory;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.net.URLEncoder;
import java.util.Calendar;

public class DemoTest {

    private static final Logger logger = LoggerFactory.getLogger(new LoggHelper().toString());

    public static void main(String[] args) throws Exception {
//        //Spring对物理资源的访问方式抽象成Resource，我们可以通过Spring提供的接口来访问磁盘文件等数据。
//        ResourceLoader resourceLoader = new DefaultResourceLoader();
//        //字节输入流，用来将文件中的数据读取到java程序中
//        String inputStream = resourceLoader.getResource("classpath:tessdata/pdf.ttf").getURL().getPath();
//        String tessdataPath = resourceLoader.getResource("classpath:tessdata").getURL().getPath();
//        logger.info(tessdataPath);
//        //环境变量
//        String gradleHome = System.getenv("GRADLE_HOME");
//        logger.info(gradleHome);
//        String rootPath = System.getProperty("user.dir");
//        logger.info(rootPath);
//
//        System.out.println("---"+"身份证".length());
//
//        System.out.println(System.getProperty("java.library.path"));


        String encodedString = URLEncoder.encode("议题收资测试", "UTF-8");
        System.out.println("====="+new String(encodedString.getBytes(),"iso-8859-1"));

        Calendar calendar = Calendar.getInstance();
        System.out.println("====="+calendar);
        System.out.println("====="+calendar.get(1) + File.separator + calendar.get(3));
    }

    @Test
    public void getUsers(){
        logger.info("666");
    }

}
