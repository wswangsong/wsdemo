package com.ws.demo.Json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ws.bean.Company;
import com.ws.bean.WebSite;
import com.ws.bean.People;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class JacksonDemo {

    /**
     * 序列化：对象转json字符串
     */
    @Test
    public void testObject2Json2() {
        People people2 = new People("1","上海辟谣专属队","职业辟谣，不信谣，不传谣，呵呵");
        List<People> people = new ArrayList<>();
        people.add(people2);
        WebSite webSite = new WebSite();
        webSite.setWebSiteName("xxxxxx.com");
        webSite.setPeople(people);

        List<WebSite> webSites = new ArrayList<>();
        webSites.add(webSite);
        Company company = new Company();
        company.setCompanyName("yyyyyy");
        company.setWebSites(webSites);

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String jsonString = objectMapper.writeValueAsString(company);
            System.out.println(jsonString);
        } catch (JsonProcessingException e) {
            //log.error("error: ", e);
        }
    }

    /**
     * 反序列化：json字符串转对象
     */
    @Test
    public void testJson2Object() {

        String json = "{\"companyName\":\"yyyyyy\",\"webSites\":[" +
                "{\"webSiteName\":\"xxxxxx.com\",\"people\":[{\"userId\":\"1\",\"username\":\"上海辟谣专属队\",\"password\":\"职业辟谣，不信谣，不传谣，呵呵\"}]}" +
                "]}";
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            Company company = objectMapper.readValue(json, Company.class);
            System.out.println(company.getWebSites().get(0).getPeople().get(0).getUsername());
        } catch (JsonProcessingException e) {
            //log.error("error: ", e);
        }
    }


    /**
     * 序列化：数组对象转json数组类型的字符串
     */
    @Test
    public void testObjectArray2JsonArrayString() {
        People people1 = new People("1","上海带刀沪卫","带刀大佬");
        People people2 = new People("1","上海辟谣专属队","职业辟谣，不信谣，不传谣，呵呵");
        List<People> people = new ArrayList<>();
        people.add(people1);
        people.add(people2);
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String json = objectMapper.writeValueAsString(people);
            System.out.println(json);
        } catch (JsonProcessingException e) {
            //log.error("error: ", e);
        }
    }


    /**
     * 反序列化：数组类型的json字符串转对象数组
     */
    @Test
    public void testJsonArrayString2ObjectArray() {
        String json = "[" +
                "{\"userId\":\"1\",\"username\":\"上海带刀沪卫\",\"password\":\"带刀大佬\"}" +
                ",{\"userId\":\"1\",\"username\":\"上海辟谣专属队\",\"password\":\"职业辟谣，不信谣，不传谣，呵呵\"}" +
                "]";
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            TypeReference<List<People>> typeReference = new TypeReference<List<People>>() {
            };
            List<People> list = objectMapper.readValue(json, typeReference);
            list.forEach(people -> {
                System.out.println(people.getUsername());
            });
        } catch (JsonProcessingException e) {
            //log.error("error: ", e);
        }
    }


    /**
     * 反序列化：多层嵌套的数组类型的json字符串转对象数组
     */
    @Test
    public void testJsonArrayString2ObjectArray2() {
        String json = "[" +
                "{" +
                "\"companyName\":\"yyyyyy\",\"webSites\":[" +
                "{\"webSiteName\":\"xxxxxx.com\",\"people\":[" +
                "{\"userId\":\"1\",\"username\":\"上海辟谣专属队\",\"password\":\"职业辟谣，不信谣，不传谣，呵呵\"}" +
                "]" +
                "}" +
                "]" +
                "}" +
                "]";
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            TypeReference<List<Company>> typeReference = new TypeReference<List<Company>>() {
            };
            List<Company> list = objectMapper.readValue(json, typeReference);
            list.forEach(company -> {
                System.out.println(company.getWebSites().get(0).getPeople().get(0).getUsername());
            });
        } catch (JsonProcessingException e) {
            //log.error("error: ", e);
        }
    }

}
