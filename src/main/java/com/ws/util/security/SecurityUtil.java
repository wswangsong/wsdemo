package com.ws.util.security;


import org.apache.commons.codec.binary.Base64;

public class SecurityUtil {

    /**
     * Base64编码
     */
    public static String encodeByBase64(String data) {
        Base64 base64 = new Base64();
        String result = base64.encodeToString(data.getBytes());
        return result;
    }

    /**
     * Base64解码
     */
    public static String decodeByBase64(String data) {
        Base64 base64 = new Base64();
        String result = new String(base64.decode(data));
        return result;
    }
}
