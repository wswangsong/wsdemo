package com.ws.util.security;

import java.net.URLDecoder;
import java.net.URLEncoder;

public class TestURLEncoder {

    public static void main(String[] args) throws Exception {
        System.out.println(URLEncoder.encode("尊敬的用户，你的验证码为623489，有效时间30分钟。【百度】", "GBK"));
        System.out.println(URLDecoder.decode("尊敬的用户，你的验证码为623489，有效时间30分钟。【百度】", "GBK"));

        String str  = URLDecoder.decode("中华人民共和国","UTF-8");
        System.out.println(str);

        String encodeStr = URLEncoder.encode("中国", "utf-8");
        System.out.println("--编码后:" + encodeStr);
        String decodeStr = URLDecoder.decode(encodeStr, "utf-8");
        System.out.println("--解码后:" + decodeStr);
    }
}
