package com.ws.util.ofd;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.Node;
import org.ofdrw.converter.ConvertHelper;
import org.ofdrw.converter.GeneralConvertException;
import org.ofdrw.converter.ImageMaker;
import org.ofdrw.converter.SVGMaker;
import org.ofdrw.reader.ContentExtractor;
import org.ofdrw.reader.OFDReader;
import org.ofdrw.reader.ResourceManage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class OfdUtil {

    private final static Logger logger = LoggerFactory.getLogger(OfdUtil.class);

    //获取ofd文件内容
    public static String getOfdText(String filePath){
        String content = "";
        try {
            OFDReader reader = new OFDReader(filePath);
            ContentExtractor extractor = new ContentExtractor(reader);
            content = String.join("", extractor.extractAll());
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return content;
    }

    //获取ofd文件内容
    public static List<String> getOfdTextList(String filePath){
        List<String> contentList = new ArrayList<String>();
        try {
            OFDReader reader = new OFDReader(filePath);
            ContentExtractor extractor = new ContentExtractor(reader);
            for (String content : extractor.extractAll()) {
                logger.info(content);
            }
            contentList = extractor.extractAll();
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return contentList;
    }

    //判断pdf文件是否包含图片
    public static Boolean isContainImage(String filePath){
        Boolean imageFlag = false;
        try {
            OFDReader reader = new OFDReader(filePath);
            ResourceManage rm = new ResourceManage(reader);
            int pageCount = reader.getNumberOfPages();
            for(int i=1; i<=pageCount; i++){
                Document document = reader.getPage(i).getDocument();
                List<Node> nodes = document.selectNodes("//ofd:ImageObject");
                for (Node node : nodes) {
                    String xmlText = node.asXML();
                    Element element = DocumentHelper.parseText(xmlText).getRootElement();
                    String refID = element.attribute("ResourceID").getValue();
                    BufferedImage image = rm.getImage(refID);
                    if(image != null){
                        imageFlag = true;
                    }
                }
            }
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return imageFlag;
    }

    //获取文件图片内容
    public static void getOfdImages(String filePath, String outPath) {
        try{
            OFDReader reader = new OFDReader(filePath);
            ResourceManage rm = new ResourceManage(reader);
            int pageCount = reader.getNumberOfPages();
            for(int i=1; i<=pageCount; i++){
                Document document = reader.getPage(i).getDocument();
                List<Node> nodes = document.selectNodes("//ofd:ImageObject");
                for (Node node : nodes) {
                    String xmlText = node.asXML();
                    Element element = DocumentHelper.parseText(xmlText).getRootElement();
                    String refID = element.attribute("ResourceID").getValue();
                    BufferedImage image = rm.getImage(refID);
                    if(image != null){
                        String format = "png";      // 假设需要将图片保存为PNG格式
                        File outTile = new File(outPath + System.nanoTime() + ".png");// 指定保存的图片文件
                        ImageIO.write(image, format, outTile); // 将图片写入文件中
                    }
                }
            }
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 将ofd转换为pdf
     * @param originPath 源文件路径
     * @param pdfPath 目标文件路径
     */
    public static void ofdToPdf(String originPath, String pdfPath) {
        // 1. 文件输入路径
        Path src = Paths.get(originPath);
        // 2. 转换后文件输出位置
        Path dst = Paths.get(pdfPath);
        try {
            // 3. OFD转换PDF
            ConvertHelper.toPdf(src, dst);
            System.out.println("生成文档位置: " + dst.toAbsolutePath());
        } catch (GeneralConvertException e) {
            // pom引入相关模块GeneralConvertException 类型错误表明转换过程中发生异常
            e.printStackTrace();
        }
    }

    /**
     * 将ofd转换为图片
     * @param originPath 源文件路径
     * @param imgPath 目标文件路径
     */
    public static void ofdToImg(String originPath, String imgPath) throws IOException {
        Files.createDirectories(Paths.get(imgPath));
        // 1. 文件输入路径
        Path src = Paths.get(originPath);
        // 2. 加载指定目录字体(非必须)
        // FontLoader.getInstance().scanFontDir(new File("src/test/resources/fonts"));
        // 3. 创建转换转换对象，设置 每毫米像素数量(Pixels per millimeter)
        try (OFDReader reader = new OFDReader(src)) {
            ImageMaker imageMaker = new ImageMaker(reader, 15);
            imageMaker.config.setDrawBoundary(false);
            for (int i = 0; i < imageMaker.pageSize(); i++) {
                // 4. 指定页码转换图片
                BufferedImage image = imageMaker.makePage(i);
                Path dist = Paths.get(imgPath,  i + ".png");
                // 5. 存储为指定格式图片
                ImageIO.write(image, "PNG", dist.toFile());
            }
        }
        // 6. Close OFDReader 删除工作过程中的临时文件 try close 语法
    }

    /**
     * 将ofd转换为SVG
     * @param originPath 源文件路径
     * @param svgPath 目标文件路径
     */
    public static void ofdToSvg(String originPath, String svgPath) throws IOException {
        Files.createDirectories(Paths.get(svgPath));
        // 1. 文件输入路径
        Path src = Paths.get(originPath);
        // 2. 加载指定目录字体(非必须)
        // FontLoader.getInstance().scanFontDir(new File("src/test/resources/fonts"));
        // 3. 创建转换转换对象，设置 每毫米像素数量(Pixels per millimeter)
        try(OFDReader reader = new OFDReader(src)) {
            SVGMaker svgMaker = new SVGMaker(reader, 20d);
            svgMaker.config.setDrawBoundary(false);
            svgMaker.config.setClip(false);
            for (int i = 0; i < svgMaker.pageSize(); i++) {
                // 4. 指定页码转换SVG，得到SVG(XML)
                String svg = svgMaker.makePage(i);
                Path dist = Paths.get(svgPath, i + ".svg");
                // 5. 存储到文件。
                Files.write(dist, svg.getBytes());
            }
        }
        // 6. Close OFDReader 删除工作过程中的临时文件 try close 语法
    }

    /**
     * 将ofd转换为HTML
     * @param originPath 源文件路径
     * @param htmlPath 目标文件路径
     */
    public static void ofdToHtml(String originPath, String htmlPath) {
        try {
            // 1. 提供文档
            Path ofdIn = Paths.get(originPath);
            Path htmlOut = Paths.get(htmlPath);
            // 2. [可选]配置字体，别名，扫描目录等
            // FontLoader.getInstance().addAliasMapping(null, "小标宋体", "方正小标宋简体", "方正小标宋简体")
            // FontLoader.getInstance().scanFontDir(new File("src/test/resources/fonts"));
            // 3. 配置参数（HTML页面宽度(px)），转换并存储HTML到文件。
            ConvertHelper.toHtml(ofdIn, htmlOut, 1000);
        } catch (GeneralConvertException | IOException e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) throws IOException {
        System.out.println(getOfdText("D:\\opt\\ofd\\辽宁清原抽水蓄能有限公司2023年第二十五次党委会会议纪要.ofd"));

        //OfdUtil.ofdToPdf("D:/opt/ofd/党委〔2022〕11号.ofd", "D:/opt/ofd/党委〔2022〕11号.pdf");
        //OfdUtil.ofdToImg("D:/opt/ofd/党委〔2022〕11号.ofd", "D:/opt/ofd/党委〔2022〕11号_img");
        //OfdUtil.ofdToSvg("D:/opt/ofd/党委〔2022〕11号.ofd", "D:/opt/ofd/党委〔2022〕11号_svg");
        //OfdUtil.ofdToHtml("D:/opt/ofd/党委〔2022〕11号.ofd", "D:/opt/ofd/党委〔2022〕11号.html");
        //System.out.println(OfdUtil.getOfdText("D:/opt/ofd/党委〔2022〕11号.ofd"));
        //OfdUtil.getOfdTextList("D:/opt/ofd/党委〔2022〕11号.ofd");
        //OfdUtil.getOfdImages("D:/opt/ofd/MutiLayer.ofd");
        //OfdUtil.getOfdImages("D:/opt/ofd/keyword.ofd");
        //OfdUtil.getOfdImages("D:/opt/ofd/keyword2.ofd");
        //OfdUtil.getOfdImages("D:/opt/ofd/AddWatermarkAnnot.ofd");
        //OfdUtil.getOfdImages("D:/opt/ofd/keyword2.ofd", "D:/opt/out/");

        //只有文字
        //System.out.println(getOfdText("D:\\opt\\ofd\\testText.ofd"));
        //getOfdImages("D:/opt/ofd/testText.ofd", "D:/opt/out/");

        //文字和图片
        //System.out.println(getOfdText("D:\\opt\\ofd\\testTextAndImage.ofd"));
        //getOfdImages("D:/opt/ofd/testTextAndImage.ofd", "D:/opt/out/");
        //System.out.println(getOfdText("D:\\opt\\ofd\\针对需兼容低版本浏览器的PC端的规范.ofd"));
        //getOfdImages("D:/opt/ofd/针对需兼容低版本浏览器的PC端的规范.ofd", "D:/opt/out/");
        //System.out.println(getOfdText("D:\\opt\\ofd\\ofd2_c01_C0117AddIcon.ofd"));
        //getOfdImages("D:/opt/ofd/ofd2_c01_C0117AddIcon.ofd", "D:/opt/out/");

        //只有图片
        //System.out.println(getOfdText("D:\\opt\\ofd\\testImage.ofd"));
        //getOfdImages("D:/opt/ofd/testImage.ofd", "D:/opt/out/");

//        //电子发票
//        System.out.println(getOfdText("D:\\opt\\ofd\\keyword2.ofd"));
//        System.out.println(isContainImage("D:\\opt\\ofd\\keyword2.ofd"));
//        getOfdImages("D:/opt/ofd/keyword2.ofd", "D:/opt/out/");

    }
}
