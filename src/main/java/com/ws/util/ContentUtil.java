package com.ws.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ContentUtil {

    /**
     * @Description 获取匹配正则表达式的字符串（第一个）
     */
    public static String getMatchContent(String content){
        // 编写正则表达式
        String regex = "([0-9]+).(jpg|png|jpeg)";
        // 匹配当前正则表达式
        Matcher matcher = Pattern.compile(regex).matcher(content);
        // 定义内容
        String result = "";
        // 判断是否可以找到匹配正则表达式的字符
        if (matcher.find()) {
            // 将匹配当前正则表达式的字符串进行赋值
            result = matcher.group();//取第一个匹配的字符串
        }
        // 返回
        return result;
    }

    /**
     * @Description 获取匹配正则表达式的字符串
     */
    public static List<String> getMatchContent(String content, String regex) {
        List<String> result = new ArrayList<String>();
        //获取pattern对象
        Pattern compile = Pattern.compile(regex);
        //获取Matcher对象
        Matcher matcher = compile.matcher(content);
        while(matcher.find()){
            result.add(matcher.group());
        }
        return result;
    }

    /**
     * @Description 替换符合正则表达式的字符串并替换
     */
    public static String replaceMatchContent(String content, String regex){
        //获取pattern对象
        Pattern compile = Pattern.compile(regex);
        //获取Matcher对象
        Matcher matcher = compile.matcher(content);
        //替换
        return matcher.replaceAll(""); //把匹配的替换成空
    }

    /**
     * @Description 是否包含符合当前正则表达式的字符串
     */
    public static Boolean isMatchContent(String content, String regex){
        // 匹配当前正则表达式
        Matcher matcher = Pattern.compile(regex).matcher(content);
        // 判断是否可以找到匹配正则表达式的字符
        if (matcher.find()) {
           return true;
        }
        // 返回
        return false;
    }

    /**
     * @Description 获取中文字符
     */
    public static List<String> getChineseContent(String content){
        List<String> result = new ArrayList<String>();
        // 正则表达式
        String regex = "[\\u4e00-\\u9fa5]+";
        // 匹配当前正则表达式
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(content);
        while (m.find()) {
            result.add(m.group());
        }
        return result;
    }

    /**
     * @Description 去除字符串中中文字符
     */
    public static String replaceChinese(String content){
        // 正则表达式
        String regex = "[\\u4e00-\\u9fa5]+";
        // 匹配当前正则表达式
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(content);
        String result = m.replaceAll("").trim();
        // 返回
        return result;
    }

    /**
     * 去除字符串中的空格、回车、换行符、制表符
     */
    public static String replaceBlank(String content) {
        // 正则表达式
        String regex = "\\s*|\r|\n|\t";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(content);
        String result = m.replaceAll("");
        return result;
    }

    public static String conversion(String content){
        return content.replaceAll("，", ",")
                .replaceAll("：", ":")
                .replaceAll("（", "(")
                .replaceAll("）", ")")
                .replaceAll("！", "!")
//				.replaceAll("。", ".")
//				.replaceAll("、", ".")
                .replaceAll("\\s*", "");//去除空格
    }

    /**
     * @Description 获取字符串的Unicode编码
     */
    public static String getUnicode(String content) {
        String strTemp = "";
        if (content != null) {
            for (char c : content.toCharArray()) {
                if (c > 255) {
                    strTemp += "\\u" + Integer.toHexString((int)c);
                } else {
                    strTemp += "\\u00" + Integer.toHexString((int)c);
                }
            }
        }
        return strTemp;
    }

    public static void main(String[] args) {
        String s = "放松fsdfs.png放松321.jpg发顺丰afaaf.jpgfds42.png";
        System.out.println(getUnicode("次期"));
        System.out.println(getMatchContent(s));
        System.out.println(getMatchContent(s,"([0-9]+).(jpg|png|jpeg)"));
        System.out.println(getChineseContent(s));
        System.out.println(replaceChinese(s));
        System.out.println(replaceBlank("fsfsdfs   hf"));
    }

}
