package com.ws.util;

import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;

/**
 * 功能: 将输入的汉语转换为汉语拼音（例如: 测试  -->  ceshi)
 */
public class ChineseUtil {
	/**
     * 获取汉字串拼音，英文字符不变
     * @param chinese 汉字串
     * @return 汉语拼音
     */
    public static String cn2Spell(String chinese) {
	    StringBuffer pybf = new StringBuffer();
	    char[] arr = chinese.toCharArray();
	    HanyuPinyinOutputFormat defaultFormat = new HanyuPinyinOutputFormat();
	    defaultFormat.setCaseType(HanyuPinyinCaseType.LOWERCASE);
	    defaultFormat.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
	    for (int i = 0; i < arr.length; i++) {
	        if (arr[i] > 128) {
	            try {
	            	pybf.append(PinyinHelper.toHanyuPinyinStringArray(arr[i], defaultFormat)[0]);
	            } catch (BadHanyuPinyinOutputFormatCombination e) {
	            	e.printStackTrace();
	            }
	        } else {
	            pybf.append(arr[i]);
	        }
	    }
	    return pybf.toString();
    }

	/**
     * 获取汉字串拼音首字母，英文字符不变
     * @param chinese 汉字串
     * @return 汉语拼音首字母
     */
    public static String cn2FirstSpell(String chinese) {
        StringBuffer pybf = new StringBuffer();
        char[] arr = chinese.toCharArray();
        HanyuPinyinOutputFormat defaultFormat = new HanyuPinyinOutputFormat();
        defaultFormat.setCaseType(HanyuPinyinCaseType.LOWERCASE);
        defaultFormat.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
        for (int i = 0; i < arr.length; i++) {
	        if (arr[i] > 128) {
                try {
                    String[] _t = PinyinHelper.toHanyuPinyinStringArray(arr[i], defaultFormat);
                    if (_t != null) {
                            pybf.append(_t[0].charAt(0));
                    }
                } catch (BadHanyuPinyinOutputFormatCombination e) {
                	e.printStackTrace();
                }
	        } else {
	            pybf.append(arr[i]);
	        }
        }
        return pybf.toString().replaceAll("\\W", "").trim().toUpperCase();
    }


    public static void main(String[] args){
        String cnStr = "测试";
        System.out.println(cn2FirstSpell(cnStr));
        System.out.println(cn2Spell(cnStr));
    }
}
