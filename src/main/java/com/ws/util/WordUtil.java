package com.ws.util;

import java.io.*;
import java.util.List;
import java.util.Map;

import com.ws.controller.DataAction;
import com.ws.demo.docx4j.Docx4jUtil;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.apache.poi.xwpf.converter.pdf.PdfConverter;
import org.apache.poi.xwpf.converter.pdf.PdfOptions;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.docx4j.Docx4J;
import org.docx4j.convert.out.FOSettings;
import org.docx4j.fonts.IdentityPlusMapper;
import org.docx4j.fonts.Mapper;
import org.docx4j.fonts.PhysicalFonts;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;

import com.documents4j.api.DocumentType;
import com.documents4j.api.IConverter;
import com.documents4j.job.LocalConverter;
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;

/**
 * Word文档工具类
 */
public class WordUtil {

	private final static Logger logger = LoggerFactory.getLogger(DataAction.class);

	/**
	 * 使用FreeMarker自动生成Word文档
	 * @param dataMap   生成Word文档所需要的数据
	 * @param fileName  生成Word文档的全路径名称
	 */
	public static void generateWord(Map<String, Object> dataMap, String fileName) throws Exception {
		// 设置FreeMarker的版本和编码格式
		Configuration configuration = new Configuration(Configuration.getVersion());
		configuration.setDefaultEncoding("UTF-8");

		// 设置FreeMarker生成Word文档所需要的模板的路径
		ClassPathResource imageResource = new ClassPathResource("templates/ftl");
		configuration.setDirectoryForTemplateLoading(imageResource.getFile());
		//configuration.setDirectoryForTemplateLoading(new File("D:/workspaces/idea/demo/src/main/resources"));
		// 设置FreeMarker生成Word文档所需要的模板
		//Template t = configuration.getTemplate("WordTemplate.ftl", "UTF-8");
		Template t = configuration.getTemplate("WordTemplate2.ftl", "UTF-8");
		// 创建一个Word文档的输出流
		Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File(fileName)), "UTF-8"));
		//FreeMarker使用Word模板和数据生成Word文档
		t.process(dataMap, out);
		out.flush();
		out.close();
	}

	/**
	 * @description 根据模板替换变量
	 * @param inputStream 模板输入流
	 * @param map 变量集合
	 * @param outputStream 输出流
	 * @throws Exception
	 */
	public static void replaceVariable(InputStream inputStream, Map<String, String> map, OutputStream outputStream) throws Exception{
		WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage.load(inputStream);
		MainDocumentPart documentPart = wordMLPackage.getMainDocumentPart();
		//清扫docx4j模板变量字符,通常以${variable}形式
		Docx4jUtil.cleanDocumentPart(documentPart);
		documentPart.variableReplace(map);
		Docx4J.save(wordMLPackage, outputStream);
	}

	/**
	 * docx转换为pdf
	 * @param inputStream 输入流
	 * @param outputStream 输出流
	 * @throws Exception
	 */
	public static void wordToPdfByDocx4j(InputStream inputStream, OutputStream outputStream) {
		try {
			WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage.load(inputStream);
			//处理中文乱码
			Mapper fontMapper = new IdentityPlusMapper();
			fontMapper.put("隶书", PhysicalFonts.get("LiSu"));
			fontMapper.put("宋体",PhysicalFonts.get("SimSun"));
			fontMapper.put("微软雅黑",PhysicalFonts.get("Microsoft Yahei"));
			fontMapper.put("黑体",PhysicalFonts.get("SimHei"));
			fontMapper.put("楷体",PhysicalFonts.get("KaiTi"));
			fontMapper.put("新宋体",PhysicalFonts.get("NSimSun"));
			fontMapper.put("华文行楷", PhysicalFonts.get("STXingkai"));
			fontMapper.put("华文仿宋", PhysicalFonts.get("STFangsong"));
			fontMapper.put("宋体扩展",PhysicalFonts.get("simsun-extB"));
			fontMapper.put("仿宋",PhysicalFonts.get("FangSong"));
			fontMapper.put("仿宋_GB2312",PhysicalFonts.get("FangSong_GB2312"));
			fontMapper.put("幼圆",PhysicalFonts.get("YouYuan"));
			fontMapper.put("华文宋体",PhysicalFonts.get("STSong"));
			fontMapper.put("华文中宋",PhysicalFonts.get("STZhongsong"));
		    wordMLPackage.setFontMapper(fontMapper);
		    Docx4J.toPDF(wordMLPackage, outputStream);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
     * 通过documents4j 实现word转pdf
     *
     * @param sourcePath 源文件地址 如 /root/example.doc
     * @param targetPath 目标文件地址 如 /root/example.pdf
     */
    public static void wordToPdfByDocuments4j(String sourcePath, String targetPath) {
        File inputWord = new File(sourcePath);
        File outputFile = new File(targetPath);
        try  {
            InputStream docxInputStream = new FileInputStream(inputWord);
            OutputStream outputStream = new FileOutputStream(outputFile);
            IConverter converter = LocalConverter.builder().build();
            converter.convert(docxInputStream)
                    .as(DocumentType.DOCX)
                    .to(outputStream)
                    .as(DocumentType.PDF).execute();
            outputStream.close();
            docxInputStream.close();
        } catch (Exception e) {
            //log.error("[documents4J] word转pdf失败:{}", e.toString());
        }
    }

	/**
	 * 通过documents4j 实现word转pdf
	 */
    public static void wordToPdfByDocuments4j(InputStream inputStream, OutputStream outputStream) {
        try  {
            IConverter converter = LocalConverter.builder().build();
            converter.convert(inputStream)
                    .as(DocumentType.DOCX)
                    .to(outputStream)
                    .as(DocumentType.PDF).execute();
        } catch (Exception e) {
            //log.error("[documents4J] word转pdf失败:{}", e.toString());
        }
    }

	/**
	 * 通过poi 实现word转pdf
	 */
	public static void wordToPdfByPoi(InputStream inputStream, OutputStream outputStream) {
		try {
			XWPFDocument document = new XWPFDocument(inputStream);
			PdfOptions options = PdfOptions.create();
			PdfConverter.getInstance().convert(document, outputStream, options);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 利用libreOffice将office文档转换成pdf
	 * @param inputFile  目标文件地址
	 * @param pdfFile    输出文件夹
	 * @return
	 */
	public static boolean wordToPdfByLibreOffice(String inputFile, String pdfFile){

		long start = System.currentTimeMillis();
		String command;
		boolean flag;
		String osName = System.getProperty("os.name");
		if (osName.contains("Windows")) {
			command = "cmd /c soffice --headless --invisible --convert-to pdf:writer_pdf_Export " + inputFile + " --outdir " + pdfFile;
		} else {
			command = "libreoffice --headless --invisible --convert-to pdf:writer_pdf_Export " + inputFile + " --outdir " + pdfFile;
		}
		flag = executeLibreOfficeCommand(command);
		long end = System.currentTimeMillis();
		logger.debug("用时:{} ms", end - start);
		return flag;
	}

	/**
	 * 执行command指令
	 * @param command
	 * @return
	 */
	public static boolean executeLibreOfficeCommand(String command) {

		logger.info("开始进行转化.......");
		Process process;// Process可以控制该子进程的执行或获取该子进程的信息
		try {
			logger.debug("convertOffice2PDF cmd : {}", command);
			process = Runtime.getRuntime().exec(command);// exec()方法指示Java虚拟机创建一个子进程执行指定的可执行程序，并返回与该子进程对应的Process对象实例。
			// 下面两个可以获取输入输出流
//            InputStream errorStream = process.getErrorStream();
//            InputStream inputStream = process.getInputStream();
		} catch (IOException e) {
			logger.error(" convertOffice2PDF {} error", command, e);
			return false;
		}

		int exitStatus = 0;
		try {
			exitStatus = process.waitFor();// 等待子进程完成再往下执行，返回值是子线程执行完毕的返回值,返回0表示正常结束
			// 第二种接受返回值的方法
			int i = process.exitValue(); // 接收执行完毕的返回值
			logger.debug("i----" + i);
		} catch (InterruptedException e) {
			logger.error("InterruptedException  convertOffice2PDF {}", command, e);
			return false;
		}

		if (exitStatus != 0) {
			logger.error("convertOffice2PDF cmd exitStatus {}", exitStatus);
		} else {
			logger.debug("convertOffice2PDF cmd exitStatus {}", exitStatus);
		}
		process.destroy(); // 销毁子进程
		logger.info("转化结束.......");
		return true;
	}
}