package com.ws.util.date;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;

import java.text.ParseException;

/**
 * 时间日期工具类
 */
public class DateUtil {

    /*
     * 将时间转换为时间戳（单位：毫秒）
     */
    public static String dateToStamp(String s){
        String res = "";
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = simpleDateFormat.parse(s);
            long ts = date.getTime();
            res = String.valueOf(ts);
        } catch (ParseException e) {
            e.printStackTrace();
            System.out.println("时间格式有误，标准格式为yyyy-MM-dd HH:mm:ss，请检查！");
            throw new IllegalArgumentException("Illegal Argument arg:" + s);
        }
        return res;
    }

    /*
     * 将时间戳转换为时间（单位：毫秒）
     */
    public static String stampToDate(String s){
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        long lt = new Long(s);
        Date date = new Date(lt);
        res = simpleDateFormat.format(date);
        return res;
    }

    /*
     * 获取昨天日期
     */
    public static String getDateForYesterday(){
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        String yesterday = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
        return yesterday;
    }

    /*
     * 获取当前日期
     */
    public static String getDate(){
        Date d = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(d);
    }

    /*
     * 获取当前时间
     */
    public static String getTime(){
        Date d = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        return sdf.format(d);
    }

    /*
     * 获取当前日期和时间
     */
    public static String getDateTime(){
        Date d = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(d);
    }

    /*
     * 获取当前时间戳（单位：毫秒）
     */
    public static long getTimeStamp(){
        return System.currentTimeMillis();
    }

    /**
     * @Description：输入日期，获取输入日期的前一天
     * @strData：参数格式：yyyy-MM-dd
     * @return：返回格式：yyyy-MM-dd
     */
    public static String getPreDateByDate(String strData) {
        String preDate = "";
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf =new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = sdf.parse(strData);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }

        c.setTime(date);
        int day1 = c.get(Calendar.DATE);
        c.set(Calendar.DATE, day1 - 1);
        preDate = sdf.format(c.getTime());
        return preDate;
    }

    /*
     * 根据两个时间的时间戳获取时间差(单位：分钟)
     */
    public static long getTimeDifference(Long startTime, Long stopTime) {
        Long minutes = (stopTime - startTime) / (1000 * 60);
        return minutes;
    }

    /*
     * 根据两个时间的时间戳获取时间差(单位：分钟)
     */
    public static long getTimeDifference(String startTime, String stopTime) {
        Long minutes = (Long.parseLong(stopTime) - Long.parseLong(startTime)) / (1000 * 60);
        return minutes;
    }

    /*
     * 获取两个日期之间相隔的工作日天数（除去周末）
     */
    public static int countWorkDays(String strStartDate, String strEndDate) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

        Calendar cl1 = Calendar.getInstance();
        Calendar cl2 = Calendar.getInstance();

        try {
            cl1.setTime(df.parse(strStartDate));
            cl2.setTime(df.parse(strEndDate));

        } catch (ParseException e) {
            System.out.println("日期格式非法");
            e.printStackTrace();
        }

        int count = 0;
        while (cl1.compareTo(cl2) <= 0) {
            if(cl1.get(Calendar.DAY_OF_WEEK) != 7 && cl1.get(Calendar.DAY_OF_WEEK) != 1)
                count++;
            cl1.add(Calendar.DAY_OF_MONTH, 1);
        }
        return count;

    }

    /**
     * @Description：判断某一时间是否在一个区间内
     * @param sourceTime 时间区间,半闭合,如[10:00-20:00)
     * @param curTime 需要判断的时间 如10:00
     * @return
     * @throws IllegalArgumentException
     */
    public static boolean isInTime(String sourceTime, String curTime) {
        if (sourceTime == null || !sourceTime.contains("-") || !sourceTime.contains(":")) {
            throw new IllegalArgumentException("Illegal Argument arg:" + sourceTime);
        }
        if (curTime == null || !curTime.contains(":")) {
            throw new IllegalArgumentException("Illegal Argument arg:" + curTime);
        }
        String[] args = sourceTime.split("-");
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        try {
            long now = sdf.parse(curTime).getTime();
            long start = sdf.parse(args[0]).getTime();
            long end = sdf.parse(args[1]).getTime();
            if (args[1].equals("00:00")) {
                args[1] = "24:00";
            }
            if (end < start) {
                if (now >= end && now < start) {
                    return false;
                } else {
                    return true;
                }
            }
            else {
                if (now >= start && now < end) {
                    return true;
                } else {
                    return false;
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
            throw new IllegalArgumentException("Illegal Argument arg:" + sourceTime);
        }

    }

    /**
     * @Description：将输入的日期转换为中文日期（例如: 2007-10-05 --> 二○○七年十月五日)
     * @param date 参数格式：yyyy-MM-dd
     * @return
     */
    public static String toChineseDate(String date){

        String chineseDate = "";

        String datestr = "";
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            sdf.setLenient(false);//严格解析日期，如果日期不合格就抛异常，不会自动计算。
            Date da = sdf.parse(date);
            datestr = sdf.format(da);
        } catch (ParseException e) {
            e.printStackTrace();
            System.out.println("时间格式有误，标准格式为yyyy-MM-dd，请检查！");
        }
        String[] strs = datestr.split("-");
        // 年
        for (int i = 0; i < strs[0].length(); i++) {
            chineseDate += formatDigit(strs[0].charAt(i));
        }
        // chineseDate = chineseDate+"年";
        // 月
        char c1 = strs[1].charAt(0);
        char c2 = strs[1].charAt(1);
        String newmonth = "";
        if (c1 == '0') {
            newmonth = String.valueOf(formatDigit(c2));
        } else if (c1 == '1' && c2 == '0') {
            newmonth = "十";
        } else if (c1 == '1' && c2 != '0') {
            newmonth = "十" + formatDigit(c2);
        }
        chineseDate = chineseDate + "年" + newmonth + "月";
        // 日
        char d1 = strs[2].charAt(0);
        char d2 = strs[2].charAt(1);
        String newday = "";
        if (d1 == '0') {//单位数天
            newday = String.valueOf(formatDigit(d2));
        } else if (d1 != '1' && d2 == '0') {//几十
            newday = String.valueOf(formatDigit(d1)) + "十";
        } else if (d1 != '1' && d2 != '0') {//几十几
            newday = formatDigit(d1) + "十" + formatDigit(d2);
        } else if (d1 == '1' && d2 != '0') {//十几
            newday = "十" + formatDigit(d2);
        } else {//10
            newday = "十";
        }
        chineseDate = chineseDate + newday + "日";

        return chineseDate;
    }

    public static char formatDigit(char sign) {
        if (sign == '0') {
            sign = '〇';
        }
        if (sign == '1') {
            sign = '一';
        }
        if (sign == '2') {
            sign = '二';
        }
        if (sign == '3') {
            sign = '三';
        }
        if (sign == '4') {
            sign = '四';
        }
        if (sign == '5') {
            sign = '五';
        }
        if (sign == '6') {
            sign = '六';
        }
        if (sign == '7') {
            sign = '七';
        }
        if (sign == '8') {
            sign = '八';
        }
        if (sign == '9') {
            sign = '九';
        }
        return sign;
    }

    public static Date convertCnDate(String cprq) {
        int yearPos = cprq.indexOf("年");
        int monthPos = cprq.indexOf("月");
        String cnYear = cprq.substring(0, yearPos);
        String cnMonth = cprq.substring(yearPos + 1, monthPos);
        String cnDay = cprq.substring(monthPos + 1, cprq.length() - 1);
        String year = ConvertCnYear(cnYear);
        String month = ConvertCnDateNumber(cnMonth);
        String day = ConvertCnDateNumber(cnDay);
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, Integer.parseInt(year));
        c.set(Calendar.MONTH, Integer.parseInt(month)-1);
        c.set(Calendar.DAY_OF_MONTH, Integer.parseInt(day));
        return c.getTime();
    }

    private static String ConvertCnYear(String cnYear) {
        if(cnYear.length() == 2) {
            return "20" + ConvertCnNumberChar(cnYear);
        } else {
            return ConvertCnNumberChar(cnYear);
        }
    }

    private static String ConvertCnDateNumber(String cnNumber) {
        if (cnNumber.length() == 1) {
            if(cnNumber.equals("十")){
                return "10";
            } else {
                return ConvertCnNumberChar(cnNumber);
            }
        } else if (cnNumber.length() == 2) {
            if (cnNumber.startsWith("十")) {
                return "1" + ConvertCnNumberChar(cnNumber.substring(1, 2));
            } else if (cnNumber.endsWith("十")) {
                return ConvertCnNumberChar(cnNumber.substring(0, 1)) + "0";
            } else {
                return ConvertCnNumberChar(cnNumber);
            }
        } else if (cnNumber.length() == 3) {
            return ConvertCnNumberChar(cnNumber.substring(0, 1) + cnNumber.substring(2, 3));
        }
        return null;
    }

    private static String ConvertCnNumberChar(String cnNumberStr) {
        String ALL_CN_NUMBER = "○零一二三四五六七八九";
        String ALL_NUMBER = "00123456789";
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < cnNumberStr.length(); i++) {
            char c = cnNumberStr.charAt(i);
            int index = ALL_CN_NUMBER.indexOf(c);
            if (index != -1) {
                buf.append(ALL_NUMBER.charAt(index));
            } else {
                buf.append(cnNumberStr.charAt(i));
            }
        }
        return buf.toString();
    }

    // 计算日期相差天数
    public static long betweenDay(LocalDate date1,LocalDate date2){
        long days = ChronoUnit.DAYS.between(date1, date2);
        return days;
    }

    // 计算日期相差天数
    public static long betweenDay(String date1Str, String date2Str){
        LocalDate date1 = LocalDate.parse(date1Str);
        LocalDate date2 = LocalDate.parse(date2Str);
        long days = ChronoUnit.DAYS.between(date1, date2);
        return days;
    }

    // 判断是否符合日期格式
    public static boolean isValidDateFormat(String date) {
        String pattern = "yyyy-MM-dd";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        try {
            LocalDate parsedDate = LocalDate.parse(date, formatter);
            String formattedDate = parsedDate.format(formatter);
            return date.equals(formattedDate);
        } catch (DateTimeParseException e) {
            return false;
        }
    }

    public static void main(String[] a) throws Exception {
        System.out.println("昨天：" + getDateForYesterday());
        System.out.println("今天：" + getDate());
        System.out.println("当前时间：" + getTime());
        System.out.println("当前时间戳：" + getTimeStamp());
        System.out.println("当前日期时间：" + getDateTime());
        System.out.println("获取输入日期的前一天：" + getPreDateByDate("2021-08-01"));
        System.out.println("时间转换为时间戳：" + dateToStamp("2018-10-24 14:01:16"));
        System.out.println("时间戳转换为时间：" + stampToDate("1537351112924"));
        System.out.println("---------------------------------");
        Long s = getTimeDifference("1537351113924","1537351212924");
        Long l = getTimeDifference(1537351113924l,1537351212924l);
        System.out.println("时间差(单位：分钟)：" + s);
        System.out.println("时间差(单位：分钟)：" + l);
        System.out.println("时间差(单位：工作日)：" + countWorkDays("2022-01-05","2022-01-10"));
        System.out.println("---------------------------------");
        String e = "2018-10-24 14:01:16".substring(11,16);
        System.out.println("当前时间：" + e);
        System.out.println(isInTime("00:00-15:00", e));
        System.out.println(isInTime("20:00-01:00", e));
        System.out.println(isInTime("20:00-23:00", "18:00"));
        System.out.println(isInTime("20:00-23:00", "20:00"));
        System.out.println(isInTime("20:00-23:00", "22:00"));
        System.out.println(isInTime("20:00-23:00", "23:00"));
        System.out.println(isInTime("20:00-24:00", "23:00"));
        System.out.println("---------------------------------");
        System.out.println("判断时间先后：");
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        System.out.println(format.parse("2018-10-24 14:00:16").before(format.parse("2018-10-24 14:01:16")));
        System.out.println(format.parse("2018-10-24 14:00:16").after(format.parse("2018-10-24 14:01:16")));
        System.out.println("---------------------------------");
        System.out.println("日期转换为中文日期：" + toChineseDate("2008-12-05"));
        System.out.println("中文日期转换为日期：" + new SimpleDateFormat("yyyy-MM-dd").format(convertCnDate("二○○九年四月三十日")));
        System.out.println("---------------------------------");
        System.out.println(betweenDay("2018-10-24", "2018-10-28"));
        System.out.println("---------------------------------");
        System.out.println(isValidDateFormat("2018:10:24"));
        System.out.println(isValidDateFormat("2018-10-24"));

    }

}


