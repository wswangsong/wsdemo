package com.ws.util;

import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.Raster;
import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import io.github.jonathanlink.PDFLayoutTextStripper;
import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import org.apache.commons.lang3.StringUtils;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.*;
import org.apache.pdfbox.pdmodel.encryption.InvalidPasswordException;
import org.apache.pdfbox.pdmodel.graphics.PDXObject;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.pdmodel.interactive.action.PDActionGoTo;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.destination.PDPageDestination;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.outline.PDDocumentOutline;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.outline.PDOutlineItem;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.outline.PDOutlineNode;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;
import org.apache.tika.Tika;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.sax.BodyContentHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import technology.tabula.*;
import technology.tabula.extractors.SpreadsheetExtractionAlgorithm;

import javax.imageio.ImageIO;

public class PdfUtil {

	private final static Logger logger = LoggerFactory.getLogger(PdfUtil.class);

	//获取pdf文件文本内容
	public static void getDocumentInformation(String filePath){
		try {
			File file = new File(filePath);
			PDDocument document = PDDocument.load(file);
			PDDocumentInformation pdd = document.getDocumentInformation();

			System.out.println("Author of the document is :"+ pdd.getAuthor());
			System.out.println("Title of the document is :"+ pdd.getTitle());
			System.out.println("Subject of the document is :"+ pdd.getSubject());
			System.out.println("Creator of the document is :"+ pdd.getCreator());
			System.out.println("Creation date of the document is :"+ pdd.getCreationDate());
			System.out.println("Modification date of the document is :"+ pdd.getModificationDate());
			System.out.println("Keywords of the document are :"+ pdd.getKeywords());

			document.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	//获取pdf文件文本内容
	public static String getTextByTika(String filePath){
		String result = "";
		try {
			Tika tika = new Tika();
			File file = new File(filePath);
			result = tika.parseToString(file);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	//获取pdf文件文本内容
	public static String getTextByPdfbox(String filePath){
		String result = "";
		try {
			File file = new File(filePath);
			PDDocument document = PDDocument.load(file);
			PDFTextStripper pdfStripper = new PDFTextStripper();
			result = pdfStripper.getText(document);
			document.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}

	//获取pdf文件文本内容
	public static String getLayoutTextByPdfbox(String filePath){
		String result = null;
		try {
			File file = new File(filePath);
			PDDocument document = PDDocument.load(file);
			PDFTextStripper pdfTextStripper = new PDFLayoutTextStripper();
			result = pdfTextStripper.getText(document);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}

	public static String getTextByRegion(String filePath, int pageNumber) {
		StringBuffer result = new StringBuffer();
		try {
			File file = new File(filePath);
			PDDocument document = PDDocument.load(file);

			//Rectangle2D region = new Rectangle2D.Double(x,y,width,height);
			Rectangle2D region = new Rectangle2D.Double(0, 0, 550, 750);
			String regionName = "region";

			PDFTextStripperByArea stripper;
			PDPage page = document.getPage(pageNumber + 1);
			stripper = new PDFTextStripperByArea();
			stripper.addRegion(regionName, region);
			stripper.extractRegions(page);
			result.append(stripper.getTextForRegion(regionName));
			document.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result.toString();
	}

	public static String getTextByRegion(String filePath){
		StringBuffer result = new StringBuffer();
		try {
			File file = new File(filePath);
			PDDocument document = PDDocument.load(file);
			PDPageTree list = document.getPages();

			String regionName = "region";
			//Rectangle2D region = new Rectangle2D.Double(x,y,width,height);
			Rectangle2D region = new Rectangle2D.Double(0, 0, 550, 750);

			PDFTextStripperByArea stripper = new PDFTextStripperByArea();
			for (PDPage page : list) {
				stripper.addRegion(regionName, region);
				stripper.extractRegions(page);
				result.append(stripper.getTextForRegion(regionName));
			}
			document.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result.toString();
	}

	//判断pdf文件是否包含图片
	public static Boolean isContainImage(String filePath){
		Boolean imageFlag = false;
		try {
			File file = new File(filePath);
			PDDocument document = PDDocument.load(file);
			PDPageTree list = document.getPages();
			for(PDPage page : list){
				PDResources pdResources = page.getResources();
				if(pdResources != null){
					for(COSName c : pdResources.getXObjectNames()){
						PDXObject o = pdResources.getXObject(c);
						if(o instanceof PDImageXObject){
							imageFlag = true;
						}
					}
				}
			}
			document.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return imageFlag;
	}

	//获取pdf文件图片内容
	public static void getPdfImagesOne(String filePath, String outPath){
		try {
			File file = new File(filePath);
			PDDocument document = PDDocument.load(file);
			PDPageTree list = document.getPages();

			for (PDPage page : list) {
				PDResources pdResources = page.getResources();
				if(pdResources != null){
					for (COSName c : pdResources.getXObjectNames()) {
						PDXObject o = pdResources.getXObject(c);
						if (o instanceof org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject) {
							File outFile = new File(outPath + System.nanoTime() + ".png");
							ImageIO.write(((org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject)o).getImage(), "png", outFile);
						}
					}
				}
			}
			document.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	//获取pdf文件图片内容
    public static void getPdfImagesTwo(String filePath, String outPath){
		try {
			File file = new File(filePath);
			PDDocument document = PDDocument.load(file);
			PDFRenderer renderer = new PDFRenderer(document);
			PDPageTree list = document.getPages();
			for(int i=0; i<list.getCount(); i++){
				BufferedImage image = renderer.renderImage(i);
				if(image != null){
					ImageIO.write(image, "JPEG", new File(outPath + System.nanoTime() + ".png"));
				}
			}
	    	document.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }

	//获取pdf文件内容(图片和文本)
	public static void getPdfContent(String filePath) {
		File file = new File(filePath);
		InputStream is = null;
		PDDocument document = null;
		try {
			if (filePath.endsWith(".pdf")) {
				document = PDDocument.load(file);
				int pageSize = document.getNumberOfPages();
				// 一页一页读取
				for (int i = 0; i < pageSize; i++) {
					// 文本内容
					PDFTextStripper stripper = new PDFTextStripper();
					// 设置按顺序输出
					stripper.setSortByPosition(true);
					stripper.setStartPage(i + 1);
					stripper.setEndPage(i + 1);
					String text = stripper.getText(document);
					System.out.println(text.trim());
					System.out.println("-=-=-=-=-=-=-=-=-=-=-=-=-");

					// 图片内容
					PDPage page = document.getPage(i);
					PDResources resources = page.getResources();
					if(resources != null){
						Iterable<COSName> cosNames = resources.getXObjectNames();
						if (cosNames != null) {
							Iterator<COSName> cosNamesIter = cosNames.iterator();
							while (cosNamesIter.hasNext()) {
								COSName cosName = cosNamesIter.next();
								if (resources.isImageXObject(cosName)) {
									PDImageXObject Ipdmage = (PDImageXObject) resources.getXObject(cosName);
									BufferedImage image = Ipdmage.getImage();
									FileOutputStream out = new FileOutputStream("D:\\ws\\out\\" + UUID.randomUUID() + ".png");
									try {
										ImageIO.write(image, "png", out);
									} catch (IOException e) {
									} finally {
										try {
											out.close();
										} catch (IOException e) {
										}
									}
								}
							}
						}
					}
				}
			}
		} catch (InvalidPasswordException e) {
		} catch (IOException e) {
		} finally {
			try {
				if (document != null) {
					document.close();
				}
				if (is != null) {
					is.close();
				}
			} catch (IOException e) {
			}
		}
	}

	//判断pdf文件是否是扫描件
	public static Boolean isScanFile(String filePath){
		Boolean scanFlag = false;
		try {
			File file = new File(filePath);
			PDDocument document = PDDocument.load(file);
			//读取pdf图片信息
			PDFRenderer renderer = new PDFRenderer(document);
			PDPageTree list = document.getPages();
			for(int i=0; i<list.getCount(); i++){
				BufferedImage image = renderer.renderImage(i);
				if(image != null){
					String content = executeOCR(image);
					if(StringUtils.isNotBlank(content)){
						scanFlag = true;
					}
				}
			}
			document.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return scanFlag;
	}

	// 执行OCR识别
	private static String executeOCR(BufferedImage targetImage) {
		String result = "";
		try {
			String tempImage = System.getProperty("user.dir")+ File.separator + System.nanoTime() + ".png";
			File tempFile = new File(tempImage);
			if (tempFile == null) {
				tempFile.mkdirs();
			}
			ImageIO.write(targetImage, "jpg", tempFile);
			File file = new File(tempImage);
			BufferedImage bi = ImageIO.read(file);

			ITesseract instance = new Tesseract();
			// 设置语言库位置
			instance.setDatapath("D:\\opt\\tessdata");
			// 设置语言,默认是英文（识别字母和数字），如果要识别中文(数字 + 中文），需要制定语言包
			instance.setLanguage("chi_sim");

			result = instance.doOCR(bi);
			file.delete();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	// 提取表格内容
	public static void getTableInfo(String filePath) throws IOException {
		File file = new File(filePath);
		List<List<String>> result = new ArrayList<List<String>>();
		try (PDDocument document = PDDocument.load(file)) {
			SpreadsheetExtractionAlgorithm sea = new SpreadsheetExtractionAlgorithm();
			PageIterator pi = new ObjectExtractor(document).extract();// 获取页面迭代器
			// 遍历所有页面
			while (pi.hasNext()) {
				Page page = pi.next();// 获取当前页
				List<Table> tables = sea.extract(page);// 解析页面上的所有表格
				// 进行去重
				tables = tables.stream().distinct().collect(Collectors.toList());
				// 遍历所有表格
				for (Table table : tables) {
					List<List<RectangularTextContainer>> rows = table.getRows();// 获取表格中的每一行
					// 遍历所有行并获取每个单元格信息
					for (List<RectangularTextContainer> cells : rows) {
						List<String> data = new ArrayList<String>();
						for (RectangularTextContainer content : cells) {
							System.out.print(content.getText().replace("\r", "、") + "|");
							//System.out.print(content.getText() + "|");
							data.add(content.getText());
						}
						System.out.println();
						result.add(data);
					}
				}
			}
		}
	}

    public static void main(String[] args) throws Exception {

//		getDocumentInformation("D:\\opt\\pdf\\国网汉阴县供电公司关于下达2023年度成本可控费用执行预算的通知.pdf");
//		getTextByPdfbox("D:\\opt\\pdf\\纪要-党委会纪要〔2023〕11号.pdf");
//		getTextByPdfbox("D:\\opt\\pdf\\国家能源局通知.pdf");
//		System.out.println(getTextByPdfbox("D:\\opt\\pdf\\数字化能力开放平台-二级检修操作方案20….pdf"));
//		System.out.println(getTextByPdfbox("D:\\opt\\pdf\\淘宝技术这十年，完整最终确认版.pdf"));
//		System.out.println(getTextByPdfbox("D:\\opt\\pdf\\6 软件著作权管理及版本一致性工作介绍.pdf"));
//		System.out.println(getTextByPdfbox("D:\\opt\\pdf\\国网汉阴县供电公司关于下达2023年度成本可控费用执行预算的通知.pdf"));
//		System.out.println(getTextByPdfbox("D:\\opt\\pdf\\三重一大系统对接协同办公系统接口文档.pdf"));
//		System.out.println(getTextByPdfbox("D:\\opt\\pdf\\6 软件著作权管理及版本一致性工作介绍.pdf"));
//		System.out.println(getTextByRegion("D:\\opt\\pdf\\6 软件著作权管理及版本一致性工作介绍.pdf"));
		//System.out.println(fetchTextByRegion("D:\\opt\\pdf\\三重一大系统对接协同办公系统接口文档.pdf", 0));

        //System.out.println(isContainImage("D:\\ws\\测试pdf.pdf"));
        //System.out.println(isContainImage("D:\\ws\\扫描件会议通知2021年14次.pdf"));
		//Path path = Paths.get("src/test/resources", "电子发票.pdf");

		//只有文字
//		System.out.println(isContainImage("D:\\opt\\pdf\\党组会议通知（2021年10月25日）38.pdf"));
//		getPdfImagesOne("D:\\opt\\pdf\\党组会议通知（2021年10月25日）38.pdf", "D:/opt/out/5/");
//		getPdfImagesTwo("D:\\opt\\pdf\\党组会议通知（2021年10月25日）38.pdf", "D:/opt/out/6/");

		//文字和图片
//		System.out.println(getTextByPdfbox("D:\\opt\\pdf\\针对需兼容低版本浏览器的PC端的规范.pdf"));
//		System.out.println(isContainImage("D:\\opt\\pdf\\针对需兼容低版本浏览器的PC端的规范.pdf"));
//		System.out.println(isScanFile("D:\\opt\\pdf\\针对需兼容低版本浏览器的PC端的规范.pdf"));
//		getPdfImagesOne("D:\\opt\\pdf\\针对需兼容低版本浏览器的PC端的规范.pdf", "D:/opt/out/3/");
//		getPdfImagesTwo("D:\\opt\\pdf\\针对需兼容低版本浏览器的PC端的规范.pdf", "D:/opt/out/4/");

		//只有图片
//		System.out.println(getTextByPdfbox("D:\\opt\\pdf\\国家能源局通知.pdf"));
//		System.out.println(isContainImage("D:\\opt\\pdf\\国家能源局通知.pdf"));
//		System.out.println(isScanFile("D:\\opt\\pdf\\国家能源局通知.pdf"));
//		getPdfImagesOne("D:\\opt\\pdf\\国家能源局通知.pdf", "D:/opt/out/1/");
//		getPdfImagesTwo("D:\\opt\\pdf\\国家能源局通知.pdf", "D:/opt/out/2/");

		//图片扫描件
//		System.out.println(getTextByPdfbox("D:\\opt\\pdf\\会议通知2021年14次扫描件.pdf"));
//		System.out.println(isContainImage("D:\\opt\\pdf\\会议通知2021年14次扫描件.pdf"));
//		System.out.println(isScanFile("D:\\opt\\pdf\\会议通知2021年14次扫描件.pdf"));
//		getPdfImagesOne("D:\\opt\\pdf\\会议通知2021年14次扫描件.pdf", "D:/opt/out/9/");
//		getPdfImagesTwo("D:\\opt\\pdf\\会议通知2021年14次扫描件.pdf", "D:/opt/out/10/");

		//电子发票
//		System.out.println(getTextByPdfbox("D:\\opt\\pdf\\电子发票.pdf"));
//		System.out.println(isContainImage("D:\\opt\\pdf\\电子发票.pdf"));
//		getPdfImagesOne("D:\\opt\\pdf\\电子发票.pdf", "D:/opt/out/7/");
//		getPdfImagesTwo("D:\\opt\\pdf\\电子发票.pdf", "D:/opt/out/8/");

		//一般扫描件
//		System.out.println(getTextByPdfbox("D:\\opt\\pdf\\纪要-党委会纪要〔2023〕11号.pdf"));
//		System.out.println(isContainImage("D:\\opt\\pdf\\纪要-党委会纪要〔2023〕11号.pdf"));
//		System.out.println(isScanFile("D:\\opt\\pdf\\纪要-党委会纪要〔2023〕11号.pdf"));
//		getPdfImagesOne("D:\\opt\\pdf\\纪要-党委会纪要〔2023〕11号.pdf", "D:/opt/out/11/");
//		getPdfImagesTwo("D:\\opt\\pdf\\纪要-党委会纪要〔2023〕11号.pdf", "D:/opt/out/12/");

//		System.out.println(getTextByPdfbox("D:\\opt\\pdf\\测试空白.pdf"));
//		System.out.println(isContainImage("D:\\opt\\pdf\\测试空白.pdf"));
//		System.out.println(isScanFile("D:\\opt\\pdf\\测试空白.pdf"));
//		getPdfImagesOne("D:\\opt\\pdf\\测试空白.pdf", "D:/opt/out/13/");
//		getPdfImagesTwo("D:\\opt\\pdf\\测试空白.pdf", "D:/opt/out/14/");

		//图片中误插入了文本
//		System.out.println(getTextByPdfbox("D:\\opt\\pdf\\会前征求意见材料3-关于做好2022年巩固脱贫成果服务乡村振兴重点工作的实施意见.pdf"));
//		System.out.println(getTextByPdfbox("D:\\opt\\pdf\\会前征求意见材料3-关于做好2022年巩固脱贫成果服务乡村振兴重点工作的实施意见5.pdf"));

		//手机扫描王软件生成
//		System.out.println(getTextByPdfbox("D:\\opt\\pdf\\手机扫描王文件1.pdf"));
//		System.out.println(isContainImage("D:\\opt\\pdf\\手机扫描王文件1.pdf"));
//		System.out.println(getTextByPdfbox("D:\\opt\\pdf\\手机扫描王文件2.pdf"));
//		System.out.println(isContainImage("D:\\opt\\pdf\\手机扫描王文件2.pdf"));


//		getTableInfo("D:\\opt\\pdf\\重庆电力公司\\01.2023年第15次董事长专题会通知.pdf");
//		getTableInfo("D:\\opt\\pdf\\重庆电力公司\\01.2023年第27次党委（扩大）会通知.pdf");

    }

}
