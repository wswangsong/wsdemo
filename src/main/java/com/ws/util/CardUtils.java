package com.ws.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 证件号码校验
 */
public class CardUtils {

    /**
     * 身份证号校验 （15位、18位）
     * 规则：6位地址编码+8位生日+3位顺序码+1位校验码（可能为数字或字符X）
     */
    public static boolean checkIdentityCard(String identityCode) {
        if (identityCode == null){
            return false;
        }
        identityCode = identityCode.toUpperCase();
        if (identityCode.length() != 15 && identityCode.length() != 18) {
            return false;
        }
        int y = 0, m = 0, d = 0;
        if (identityCode.length() == 15) {
            y = Integer.parseInt("19" + identityCode.substring(6, 8), 10);
            m = Integer.parseInt(identityCode.substring(8, 10), 10);
            d = Integer.parseInt(identityCode.substring(10, 12), 10);
        } else if (identityCode.length() == 18) {
            if (identityCode.indexOf("X") >= 0 && identityCode.indexOf("X") != 17) {
                return false;
            }
            char verifyBit = 0;
            int sum = (identityCode.charAt(0) - '0') * 7 + (identityCode.charAt(1) - '0') * 9 + (identityCode.charAt(2) - '0') * 10
                    + (identityCode.charAt(3) - '0') * 5 + (identityCode.charAt(4) - '0') * 8 + (identityCode.charAt(5) - '0') * 4
                    + (identityCode.charAt(6) - '0') * 2 + (identityCode.charAt(7) - '0') * 1 + (identityCode.charAt(8) - '0') * 6
                    + (identityCode.charAt(9) - '0') * 3 + (identityCode.charAt(10) - '0') * 7 + (identityCode.charAt(11) - '0') * 9
                    + (identityCode.charAt(12) - '0') * 10 + (identityCode.charAt(13) - '0') * 5 + (identityCode.charAt(14) - '0') * 8
                    + (identityCode.charAt(15) - '0') * 4 + (identityCode.charAt(16) - '0') * 2;
            sum = sum % 11;
            switch (sum) {
                case 0:
                    verifyBit = '1';
                    break;
                case 1:
                    verifyBit = '0';
                    break;
                case 2:
                    verifyBit = 'X';
                    break;
                case 3:
                    verifyBit = '9';
                    break;
                case 4:
                    verifyBit = '8';
                    break;
                case 5:
                    verifyBit = '7';
                    break;
                case 6:
                    verifyBit = '6';
                    break;
                case 7:
                    verifyBit = '5';
                    break;
                case 8:
                    verifyBit = '4';
                    break;
                case 9:
                    verifyBit = '3';
                    break;
                case 10:
                    verifyBit = '2';
                    break;

            }

            if (identityCode.charAt(17) != verifyBit) {
                return false;
            }
            y = Integer.parseInt(identityCode.substring(6, 10), 10);
            m = Integer.parseInt(identityCode.substring(10, 12), 10);
            d = Integer.parseInt(identityCode.substring(12, 14), 10);
        }

        int currentY = Calendar.getInstance().get(Calendar.YEAR);

        /*
         * if(isGecko){ currentY += 1900; }
         */
        if (y > currentY || y < 1870) {
            return false;
        }
        if (m < 1 || m > 12) {
            return false;
        }
        if (d < 1 || d > 31) {
            return false;
        }
        return true;
    }

    /**
     * 护照验证
     * 规则： G + 8位数字, P + 7位数字, S/D + 7或8位数字,等
     * 例： G12345678, P1234567
     */
    public static Boolean checkPassportCard(String card) {
        String reg = "^([a-zA-z]|[0-9]){5,17}$";
        if (card.matches(reg) == false) {
            //护照号码不合格
            return  false;
        } else {
            //校验通过
            return true;
        }
    }

    /**
     * 台湾居民来往大陆通行证
     * 规则： 新版8位或18位数字 或 旧版9位数字 + 英文字母 或 8位数字 + 英文字母
     * 样本： 12345678
     */
    public static Boolean checkTWCard(String card) {
        String reg = "^\\d{8}|^[a-zA-Z0-9]{10}|^[a-zA-Z0-9]{9}|^\\d{18}$";
        if (card.matches(reg) == false) {
            //台湾居民来往大陆通行证号码不合格
            return false;
        } else {
            //校验通过
            return true;
        }
    }

    /**
     * 港澳居民来往内地通行证
     * 规则： H/M + 10位或6位数字
     * 例：H1234567890
     */
    public static Boolean checkHKCard(String card) {
        String reg = "^([A-Z]\\d{6,10}(\\(\\w{1}\\))?)$";
        if (card.matches(reg) == false) {
            //港澳居民来往内地通行证号码不合格
            return false;
        } else {
            //校验通过
            return true;
        }
    }

    public static void main(String[] args) {
        System.out.println(checkIdentityCard("110110198001019719"));
        System.out.println(checkPassportCard("G12345678"));
        System.out.println(checkTWCard("12345678"));
        System.out.println(checkHKCard("H123456"));
    }

}


