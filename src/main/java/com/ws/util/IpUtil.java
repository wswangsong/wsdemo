package com.ws.util;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class IpUtil {

    private final static Logger logger = LoggerFactory.getLogger(IpUtil.class);

    public static String getIpAddr(HttpServletRequest request) {
        StringBuilder ipLogs = new StringBuilder();
        String ipAddress = request.getHeader("x-forwarded-for");
        ipLogs.append("**F5 x-forwarded-for IP=" + ipAddress);
        if (StringUtils.isEmpty(ipAddress) || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getHeader("Proxy-Client-IP");
            ipLogs.append("**Proxy-Client-IP=" + ipAddress);
        }
        if (StringUtils.isEmpty(ipAddress) || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getHeader("WL-Proxy-Client-IP");
            ipLogs.append("**WL-Proxy-Client-IP=" + ipAddress);
        }
        if (StringUtils.isEmpty(ipAddress) || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getHeader("X-Real-IP");
            ipLogs.append("**X-Real-IP=" + ipAddress);
        }
        if (StringUtils.isEmpty(ipAddress) || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getRemoteAddr();
            ipLogs.append("**RemoteAddr=" + ipAddress);
            if ("127.0.0.1".equals(ipAddress) || "\"0:0:0:0:0:0:0:1\"".equals(ipAddress)) {
                try {
                    InetAddress inet = InetAddress.getLocalHost();
                    ipAddress = inet.getHostAddress();
                } catch (UnknownHostException e) {
                    logger.error(e.getMessage(), e);
                }
            }
        }
        if (StringUtils.isNotEmpty(ipAddress) && ipAddress.indexOf(",") > 0) {
            ipAddress = ipAddress.split(",")[0];
        }
        ipLogs.append("**service client ip=" + ipAddress);
        logger.info(ipLogs.toString());

        return ipAddress;
    }
}
