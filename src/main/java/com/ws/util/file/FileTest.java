package com.ws.util.file;

import cn.hutool.core.util.CharsetUtil;
import cn.hutool.extra.compress.extractor.Extractor;
import com.google.common.base.Splitter;
import org.apache.commons.compress.archivers.ArchiveStreamFactory;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.junit.jupiter.api.Test;
import org.springframework.util.StreamUtils;

import java.io.*;
import java.net.URLDecoder;
import java.util.Map;

public class FileTest {

    @Test
    public void file() throws IOException {
        String fileUrl = "http://zb.ccoa.sgcc.com.cn/svr1/servlet/DownLoadServlet?MssDatabase=coa/mss/urgerMss1.nsf&AttachName=%E7%A4%BC%E8%A1%8C%E5%A4%A9%E4%B8%8B.pdf&FileUNID=62F58170CAB5DF854825847200225E47";
        //获取附件名称
        String fileTempName = getFileName(fileUrl);
        fileTempName = URLDecoder.decode(fileTempName, "UTF-8");
        System.out.println("------------附件名称:"+fileTempName);
        File file = getFileByUrlPost(fileUrl, fileTempName);
        //删除旧文件
        //org.apache.commons.io.FileUtils.del(file);
        //org.apache.tomcat.util.http.fileupload.FileUtils.forceDeleteOnExit(file);
    }

    public static String getFileName(String fileUrl){
        if(StringUtils.isBlank(fileUrl) || fileUrl.indexOf("?") == -1)return null;
        String params = fileUrl.substring(fileUrl.indexOf("?") + 1, fileUrl.length());
        Map<String, String> splitMap = Splitter.on("&").withKeyValueSeparator("=").split(params);
        if(null == splitMap || StringUtils.isBlank(splitMap.get("AttachName")))return null;
        return splitMap.get("AttachName");
    }

    /**
     * 附件下载调用方法
     * @param url
     * @param fileTempName
     * @return
     */
    public static File getFileByUrlPost(String url,String fileTempName ) {
        CloseableHttpClient httpClient = null;
        httpClient = HttpClients.createDefault();
        CloseableHttpResponse response = null;
        String fNameString = "d:/opt/file/"+fileTempName;
        File file = new File(fNameString);
//		File file = new File("C:/Users/Administrator/Desktop/test.pdf");
        InputStream is = null;
        OutputStream os = null;
        try {
            HttpPost httpPost = new HttpPost(url);
            // 发起请求 并返回请求的响应
            response = httpClient.execute(httpPost);
            // 获取响应对象
            org.apache.http.HttpEntity entity = response.getEntity();
            File folder = file.getParentFile();
            folder.mkdirs();
            is = entity.getContent();
            os = new FileOutputStream(file);
            //os = FileServiceScan.getFileOutputStream(file);
            StreamUtils.copy(is, os);
        }catch (ClientProtocolException e) {
            //LOGGER.error("文件下载出现异常1" + e.getMessage());
        }catch (IOException e) {
            //LOGGER.error("文件下载出现异常2" + e.getMessage());
        }catch(Exception e){
            //LOGGER.error("文件下载出现异常3" + e.getMessage());
        }finally {
            try {
                if (null != is) {
                    is.close();
                }
            }catch (IOException e) {
                //LOGGER.error("文件下载关闭流出现异常" + e.getMessage());
            }
            try {
                if (null != os) {
                    os.close();
                }
            }catch (IOException e) {
                //LOGGER.error("文件下载关闭流出现异常" + e.getMessage());
            }

            try {
                if (null != response) {
                    response.close();
                }
            }catch (IOException e) {
                //LOGGER.error("文件下载关闭流出现异常" + e.getMessage());
            }
            try {
                if (null != httpClient) {
                    httpClient.close();
                }
            }catch (IOException e) {
                //LOGGER.error("文件下载关闭流出现异常" + e.getMessage());
            }
        }
        return file;
    }

    @Test
    public void compress1() throws Exception {
        System.out.println(FileUtil.getFileFormat(new File("D:\\opt\\启明星公司决策材料自动化报送-sgcc-20231215.rar")));
        System.out.println(FileUtil.getFileFormat(new File("D:\\opt\\VCD015soft-v2015.rar")));
        System.out.println(FileUtil.getFileFormat(new File("D:\\opt\\rocketmq-all-4.5.1-bin-release.zip")));
    }

    @Test
    public void compress2(){
        //压缩文件
        final File file = cn.hutool.core.io.FileUtil.file("D:\\opt\\test.7z");
        cn.hutool.extra.compress.CompressUtil.createArchiver(CharsetUtil.CHARSET_UTF_8, ArchiveStreamFactory.SEVEN_Z, file)
                .add(cn.hutool.core.io.FileUtil.file("D:\\opt\\document\\"))
                .finish()
                .close();
    }

    @Test
    public void compress3(){
        //解压文件
        Extractor extractor = cn.hutool.extra.compress.CompressUtil.createExtractor(
                CharsetUtil.defaultCharset(),
                cn.hutool.core.io.FileUtil.file("D:\\opt\\test.7z"));
        extractor.extract(cn.hutool.core.io.FileUtil.file("D:\\opt\\test\\"));
    }

    @Test
    public void compress4() throws Exception {
        String inputFile1 = "D:\\opt\\rocketmq-all-4.5.1-bin-release.zip";
        String destDirPath1 = "D:\\opt\\zip";
        CompressUtil.unZip(inputFile1, destDirPath1, false);
    }

    @Test
    public void compress5() throws Exception {
//        String inputFile1 = "D:\\opt\\启明星公司决策材料自动化报送-sgcc-20231215.rar";
//        String destDirPath1 = "D:\\opt\\rar";
//        CompressUtil.unRar(inputFile1, destDirPath1, "sgcc");

        String inputFile2 = "D:\\opt\\VCD015soft-v2015.rar";
        String destDirPath2 = "D:\\opt\\rar";
        CompressUtil.unRar(inputFile2, destDirPath2, "");
    }

    @Test
    public void compress6() throws Exception {
        String inputFile3 = "D:\\opt\\启明星-重庆本部RPA与三重一大.7z";
        String destDirPath3 = "D:\\opt\\7z";
        CompressUtil.un7z(inputFile3, destDirPath3);
    }

    @Test
    public void compress7() throws Exception{
        CompressUtil.deCompress("D:\\opt\\rocketmq-all-4.5.1-bin-release.zip","D:\\opt\\test\\");
    }

    @Test
    public void compress11(){
        //压缩文件
        CompressUtil.compressByZip("D:\\opt\\test\\","D:\\opt\\test.zip");
    }

    @Test
    public void compress12(){
        //压缩文件
        CompressUtil.compressBy7z("D:\\opt\\test\\","D:\\opt\\test.7z");
    }

    @Test
    public void file1(){
        File[] files = cn.hutool.core.io.FileUtil.ls("D:\\opt\\zip\\rocketmq-all-4.5.1-bin-release");
        for(File file : files){
            System.out.println("绝对路径："+file.getAbsolutePath());
            System.out.println(file.getPath());
        }
    }
}
