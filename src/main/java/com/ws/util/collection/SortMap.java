package com.ws.util.collection;

import java.util.Map;

public class SortMap {
	
	public static String comparingByNum(Map<String, Object> map){
		return (String) map.get("num");
	}

	public static String comparingByName(Map<String, Object> map){
		return (String) map.get("name");
	}
	
	public static String comparingByTime(Map<String, Object> map){
		return (String) map.get("time");
	}

	public static String comparingByApprove(Map<String, Object> map){
		return (String) map.get("approve");
	}

}