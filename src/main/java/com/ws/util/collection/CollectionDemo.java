package com.ws.util.collection;

import com.alibaba.fastjson2.JSONObject;
import com.ws.bean.People;
import com.ws.bean.Person;
import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CollectionDemo {

    /**
     * 对List进行排序
     */
    @Test
    public void listSort(){

        String[] defined = {"玫瑰","牡丹","月季","荷花"};
        final List<String> definedOrder = Arrays.asList(defined);
        String[] toBeOrdered = {"玫瑰","牡丹","月季","荷花","月季","牡丹","月季","荷花","月季"};
        List<String> toBeOrderedList = Arrays.asList(toBeOrdered);
        CollectionUtil.sortedStringList(toBeOrderedList, definedOrder);
        System.out.println(toBeOrderedList);

        System.out.println("---------------------------------");

        List<Map<String, Object>> enrollmentGroups = new ArrayList<Map<String, Object>>();
        Map<String, Object> province1 = new HashMap<String, Object>();
        province1.put("meeting","党委会");
        province1.put("province","河北");
        Map<String, Object> province2 = new HashMap<String, Object>();
        province2.put("meeting","董事会会议");
        province2.put("province","北京");
        Map<String, Object> province3 = new HashMap<String, Object>();
        province3.put("meeting","总经理办公会");
        province3.put("province","天津");
        enrollmentGroups.add(province1);
        enrollmentGroups.add(province2);
        enrollmentGroups.add(province3);
        List<String> meetingOrder = Arrays.asList("董事会会议", "总经理办公会", "党委会");
        List<String> provinceOrder = Arrays.asList("北京", "天津", "河北", "山西", "内蒙", "辽宁", "吉林", "黑龙", "上海", "江苏", "浙江", "安徽", "福建", "江西", "山东", "河南", "湖北", "湖南", "广东", "广西", "海南", "重庆", "四川", "贵州", "云南", "西藏", "陕西", "甘肃", "青海", "宁夏", "新疆");
        //CollectionUtil.sortedMapList(enrollmentGroups, provinceOrder, "province");
        CollectionUtil.sortedMapList(enrollmentGroups, meetingOrder, "meeting");
        for(Map<String, Object> enrollment : enrollmentGroups){
            System.out.println(enrollment);
        }

        System.out.println("---------------------------------");
    }

    /**
     * 对List进行排序、转换成新List、去重
     */
    @Test
    public void listToList(){
        List<Map<String, Object>> userList = new ArrayList<Map<String, Object>>();
        Map<String,Object> user1 = new HashMap<String,Object>();
        user1.put("num","1");
        user1.put("name","caocao");
        user1.put("approve","曹操");
        user1.put("attendances","张辽，荀彧");
        user1.put("approveOrg","曹仁");
        user1.put("time","1993-03-01");
        Map<String,Object> user2 = new HashMap<String,Object>();
        user2.put("num","2");
        user2.put("name","sunquan");
        user2.put("approve","孙权");
        user2.put("attendances","周瑜，鲁肃");
        user2.put("approveOrg","吕蒙");
        user2.put("time","1994-04-01");
        Map<String,Object> user3 = new HashMap<String,Object>();
        user3.put("num","3");
        user3.put("name","liubei");
        user3.put("approve","刘备");
        user3.put("attendances","张飞，赵云");
        user3.put("approveOrg","关羽");
        user3.put("time","1994-04-02");
        Map<String,Object> user4 = new HashMap<String,Object>();
        user4.put("num","4");
        user4.put("name","sunquan");
        user4.put("approve","孙权");
        user4.put("attendances","周泰，黄盖");
        user4.put("approveOrg","陆逊");
        user4.put("time","1994-04-04");
        userList.add(user1);
        userList.add(user2);
        userList.add(user3);
        userList.add(user4);
        //排序(先按照name排序，再按照time倒序排序)
        List<Map<String, Object>> newUserList = userList.stream().sorted(Comparator.comparing(SortMap::comparingByName)
                .thenComparing(Comparator.comparing(SortMap::comparingByTime).reversed()))
                .collect(Collectors.toList());
        for(Map<String, Object> newUser : newUserList){
            System.out.println(newUser);
        }

        System.out.println("-----------");
        //对name属性去重
        List<Map<String, Object>> newNameUserList = userList.stream().collect(
                Collectors.collectingAndThen(Collectors.toCollection(
                        ()->new TreeSet<>(Comparator.comparing(m -> (String)m.get("name")))), ArrayList::new));
        for(Map<String, Object> newNameUser : newNameUserList){
            System.out.println(newNameUser);
        }
        System.out.println("-----------");
        //获取只有name属性的list
        List<String> nameList1 = userList.stream().map(m->(String)m.get("name")).collect(Collectors.toList());
        //获取只有name属性的list并去重
        List<String> nameList2 = userList.stream().map(m->(String)m.get("name")).distinct().collect(Collectors.toList());
        System.out.println(nameList1);
        System.out.println(nameList2);

        System.out.println("---------------------------------");
    }

    /**
     * 对map进行排序
     */
    @Test
    public void beanListToMapList(){
        List<Person> personList = new ArrayList<Person>();
        personList.add(new Person(1, "111"));
        personList.add(new Person(2, "222"));
        List<Map<String, ?>> maps = BeanMapUtil.beansToMaps(personList);
        System.out.println(maps);
        System.out.println("---------------------------------");

        List<Map<String, Object>> mapList = new ArrayList<>();
        mapList.add(new HashMap<String, Object>() {{
            put("name", "test11");
            put("addr", "北京");
        }});
        mapList.add(new HashMap<String, Object>() {{
            put("name", "test22");
            put("addr", "天津");
        }});
        List<Person> list = BeanMapUtil.mapsToBeans(mapList, Person.class);
        System.out.println(list);
    }

    /**
     * List转换成Map、对List进行统计
     */
    @Test
    public void listToMap(){
        //List集合转Map
        List<Person> personList = new ArrayList<Person>();
        personList.add(new Person(1, "111"));
        personList.add(new Person(2, "222"));
        Map<Integer, String> personMap = personList.stream().collect(Collectors.toMap(Person::getId, Person::getName));
        for(Integer key : personMap.keySet()){
            System.out.println("key="+key+" value="+personMap.get(key));
        }
        System.out.println("---------------------------------");

        List<Map<String, String>> list = new ArrayList<>();
        list.add(new HashMap<String, String>() {{
            put("name", "test11");
            put("type", "0");
        }});
        list.add(new HashMap<String, String>() {{
            put("name", "test22");
            put("type", "0");
        }});
        list.add(new HashMap<String, String>() {{
            put("name", "test33");
            put("type", "1");
        }});

        //list转map string
        Map<String, String> mapstring = list.stream().collect(Collectors.toMap(
                item -> item.get("type"),
                item -> item.get("name"),
                (o1, o2) -> o2)  //当key值一致时取后者
        );
        System.out.println("list转map string： " + JSONObject.toJSONString(mapstring));
        //执行结果： {"0":"test22","1":"test33"}

        //list转map map
        Map<String, Map<String, String>> map = list.stream().filter(Objects::nonNull).collect(Collectors.toMap(
                item -> item.get("type"),
                item -> item,
                (o1, o2) -> o2));
        System.out.println("list转map： " +JSONObject.toJSONString(map));
        //执行结果：  {"0":{"name":"test22","type":"0"},"1":{"name":"test33","type":"1"}}

        //list转 maplist
        Map<String, List<Map<String, String>>> map1 = list.stream().filter(Objects::nonNull).collect(Collectors.groupingBy(item -> item.get("type")));
        System.out.println("map分组： "+ JSONObject.toJSONString(map1));
        //执行结果： {"0":[{"name":"test11","type":"0"},{"name":"test22","type":"0"}],"1":[{"name":"test33","type":"1"}]}

        //统计个数
        Map<String, Long> map2 = list.stream().collect(Collectors.groupingBy(item -> item.get("type"), Collectors.counting()));
        System.out.println("统计个数： "+ JSONObject.toJSONString(map2));
        //执行结果：  {"0":2,"1":1}

    }

    /**
     * 筛选List
     * 筛选集合中所有country为吴的人组成的新集合
     */
    @Test
    public void listFilterList(){
        List<Map<String, Object>> userList = new ArrayList<Map<String, Object>>();
        Map<String,Object> user1 = new HashMap<String,Object>();
        user1.put("num","1");
        user1.put("name","曹操");
        user1.put("country","魏");
        user1.put("time","1993-03-01");
        Map<String,Object> user2 = new HashMap<String,Object>();
        user2.put("num","2");
        user2.put("name","吕蒙");
        user2.put("country","吴");
        user2.put("time","1994-04-01");
        Map<String,Object> user3 = new HashMap<String,Object>();
        user3.put("num","3");
        user3.put("name","刘备");
        user3.put("country","蜀");
        user3.put("time","1994-04-02");
        Map<String,Object> user4 = new HashMap<String,Object>();
        user4.put("num","4");
        user4.put("name","孙权");
        user4.put("country","吴");
        user4.put("time","1994-04-04");
        userList.add(user1);
        userList.add(user2);
        userList.add(user3);
        userList.add(user4);

        System.out.println("---------------------------------");
        List<Map<String, Object>> resultList = userList.stream().filter(user -> Objects.equals(user.get("country"),"吴")).collect(Collectors.toList());
        resultList.stream().forEach(map ->{
            System.out.println(map.get("name"));
        });

        System.out.println("---------------------------------");
    }

    /**
     * 对map进行排序
     */
    @Test
    public void mapSort(){
        Map<String, String> param = new HashMap<String, String>();
        param.put("type", "1");
        param.put("user_mobile", "2");
        param.put("full_name", "3");
        param.put("id_no", "4");
        param.put("secret", "5");
        System.out.println(CollectionUtil.mapSortedByKey(param));
        System.out.println("---------------------------------");
        System.out.println(CollectionUtil.mapSortedByKeyUseTreeMap(param));
        System.out.println("---------------------------------");
    }

}
