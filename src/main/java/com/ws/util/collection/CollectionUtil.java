package com.ws.util.collection;

import com.ws.bean.Person;
import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CollectionUtil {

    /**
     * @Description：对List<String>进行排序
     * @param sourceList 排序之前的List
     * @param orderList 固定顺序的List
     */
    public static void sortedStringList(List<String> sourceList, List<String> orderList){
        Collections.sort(sourceList, new Comparator<String>(){
            @Override
            public int compare(String o1, String o2) {
                int io1 = orderList.indexOf(o1);
                int io2 = orderList.indexOf(o2);
                return io1 - io2;
            }
        });
    }

    /**
     * @Description：对List<Map<String, Object>>进行排序
     * @param sourceList 排序之前的List
     * @param orderList 固定顺序的List
     * @param key 按照排序的map的kay
     */
    public static void sortedMapList(List<Map<String, Object>> sourceList, List<String> orderList, String key) {
        Function<Map<String, Object>, String> getMap = (oneGroup) -> {
            String itemMap;
            String temp = (String) oneGroup.get(key);

            if (temp.contains(",")) {
                String[] provinces = temp.replaceAll("\\s*", "").split(",");
                itemMap = provinces[0].substring(0, provinces[0].length());

                for (int i = 0; i < provinces.length; i++) {
                    if (orderList.indexOf(itemMap) == -1) {
                        itemMap = provinces[i].substring(0, provinces[i].length());
                    }else if (orderList.indexOf(itemMap) > orderList.indexOf(provinces[i].substring(0, 2))) {
                        itemMap = provinces[i].substring(0, provinces[i].length());
                    }
                }
            } else {
                itemMap = temp.substring(0, temp.length());
            }

            return itemMap;
        };

        Collections.sort(sourceList, new Comparator<Map<String, Object>>() {
            @Override
            public int compare(Map<String, Object> o1, Map<String, Object> o2) {
                String o1_map = getMap.apply(o1); //o1中最优先的元素
                String o2_map = getMap.apply(o2); //o2中最优先的元素

                int o1_index = orderList.indexOf(o1_map);
                int o2_index = orderList.indexOf(o2_map);

                if (o1_index == -1) {
                    return 1;
                }

                if (o2_index == -1) {
                    return -1;
                }

                return o1_index - o2_index;
            }
        });
    }

    /**
     * @Description：对Map<String, String>进行排序
     */
    public static Map mapSortedByKey(Map<String, String> param) {
        HashMap<String, String> map = new HashMap<String, String>();
        ArrayList<String> keyList = new ArrayList<>(param.keySet());
        Collections.sort(keyList);
        for (int i = 0; i < keyList.size(); i++) {
            String key = keyList.get(i);
            map.put(key, param.get(key));
        }
        return map;
    }

    /**
     * @Description：对Map<String, String>进行排序
     */
    public static TreeMap<String, String> mapSortedByKeyUseTreeMap(Map<String, String> param) {
        TreeMap<String, String> treeMap = new TreeMap<String, String>(param);
        return treeMap;
    }

}
