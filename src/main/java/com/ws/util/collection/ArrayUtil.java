package com.ws.util.collection;

/*
 * 数组操作工具类
 */
public class ArrayUtil {

    //获取数组某元素的下标
    public static int printArray(String[] array,String value){
        for(int i = 0; i<array.length; i++){
            if(array[i].equals(value)){
                return i;
            }
        }
        return -1;//当if条件不成立时，默认返回一个负数值-1
    }

    //获取数组某元素的下一个元素
    public static String nextItem(String[] array,String value){
        for(int i = 0; i<array.length; i++){
            if(array[i].equals(value)){
                return array[i+1];
            }
        }
        return "";
    }

    public static void main(String[] args){

        String s = "aa,bb,cc";
        String[] ss = s.split(",");
        int index = printArray(ss,"bb");
        System.out.println(index);
        String next = nextItem(ss,"bb");
        System.out.println(next);

    }
}
