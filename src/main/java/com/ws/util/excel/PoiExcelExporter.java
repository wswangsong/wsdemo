package com.ws.util.excel;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFCell;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Component
public class PoiExcelExporter {

    private static Logger LOGGER = LoggerFactory.getLogger(PoiExcelExporter.class);

    public void exportMulTitle(String tableName, List<List<String>> titleList, List<List<String>> contentList,
                               List<CellRangeAddress> cellRangeAddressList, HttpServletResponse response) {

        LOGGER.info("------------excel export start-----------");
        try (SXSSFWorkbook wb = new SXSSFWorkbook(); OutputStream output = response.getOutputStream()) {
            // 行号 一行一行添加
            int rowNum = 0;

            CellStyle style = createTitleCellStyle(wb);
            SXSSFSheet hssfSheet = wb.createSheet("sheet1");
            // 先处理标题头
            for (List<String> title : ListUtils.emptyIfNull(titleList)) {
                setSheetTitle(hssfSheet, title, style, rowNum);
                rowNum++;
                System.out.println("title rowNum " + rowNum);
            }
            // 再处理 cellRangeAddressList
            if(CollectionUtils.isNotEmpty(cellRangeAddressList)){
                cellRangeAddressList.forEach(hssfSheet::addMergedRegion);
            }

            if (CollectionUtils.isNotEmpty(contentList)) {
                // 再处理内容
                for (List<String> content : ListUtils.emptyIfNull(contentList)) {
                    setSheetContent(hssfSheet, content, rowNum);
                    rowNum++;
                    System.out.println("content rowNum " + rowNum);
                }
                LOGGER.info("------------excel export complied-----------");
            }

            setResponseExcel(response, tableName);
            wb.write(output);
            output.flush();
        } catch (Exception e) {
            LOGGER.error("export wrong: ", e);
            throw new RuntimeException(e);
        }
    }

    private static void setSheetTitle(SXSSFSheet sheet, List<String> titleList, CellStyle style, int rownum) {
        SXSSFRow row = sheet.createRow(rownum);
        AtomicInteger i = new AtomicInteger();
        for (String title : ListUtils.emptyIfNull(titleList)) {
            SXSSFCell cell = row.createCell(i.getAndIncrement());
            cell.setCellValue(title);
            cell.setCellStyle(style);
        }
    }

    private static void setSheetContent(SXSSFSheet sheet, List<String> titleList, int rownum) {
        SXSSFRow row = sheet.createRow(rownum);
        AtomicInteger i = new AtomicInteger();
        for (String title : ListUtils.emptyIfNull(titleList)) {
            SXSSFCell cell = row.createCell(i.getAndIncrement());
            cell.setCellValue(title);
        }
    }

    private static CellStyle createTitleCellStyle(SXSSFWorkbook wb) {
        CellStyle style = wb.createCellStyle();
        Font font = wb.createFont();
        font.setBold(true);//粗体显示style.setFont(font);//单元格样式cell1.setCellStyle(style);//给cell1这个单元格设置样式
        style.setFont(font);
        style.setAlignment(HorizontalAlignment.CENTER);//水平居中
        style.setVerticalAlignment(VerticalAlignment.CENTER);//垂直居中
        style.setBorderTop(BorderStyle.DOUBLE);//上边框
        style.setBorderBottom(BorderStyle.THIN);//下边框
        style.setBorderLeft(BorderStyle.THICK);//左边框
        style.setBorderRight(BorderStyle.MEDIUM);//右边框
        style.setTopBorderColor(IndexedColors.BLACK.getIndex());//上边框颜色
        style.setBottomBorderColor(IndexedColors.BLACK.getIndex());//下边框颜色
        style.setLeftBorderColor(IndexedColors.BLACK.getIndex());//左边框颜色
        style.setRightBorderColor(IndexedColors.BLACK.getIndex());//右边框颜色
        return style;
    }

    private static void setResponseExcel(HttpServletResponse response, String tableName)
            throws UnsupportedEncodingException {
        //获取输出流
        response.reset();
        //设置响应的编码
        response.setContentType("application/x-download");//下面三行是关键代码，处理乱码问题
        response.setCharacterEncoding("utf-8");
        //设置浏览器响应头对应的Content-disposition
        String fileName = URLEncoder.encode(tableName, "UTF-8");
        response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".xls");
    }

}
