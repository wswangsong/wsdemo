package com.ws.util.excel;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

/**
 * 导出Excel文件
 */
public class ExcelExportUtil {
    // 定制浮点数格式
    public static final String NUMBER_FORMAT = "#,##0.00";
    public static final String FORMAT_NUMBER = "#,##0.0";
    private OutputStream out = null;
    private SXSSFWorkbook workbook = null;
    private SXSSFSheet sheet = null;
    private SXSSFRow row = null;
    private CellStyle titleStyle = null;

    public ExcelExportUtil() {
        this.workbook = new SXSSFWorkbook(1000);
        this.titleStyle = getTitleCellStyle();
    }

    /**
     * 初始化Excel
     */
    public ExcelExportUtil(OutputStream out, String sheetName) {
        this.out = out;
        this.workbook = new SXSSFWorkbook(1000);
        this.sheet = (SXSSFSheet) workbook.createSheet(sheetName);
        this.titleStyle = getTitleCellStyle();
    }

    public void setSheet(int sheetNum, String sheetTitle){
        // 生成一个表格
        this.sheet = (SXSSFSheet) workbook.createSheet();
        this.workbook.setSheetName(sheetNum, sheetTitle);
        // 设置表格默认列宽度为20个字节
        this.sheet.setDefaultColumnWidth((short) 20);
    }

    public SXSSFSheet getSheet(){
        return this.sheet;
    }

    /**
     * 导出Excel文件
     * @throws IOException
     */
    public void export(int[] columnWidth) throws FileNotFoundException, IOException {
        try {
            //调整列宽
            for(int i=0; i<columnWidth.length; i++) {
                //sheet.autoSizeColumn((short)i);  使用此种方法自动调节的话，会导致中文列的调整有问题，因此采用每行调整的方法
                sheet.setColumnWidth(i, columnWidth[i] * 256);
            }
            workbook.write(out);
            out.flush();
            out.close();
            //清理临时文件
            workbook.dispose();
        } catch (FileNotFoundException e) {
            throw new IOException(" 生成导出Excel文件出错! ");
        } catch (IOException e) {
            throw new IOException(" 写入Excel文件出错! ");
        }
    }

    /**
     * 导出Excel文件
     * @throws IOException
     */
    public void export(Integer[] columnWidth) throws FileNotFoundException, IOException {
        try {
            //调整列宽
            for(Integer i=0; i<columnWidth.length; i++) {
                //sheet.autoSizeColumn((short)i);  使用此种方法自动调节的话，会导致中文列的调整有问题，因此采用每行调整的方法
                sheet.setColumnWidth(i, columnWidth[i] * 256);
            }
            workbook.write(out);
            out.flush();
            out.close();
            //清理临时文件
            workbook.dispose();
        } catch (FileNotFoundException e) {
            throw new IOException(" 生成导出Excel文件出错! ");
        } catch (IOException e) {
            throw new IOException(" 写入Excel文件出错! ");
        }
    }

    /**
     * 增加一行
     * @param index 行号
     */
    public void createRow(int index) {
        this.row = (SXSSFRow) this.sheet.createRow(index);
    }

    /**
     * 获取单元格的值
     * @param index 列号
     */
    public String getCell(int index) {
        Cell cell = this.row.getCell((short) index);
        String cellValue = "";
//        //第一种方式
//        DataFormatter dataFormatter = new DataFormatter();
//        cellValue = dataFormatter.formatCellValue(cell);
        //第二种方式
        if (cell != null) {
            CellType cellType = cell.getCellType();
            switch (cellType) {
                case FORMULA:
                    cellValue = "FORMULA";
                    break;
                case NUMERIC:{
                    double numericCellValue = cell.getNumericCellValue();//获取数字类型的单元格中的数据NUMERIC
                    //stripTrailingZeros()：去除末尾多余的0，toPlainString()：输出时不用科学计数法
                    String s = new BigDecimal(String.valueOf(numericCellValue)).stripTrailingZeros().toPlainString();
                    cellValue = s.trim().replaceAll(" ", "").replaceAll("/t", "").replaceAll("	", "");
                    break;
                }
                case STRING:
                    cellValue = cell.getRichStringCellValue().getString();
                    break;
                case BLANK:
                    cellValue = "";
                    break;
                case BOOLEAN:
                    cellValue = cell.getBooleanCellValue() ? "TRUE" : "FALSE";
                    break;
                case ERROR:
                    cellValue = FormulaError.forInt(cell.getErrorCellValue()).getString();
                    break;
                default:
                    cellValue = "";
                    break;
            }
        } else {
            return "";
        }
        return cellValue;
    }

    /**
     * 设置单元格
     * @param index 列号
     * @param value 单元格填充值
     */
    public void setCell(int index, String value) {
        Cell cell = this.row.createCell((short) index);
        cell.setCellValue(value);
        CellStyle cellStyle = getCellStyle();
        cell.setCellStyle(cellStyle);
        //this.row.setHeightInPoints(25);//设置行高
        //this.row.setHeight((short)25);//设置行高
    }

    /**
     * 设置单元格
     * @param index 列号
     * @param value 单元格填充值
     */
    public void setCell(int index, Integer value) {
        Cell cell = this.row.createCell((short) index);
        if(value != null) {
            cell.setCellValue(value);
        }
        CellStyle cellStyle = getCellStyle();
        cell.setCellStyle(cellStyle);
    }

    /**
     * 设置单元格
     * @param index 列号
     * @param value 单元格填充值
     */
    public void setCell(int index, Float value) {
        Cell cell = this.row.createCell((short) index);
        if(value != null) {
            cell.setCellValue(value);
        }
        CellStyle cellStyle = getCellStyle(); // 建立新的cell样式
        DataFormat format = workbook.createDataFormat();
        cellStyle.setDataFormat(format.getFormat(NUMBER_FORMAT)); // 设置cell样式为定制的浮点数格式
        cell.setCellStyle(cellStyle); // 设置该cell浮点数的显示格式
    }

    /**
     * 设置单元格
     * @param index 列号
     * @param value 单元格填充值
     * @param formatvalue 自定义格式
     */
    public void setCell(int index, Float value, String formatvalue) {
        Cell cell = this.row.createCell((short) index);
        if(value != null) {
            cell.setCellValue(value);
        }
        CellStyle cellStyle = getCellStyle(); // 建立新的cell样式
        DataFormat format = workbook.createDataFormat();
        cellStyle.setDataFormat(format.getFormat(formatvalue)); // 设置cell样式为定制的浮点数格式
        cell.setCellStyle(cellStyle); // 设置该cell浮点数的显示格式
    }

    /**
     * 设置单元格
     * @param index 列号
     * @param value 单元格填充值
     */
    public void setCell(int index, Double value) {
        Cell cell = this.row.createCell((short) index);
        if(value != null) {
            cell.setCellValue(value);
        }
        CellStyle cellStyle = getCellStyle(); // 建立新的cell样式
        DataFormat format = workbook.createDataFormat();
        cellStyle.setDataFormat(format.getFormat(NUMBER_FORMAT)); // 设置cell样式为定制的浮点数格式
        cell.setCellStyle(cellStyle); // 设置该cell浮点数的显示格式
    }

    /**
     * 设置单元格
     * @param index 列号
     * @param value 单元格填充值
     */
    public void setCell(int index, Double value, String formatstring) {
        Cell cell = this.row.createCell((short) index);
        if(value != null) {
            cell.setCellValue(value);
        }
        CellStyle cellStyle = getCellStyle(); // 建立新的cell样式
        DataFormat format = workbook.createDataFormat();
        cellStyle.setDataFormat(format.getFormat(formatstring)); // 设置cell样式为定制的浮点数格式
        cell.setCellStyle(cellStyle); // 设置该cell浮点数的显示格式
    }

    /**
     * 设置单元格
     * @param index 列号
     * @param value 单元格填充值
     */
    public void setCell(int index, Calendar value, String format) {
        Cell cell = this.row.createCell((short) index);
        cell.setCellValue(value.getTime());
        CellStyle cellStyle = getCellStyle(); // 建立新的cell样式
        cellStyle.setDataFormat((short)BuiltinFormats.getBuiltinFormat(format)); // 设置cell样式为定制的日期格式
        cell.setCellStyle(cellStyle); // 设置该cell日期的显示格式
    }

    /**
     * 设置Excel的标题
     * @param title 标题
     */
    public void setTitle(String title[]) {
        this.createRow(0);
        for(int i=0;i < title.length;i++) {
            this.setTitleCell(i, title[i]);   // 登录日期
        }
    }

    /**
     * 设置标题的单元格
     * @param index 列号
     * @param value 单元格填充值
     */
    private void setTitleCell(int index, String value) {
        Cell cell = this.row.createCell((short) index);
        cell.setCellStyle(titleStyle);
        cell.setCellValue(value);
        this.row.setHeightInPoints(25);//设置行高
        //this.row.setHeight((short)25);//设置行高
    }

    /**
     * 设置标题的单元格
     * @param index 列号
     * @param value 单元格填充值
     */
    private void setMulTitleCell(int index, String value) {
        Cell cell = this.row.createCell((short) index);
        cell.setCellStyle(getBorderCellStyle());
        cell.setCellValue(value);
    }

    /**
     * 设置标题单元格的格式
     */
    private CellStyle getTitleCellStyle() {
        CellStyle cellStyle = this.workbook.createCellStyle();
        // 设置字体
        Font font = this.workbook.createFont();
        font.setBold(true);//设置加粗
        // 设置字体大小
        font.setFontHeightInPoints((short)9);
        cellStyle.setFont(font);
        // 下边框
        cellStyle.setBorderBottom(BorderStyle.THIN);
        // 左边框
        cellStyle.setBorderLeft(BorderStyle.THIN);
        // 上边框
        cellStyle.setBorderTop(BorderStyle.THIN);
        // 右边框
        cellStyle.setBorderRight(BorderStyle.THIN);
        // 水平对齐方式
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        // 垂直对齐方式
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        cellStyle.setLocked(false);
        // 设置锁定单元格
        cellStyle.setLocked(true);
        // 设置冻结某行某列
        // sheet.createFreezePane(cell.getColumnIndex(),0);
        // 设置背景颜色
        cellStyle.setFillBackgroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        return cellStyle;
    }

    /**
     * 设置普通单元格的格式
     */
    private CellStyle getCellStyle() {
        CellStyle cellStyle = this.workbook.createCellStyle();
        // 设置字体
        Font font = this.workbook.createFont();
        // 设置字体大小
        font.setFontHeightInPoints((short)8);
        cellStyle.setFont(font);
        // 下边框
        cellStyle.setBorderBottom(BorderStyle.THIN);
        // 左边框
        cellStyle.setBorderLeft(BorderStyle.THIN);
        // 上边框
        cellStyle.setBorderTop(BorderStyle.THIN);
        // 右边框
        cellStyle.setBorderRight(BorderStyle.THIN);
        // 水平对齐方式
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        // 垂直对齐方式
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        // 设置锁定单元格
        cellStyle.setLocked(false);
        return cellStyle;
    }

    /**
     * 设置多样单元格的格式
     */
    private CellStyle getBorderCellStyle() {
        CellStyle style = this.workbook.createCellStyle();
        style.setAlignment(HorizontalAlignment.CENTER);//水平居中
        style.setVerticalAlignment(VerticalAlignment.CENTER);//垂直居中
        style.setBorderTop(BorderStyle.MEDIUM);
        style.setBorderBottom(BorderStyle.MEDIUM);
        style.setBorderLeft(BorderStyle.MEDIUM);
        style.setBorderRight(BorderStyle.MEDIUM);
        return style;
    }

    public static Object nvl(Object in, Object nvl) {
        return in == null?nvl:in;
    }

    public void write(OutputStream output) throws IOException{
        workbook.write(output);
    }

    public void dispose() {
        workbook.dispose();
    }

    // 参考案例
    public static void exportSimple() {
        File file = new File("E:\\test\\a.xlsx");
        FileOutputStream outputstrem = null;
        try {
            outputstrem = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        ExcelExportUtil excelWriter = new ExcelExportUtil(outputstrem, "决策概况");
        String[] title = new String[]{"序号", "单位名称", "决策制度", "决策事项", "累计会议数", "股东会", "党委会",
                "董事会会议", "董事长办公会", "总经理办公会", "其他会议", "累计议题数", "开始时间", "结束时间"};
        excelWriter.setTitle(title);
        for(int i=0; i<2000; i++){
            int c=0;
            excelWriter.createRow(i);
            excelWriter.setCell(c++,i);//序号
            excelWriter.setCell(c++, "companyName" + i);//单位名称
            excelWriter.setCell(c++, "regulation" + i);//决策制度
            excelWriter.setCell(c++, "0.001");//决策事项
            excelWriter.setCell(c++, "meeting" + i);//会议总数
            excelWriter.setCell(c++, "gdMeeting" + i);//股东会
            excelWriter.setCell(c++, "dzMeeting" + i);//党委会
            excelWriter.setCell(c++, "dsMeeting" + i);//董事会会议
            excelWriter.setCell(c++, "dsbMeeting" + i);//董事长办公会
            excelWriter.setCell(c++, "jlMeeting" + i);//总经理办公会
            excelWriter.setCell(c++, "otherMeeting" + i);//其他会议
            excelWriter.setCell(c++, "subject" + i);//议题总数
            excelWriter.setCell(c++, "startTime");
            excelWriter.setCell(c++, "endTime");
        }
        int[] columnWidth = new int[]{5, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20};
        try {
            excelWriter.export(columnWidth);
            outputstrem.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    //设置表头
    public void setMergedRegionTitle() {
        //单位名称、本年度（会议数、议题数）、第1季度（会议数、议题数）、第2季度（会议数、议题数）、第3季度（会议数、议题数）、第4季度（会议数、议题数）、最新会议时间
        //复合表头
        List<CellRangeAddress> cellRangeAddressList = new ArrayList<>();
        cellRangeAddressList.add(new CellRangeAddress(0,1,0,0));
        cellRangeAddressList.add(new CellRangeAddress(0,0,1,2));
        cellRangeAddressList.add(new CellRangeAddress(0,0,3,4));
        cellRangeAddressList.add(new CellRangeAddress(0,0,5,6));
        cellRangeAddressList.add(new CellRangeAddress(0,0,7,8));
        cellRangeAddressList.add(new CellRangeAddress(0,0,9,10));
        cellRangeAddressList.add(new CellRangeAddress(0,1,11,11));

        //复合表头值
        List<String> headList = new ArrayList<String>();
        List<String> titleList = new ArrayList<String>();

        headList.add("单位名称");
        titleList.add("");

        headList.add("本年度");
        headList.add("");
        titleList.add("会议数");
        titleList.add("议题数");

        headList.add("第1季度");
        headList.add("");
        titleList.add("会议数");
        titleList.add("议题数");

        headList.add("第2季度");
        headList.add("");
        titleList.add("会议数");
        titleList.add("议题数");

        headList.add("第3季度");
        headList.add("");
        titleList.add("会议数");
        titleList.add("议题数");

        headList.add("第4季度");
        headList.add("");
        titleList.add("会议数");
        titleList.add("议题数");

        headList.add("最新会议时间");
        titleList.add("");
        String[] head = headList.toArray(new String[headList.size()]);
        String[] title = titleList.toArray(new String[titleList.size()]);

        //设置复合表头
        cellRangeAddressList.forEach(this.sheet::addMergedRegion);

        this.createRow(0);
        for(int i=0;i < head.length;i++) {
            this.setMulTitleCell(i, head[i]);
        }
        this.createRow(1);
        for(int i=0;i < title.length;i++) {
            this.setMulTitleCell(i, title[i]);
        }
    }

    //设置表头
    public static List<Map<String, Object>> setMergedRegionData() {
        List<Map<String, Object>> allList = new ArrayList<Map<String, Object>>();

        List<Map<String, Object>> sons1 = new ArrayList<Map<String, Object>>();
        Map<String, Object> son11 = new HashMap<String, Object>();
        son11.put("mNum", "10");
        son11.put("sNum", "20");
        son11.put("mOne", "2");
        son11.put("sOne", "4");
        son11.put("mTwo", "1");
        son11.put("sTwo", "2");
        son11.put("mThree", "4");
        son11.put("sThree", "8");
        son11.put("mFour", "3");
        son11.put("sFour", "6");
        son11.put("nmTime", "2022-10-01");
        Map<String, Object> son12 = new HashMap<String, Object>();
        son12.put("mNum", "15");
        son12.put("sNum", "30");
        son12.put("mOne", "5");
        son12.put("sOne", "10");
        son12.put("mTwo", "2");
        son12.put("sTwo", "4");
        son12.put("mThree", "4");
        son12.put("sThree", "12");
        son12.put("mFour", "4");
        son12.put("sFour", "4");
        son12.put("nmTime", "2022-10-05");
        sons1.add(son11);
        sons1.add(son12);
        Map<String, Object> data1 = new HashMap<String, Object>();
        data1.put("name", "重庆电力");
        data1.put("sons", sons1);
        allList.add(data1);

        List<Map<String, Object>> sons2 = new ArrayList<Map<String, Object>>();
        Map<String, Object> son21 = new HashMap<String, Object>();
        son21.put("mNum", "8");
        son21.put("sNum", "16");
        son21.put("mOne", "1");
        son21.put("sOne", "2");
        son21.put("mTwo", "3");
        son21.put("sTwo", "6");
        son21.put("mThree", "2");
        son21.put("sThree", "4");
        son21.put("mFour", "2");
        son21.put("sFour", "4");
        son21.put("nmTime", "2022-10-02");
        sons2.add(son21);
        Map<String, Object> data2 = new HashMap<String, Object>();
        data2.put("name", "四川电力");
        data2.put("sons", sons2);
        allList.add(data2);

        List<Map<String, Object>> sons3 = new ArrayList<Map<String, Object>>();
        Map<String, Object> son31 = new HashMap<String, Object>();
        son31.put("mNum", "10");
        son31.put("sNum", "20");
        son31.put("mOne", "2");
        son31.put("sOne", "4");
        son31.put("mTwo", "1");
        son31.put("sTwo", "2");
        son31.put("mThree", "4");
        son31.put("sThree", "8");
        son31.put("mFour", "3");
        son31.put("sFour", "6");
        son31.put("nmTime", "2022-10-01");
        Map<String, Object> son32 = new HashMap<String, Object>();
        son32.put("mNum", "15");
        son32.put("sNum", "30");
        son32.put("mOne", "5");
        son32.put("sOne", "10");
        son32.put("mTwo", "2");
        son32.put("sTwo", "4");
        son32.put("mThree", "4");
        son32.put("sThree", "12");
        son32.put("mFour", "4");
        son32.put("sFour", "4");
        son32.put("nmTime", "2022-10-02");
        Map<String, Object> son33 = new HashMap<String, Object>();
        son33.put("mNum", "15");
        son33.put("sNum", "30");
        son33.put("mOne", "5");
        son33.put("sOne", "10");
        son33.put("mTwo", "2");
        son33.put("sTwo", "4");
        son33.put("mThree", "4");
        son33.put("sThree", "12");
        son33.put("mFour", "4");
        son33.put("sFour", "4");
        son33.put("nmTime", "2022-10-03");
        sons3.add(son31);
        sons3.add(son32);
        sons3.add(son33);
        Map<String, Object> data3 = new HashMap<String, Object>();
        data3.put("name", "河北电力");
        data3.put("sons", sons3);
        allList.add(data3);

        return allList;
    }

    public static void exportMergedRegion() {
        File file = new File("E:\\test\\b.xlsx");
        FileOutputStream outputstrem = null;
        try {
            outputstrem = new FileOutputStream(file);

            ExcelExportUtil excelWriter = new ExcelExportUtil(outputstrem, "测试数据");

            //设置表头
            excelWriter.setMergedRegionTitle();
            //准备数据
            List<Map<String, Object>> allList = setMergedRegionData();

            int rowNo = 1;
            for (int i = 0; i < allList.size(); i++) {
                int startNo = rowNo + 1;//开始写数据行号
                Map<String, Object> obj = allList.get(i);
                List<Map<String, Object>> datas = (List) obj.get("sons");
                for(int j = 0; j < datas.size(); j++){
                    Map<String, Object> data = datas.get(j);
                    excelWriter.createRow(++rowNo);
                    int c = 0;
                    excelWriter.setCell(c++, String.valueOf(excelWriter.nvl(obj.get("name"), "")));//单位名称
                    excelWriter.setCell(c++, String.valueOf(excelWriter.nvl(data.get("mNum"), "")));//本年度会议数
                    excelWriter.setCell(c++, String.valueOf(excelWriter.nvl(data.get("sNum"), "")));//本年度议题数
                    excelWriter.setCell(c++, String.valueOf(excelWriter.nvl(data.get("mOne"), "")));//第一季度会议数
                    excelWriter.setCell(c++, String.valueOf(excelWriter.nvl(data.get("sOne"), "")));//第一季度议题数
                    excelWriter.setCell(c++, String.valueOf(excelWriter.nvl(data.get("mTwo"), "")));//第二季度会议数
                    excelWriter.setCell(c++, String.valueOf(excelWriter.nvl(data.get("sTwo"), "")));//第二季度议题数
                    excelWriter.setCell(c++, String.valueOf(excelWriter.nvl(data.get("mThree"), "")));//第三季度会议数
                    excelWriter.setCell(c++, String.valueOf(excelWriter.nvl(data.get("sThree"), "")));//第三季度议题数
                    excelWriter.setCell(c++, String.valueOf(excelWriter.nvl(data.get("mFour"), "")));//第四季度会议数
                    excelWriter.setCell(c++, String.valueOf(excelWriter.nvl(data.get("sFour"), "")));//第四季度议题数
                    excelWriter.setCell(c++, String.valueOf(excelWriter.nvl(data.get("nmTime"), "")));//最新会议时间
                }

                if(datas.size() > 1){
                    //合并单元格
                    List<CellRangeAddress> cellRangeAddressList = new ArrayList<>();
                    cellRangeAddressList.add(new CellRangeAddress(startNo, rowNo, 0, 0));
                    cellRangeAddressList.forEach(excelWriter.getSheet()::addMergedRegion);
                }
            }
            allList = null;

            List<Integer> columnWidthList = new ArrayList<Integer>();
            columnWidthList.add(30);
            columnWidthList.add(10);
            columnWidthList.add(10);
            columnWidthList.add(10);
            columnWidthList.add(10);
            columnWidthList.add(10);
            columnWidthList.add(10);
            columnWidthList.add(10);
            columnWidthList.add(10);
            columnWidthList.add(10);
            columnWidthList.add(10);
            columnWidthList.add(25);
            Integer[] columnWidth = columnWidthList.toArray(new Integer[columnWidthList.size()]);
            excelWriter.export(columnWidth);
            outputstrem.close();
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (outputstrem != null) {
                try {
                    outputstrem.close();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }

    public static void main(String[] args) {
        //exportSimple();
        exportMergedRegion();
    }
}