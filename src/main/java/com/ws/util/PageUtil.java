package com.ws.util;

import org.apache.commons.lang3.StringUtils;

import java.util.List;

/**
 * @Descrption: 分页工具类
 */
public class PageUtil {

    /**
     * List分页
     *
     * @param obj 传入的List数据集合
     * @param totalcount 数据总数
     * @param startnum 当前页码
     * @param endnum 每页显示的总数
     * @return
     */
    public static List paging(List obj, int totalcount, int startnum, int endnum){
        //================将数据分页==============
        //总的页数
        int pagecount= 0;
        /*计算出总共能分成多少页*/
        if(totalcount%endnum>0){//数据总数和每页显示的总数不能整除的情况
            pagecount = totalcount/endnum+1;
        }else{//数据总数和每页显示的总数能整除的情况
            pagecount = totalcount/endnum;
        }
        if(totalcount >0){
            if(startnum<=pagecount){
                if(startnum==1){//当前页数为第一页
                    if(totalcount<=endnum){//数据总数小于每页显示的数据条数
                        //截止到总的数据条数(当前数据不足一页，按一页显示)，这样才不会出现数组越界异常
                        obj=obj.subList(0,totalcount);
                    }else{
                        obj=obj.subList(0,endnum);
                    }
                }else{
                    //定义：截取起始下标
                    int fromindex=(startnum-1)*endnum;
                    //定义：截取结束下标(
                    //此处是重点，假设整除情况为了计算截止下标)
                    //下面的判断也可以换成，能整除时和不能整除时(就是第一个if判断)
                    int toindex=startnum*endnum;
                    /*计算截取截止下标*/
                    if((totalcount-toindex)%endnum>=0){//此页后还有下一页
                        toindex=startnum*endnum;
                    }else{//此页为最后一页
                        toindex=(startnum-1)*endnum+(totalcount%endnum);
                    }
                    if(totalcount>=toindex){
                        obj=obj.subList(fromindex,toindex);
                    }
                }
            }else{
                obj=null;
            }
        }
        return obj;
    }
}

