package com.ws.service;

import com.ws.bean.po.Schedule;

import java.util.List;

public interface ScheduleService {
    List<Schedule> findAll();
}