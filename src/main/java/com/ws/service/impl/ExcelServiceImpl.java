package com.ws.service.impl;

import com.google.common.collect.Lists;
import com.ws.service.ExcelService;
import com.ws.util.excel.PoiExcelExporter;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class ExcelServiceImpl implements ExcelService {

    @Resource
    private PoiExcelExporter poiExcelExporter;

    @Override
    public void test(HttpServletResponse response) {
        String tableName = "test";
        // 获取需要导出的数据
        List<Map<String, Object>> dataList = getResultData();
        // 需要展示的列
        List<String> fieldList = Lists.newArrayList("orderTime","total","except","overTime","timelyRate");
        // 匹配数据
        List<List<String>> results = matchFieldData(dataList, fieldList);
        // 设置表头
        List<List<String>> titleList = setTableTitle();
        // 设置表头合并
        List<CellRangeAddress> cellRangeAddressList = addMergeOrder();

        try {
            poiExcelExporter.exportMulTitle(tableName, titleList, results, cellRangeAddressList, response);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void exportOrder(HttpServletResponse response) {
        String tableName = "order " + LocalDate.now().getYear() + "-" + LocalDate.now().getMonthValue() + "-"
                + LocalDate.now().getDayOfMonth();
        // 获取需要导出的数据
        List<Map<String, Object>> dataList = getResultData();
        // 需要展示的列
        List<String> fieldList = Lists.newArrayList("orderTime","total","except","overTime","timelyRate");
        // 匹配数据
        List<List<String>> results = matchFieldData(dataList, fieldList);
        // 设置表头
        List<List<String>> titleList = setTableTitle();
        // 设置表头合并
        List<CellRangeAddress> cellRangeAddressList = addMergeOrder();

        try {
            poiExcelExporter.exportMulTitle(tableName, titleList, results, cellRangeAddressList, response);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private List<Map<String, Object>> getResultData(){
        String data = "[{\"orderTime\":\"2021-03-03 10:10:10\",\"total\":55,\"except\":12,\"overTime\":8,\"timelyRate\":\"88%\",\"successRate\":\"77\"},{\"orderTime\":\"2021-03-03 10:15:10\",\"total\":155,\"except\":44,\"overTime\":20,\"timelyRate\":\"78%\",\"successRate\":\"65\"},{\"orderTime\":\"2021-03-03 10:20:10\",\"total\":85,\"except\":6,\"overTime\":5,\"timelyRate\":\"98%\",\"successRate\":\"97\"}]";
        //return GsonUtils.changeJsonToList(data);
        return new ArrayList<Map<String, Object>>();
    }

    private List<List<String>> setTableTitle(){
        List<List<String>> titleList = new ArrayList<>();
        List<String> titleOne = Lists.newArrayList("统计","","","","");
        List<String> titleTwo = Lists.newArrayList("日期","综合","","","");
        List<String> titleThree = Lists.newArrayList("","订单总量","异常量","超时量","及时率");
        titleList.add(titleOne);
        titleList.add(titleTwo);
        titleList.add(titleThree);
        return titleList;
    }

    private List<CellRangeAddress> addMergeOrder() {
        List<CellRangeAddress> cellRangeAddressList = new ArrayList<>();
        cellRangeAddressList.add(new CellRangeAddress(0, 0, 0, 4));
        cellRangeAddressList.add(new CellRangeAddress(1, 1, 1, 4));
        cellRangeAddressList.add(new CellRangeAddress(1, 2, 0, 0));
        return cellRangeAddressList;
    }

    private List<List<String>> matchFieldData(List<Map<String, Object>> dataList, List<String> fieldList) {
        return ListUtils
                .emptyIfNull(dataList).stream().filter(Objects::nonNull).map(e -> ListUtils.emptyIfNull(fieldList)
                        .stream().map(f -> MapUtils.getString(e, f)).collect(Collectors.toList()))
                .collect(Collectors.toList());
    }
}

