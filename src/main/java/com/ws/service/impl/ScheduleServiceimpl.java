package com.ws.service.impl;

import com.ws.bean.po.Schedule;
import com.ws.mapper.ScheduleMapper;
import com.ws.service.ScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("scheduleService")
public class ScheduleServiceimpl implements ScheduleService {

    @Autowired
    private ScheduleMapper scheduleMapper;

    @Override
    public List<Schedule> findAll() {
        return scheduleMapper.findAll();
    }
}
