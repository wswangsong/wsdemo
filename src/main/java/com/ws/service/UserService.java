package com.ws.service;

import com.ws.bean.po.User;

import java.util.List;

public interface UserService {
    List<User> findAll();
}