package com.ws.mapper;

import com.ws.bean.po.User;

import java.util.List;

public interface UserMapper {

    List<User> findAll();
}
