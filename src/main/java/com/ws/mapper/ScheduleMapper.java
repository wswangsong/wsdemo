package com.ws.mapper;

import com.ws.bean.po.Schedule;

import java.util.List;

public interface ScheduleMapper {

    List<Schedule> findAll();
}
