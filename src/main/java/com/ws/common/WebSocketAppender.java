//package com.ws.common;
//
//import java.nio.charset.StandardCharsets;
//import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
//import ch.qos.logback.classic.spi.ILoggingEvent;
//import ch.qos.logback.core.AppenderBase;
//import com.ws.util.GetBeanUtil;
//
///**
// *
// * WebSocketAppender
// *
// */
//public class WebSocketAppender extends AppenderBase<ILoggingEvent> {
//
//    // encoder 是必须的
//    private PatternLayoutEncoder encoder;
//    public LoggingChannel loggingChannel = GetBeanUtil.getApplicationContext().getBean(LoggingChannel.class);
//
//    @Override
//    public void start() {
//        // TODO 可以进行资源检查
//        super.start();
//    }
//
//    @Override
//    public void stop() {
//        // TODO 可以进行资源释放
//        super.stop();
//    }
//
//    @Override
//    protected void append(ILoggingEvent eventObject) {
//
//        // 使用 encoder 编码日志
//        byte[] data = this.encoder.encode(eventObject);
//
//        // 推送到所有 WebSocket 客户端
//        System.out.println("+++"+new String(data, StandardCharsets.UTF_8));
//        LoggingChannel.push(new String(data, StandardCharsets.UTF_8));
//        //this.loggingChannel = GetBeanUtil.getApplicationContext().getBean(LoggingChannel.class);
//        //this.loggingChannel.push(new String(data, StandardCharsets.UTF_8));
//    }
//
//    // 必须要有合法的getter/setter
//    public PatternLayoutEncoder getEncoder() {
//        return encoder;
//    }
//    public void setEncoder(PatternLayoutEncoder encoder) {
//        this.encoder = encoder;
//    }
//}
