//package com.ws.common;
//
//import java.io.IOException;
//import java.util.concurrent.ConcurrentHashMap;
//import java.util.concurrent.ConcurrentMap;
//
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.stereotype.Component;
//
//import javax.websocket.*;
//import javax.websocket.server.ServerEndpoint;
//
///**
// *
// * WebSocket 日志端点
// *
// */
//@Component
//@ServerEndpoint(value = "/channel/logging")
//public class LoggingChannel {
//
//    private static final Logger LOGGER = LoggerFactory.getLogger(LoggingChannel.class);
//
//    public static final ConcurrentMap<String, Session> SESSIONS = new ConcurrentHashMap<>();
//
//    private Session session;
//
//    @OnMessage
//    public void onMessage(String messagel){
//        try {
//            this.session.close(new CloseReason(CloseReason.CloseCodes.CANNOT_ACCEPT, "该端点不接受推送"));
//        } catch (IOException e) {
//        }
//    }
//
//    @OnOpen
//    public void onOpen(Session session,  EndpointConfig endpointConfig){
//        this.session = session;
//        SESSIONS.put(this.session.getId(), this.session);
//
//        LOGGER.info("新的连接：{}", this.session.getId());
//    }
//
//    @OnClose
//    public void onClose(CloseReason closeReason){
//
//        LOGGER.info("连接断开：id={}，reason={}",this.session.getId(),closeReason);
//
//        SESSIONS.remove(this.session.getId());
//    }
//
//    @OnError
//    public void onError(Throwable throwable) throws IOException {
//        LOGGER.info("连接异常：id={},throwable={}", this.session.getId(), throwable);
//        this.session.close(new CloseReason(CloseReason.CloseCodes.UNEXPECTED_CONDITION, throwable.getMessage()));
//    }
//
//    /**
//     * 推送日志
//     * @param log
//     */
//    public static void push (String log) {
//        SESSIONS.values().stream().forEach(session -> {
//            if (session.isOpen()) {
//                try {
//                    session.getBasicRemote().sendText(log);
//                } catch (IOException e) {
//                }
//            }
//        });
//    }
//}
