package com.ws.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 用户信息封装类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserInfo {

    private String name;
    private String sex;
    private String photo;
    private String birthday;
    private String time;
    private String num;
    private String attendances;
    private String approveOrgName;
}


