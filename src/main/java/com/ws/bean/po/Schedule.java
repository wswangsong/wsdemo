package com.ws.bean.po;

import lombok.Data;

import java.io.Serializable;

@Data
public class Schedule implements Serializable {
    private Long id;//编号
    private String crons;
    private String content;
}
