package com.ws.bean;

import java.util.List;
import java.util.Map;

public class MeetingNoticeVo {

    private String type;
    private String yearNum;
    private String orderNum;
    private String meetingTime;
    private String meetingPlace;
    private String moderator;
    private String attendUsers;
    private String attendancePersonne1;
    private String sendMailTime;
    private String toWorkTime;
    private String createNoticeTime;
    private String subjectPlace;
    private String receiptMeetingName;
    private String meetingVerify;
    List<Map<String, Object>> subjectDetailsArray;
    private String[] subjectTime;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getYearNum() {
        return yearNum;
    }

    public void setYearNum(String yearNum) {
        this.yearNum = yearNum;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public String getMeetingTime() {
        return meetingTime;
    }

    public void setMeetingTime(String meetingTime) {
        this.meetingTime = meetingTime;
    }

    public String getMeetingPlace() {
        return meetingPlace;
    }

    public void setMeetingPlace(String meetingPlace) {
        this.meetingPlace = meetingPlace;
    }

    public String getModerator() {
        return moderator;
    }

    public void setModerator(String moderator) {
        this.moderator = moderator;
    }

    public String getAttendUsers() {
        return attendUsers;
    }

    public void setAttendUsers(String attendUsers) {
        this.attendUsers = attendUsers;
    }

    public String getAttendancePersonne1() {
        return attendancePersonne1;
    }

    public void setAttendancePersonne1(String attendancePersonne1) {
        this.attendancePersonne1 = attendancePersonne1;
    }

    public String getSendMailTime() {
        return sendMailTime;
    }

    public void setSendMailTime(String sendMailTime) {
        this.sendMailTime = sendMailTime;
    }

    public String getToWorkTime() {
        return toWorkTime;
    }

    public void setToWorkTime(String toWorkTime) {
        this.toWorkTime = toWorkTime;
    }

    public String getCreateNoticeTime() {
        return createNoticeTime;
    }

    public void setCreateNoticeTime(String createNoticeTime) {
        this.createNoticeTime = createNoticeTime;
    }

    public String getSubjectPlace() {
        return subjectPlace;
    }

    public void setSubjectPlace(String subjectPlace) {
        this.subjectPlace = subjectPlace;
    }

    public String getReceiptMeetingName() {
        return receiptMeetingName;
    }

    public void setReceiptMeetingName(String receiptMeetingName) {
        this.receiptMeetingName = receiptMeetingName;
    }

    public String getMeetingVerify() {
        return meetingVerify;
    }

    public void setMeetingVerify(String meetingVerify) {
        this.meetingVerify = meetingVerify;
    }

    public List<Map<String, Object>> getSubjectDetailsArray() {
        return subjectDetailsArray;
    }

    public void setSubjectDetailsArray(List<Map<String, Object>> subjectDetailsArray) {
        this.subjectDetailsArray = subjectDetailsArray;
    }

    public String[] getSubjectTime() {
        return subjectTime;
    }

    public void setSubjectTime(String[] subjectTime) {
        this.subjectTime = subjectTime;
    }
}
