package com.ws.controller;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ws.util.WordUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.documents4j.api.DocumentType;
import com.documents4j.api.IConverter;
import com.documents4j.job.LocalConverter;

import freemarker.template.Configuration;
import freemarker.template.Template;

@RestController
@RequestMapping("/dataAction")
public class DataAction {
	
	private final static Logger LOGGER = LoggerFactory.getLogger(DataAction.class);
	
	@RequestMapping("/testGeneratePdfByFreemaker")
	public Object testGeneratePdfByFreemaker() {
		Map map = new HashMap();
		map.put("name", "张三");//主申请人
		map.put("baseOrgName", "财政部");//申请部门
		map.put("unitName", "中电启明星");//单位名称

		//开始生成pdf文档
		Writer wos = null;
		//pdf文件路径
		String basePath = "/app";
		//String basePath = "D:\\usr";
		try {
			//ClassPathResource ftlResource = new ClassPathResource("ftl");
			//设置freemarker的日志框架为"SLF4J"
			freemarker.log.Logger.selectLoggerLibrary(5);
			//1.创建配置类
			Configuration configuration = new Configuration(Configuration.getVersion());
			//2.设置模板所在的目录
			//configuration.setDirectoryForTemplateLoading(ftlResource.getFile());
			configuration.setDirectoryForTemplateLoading(new File("/app/ftl"));
			//configuration.setDirectoryForTemplateLoading(new File("D:\\usr\\ftl"));
			//3.设置字符集
			configuration.setDefaultEncoding("utf-8");
			//4.加载模板;
			Template template = configuration.getTemplate("BusinessApplyForm.ftl", "utf-8");

			//6.创建 Writer 对象
			String wordPath = basePath + File.separator + "测试"+System.currentTimeMillis()+".docx";
			//wos = new FileWriter(new File(wordPath));
			wos = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File(wordPath)), "utf-8"), 10240);
			//7.替换模板内容
			template.process(map, wos);
			//8.关闭 Writer 对象
			wos.close();

			//9.word转pdf
			//String pdfPath = basePath + File.separator + "测试"+System.currentTimeMillis()+".pdf";
			String pdfPath = basePath + File.separator;
//	        InputStream pis = new FileInputStream(wordPath);
//	        OutputStream pos = new FileOutputStream(pdfPath);
			//WordUtil.wordToPdfByDocx4j(pis, pos);
			//WordUtil.wordToPdfByDocuments4j(pis, pos);
			WordUtil.wordToPdfByLibreOffice(wordPath, pdfPath);
//	        pos.close();
//	        pis.close();
		} catch (Exception e){
			e.printStackTrace();
		} finally {
			if(wos != null){
				try {
					wos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return "success";
	}

	@RequestMapping("/testGeneratePdfDocx4j")
	public Object testGeneratePdfByDocx4j() {
		Map map = new HashMap();
		map.put("name", "张三");//主申请人
		map.put("baseOrgName", "财政部");//申请部门
		map.put("unitName", "中电启明星");//单位名称

		//开始生成pdf文档
		Writer wos = null;
		//pdf文件路径
		String basePath = "/app";
		//String basePath = "D:\\usr";
		try {
			String templatePath = basePath + File.separator + "ftl" + File.separator + "BusinessApplyForm.docx";
			InputStream templatePathIs = new FileInputStream(templatePath);

			String wordPath = basePath + File.separator + "测试"+System.currentTimeMillis()+".docx";
			//生成的word文件输出流
			OutputStream os = new FileOutputStream(wordPath);

			//根据模板文件替换变量
			WordUtil.replaceVariable(templatePathIs, map, os);

			os.flush();
			templatePathIs.close();
			os.close();

			//word转pdf
//			String pdfPath = basePath + File.separator + "测试"+System.currentTimeMillis()+".pdf";
			String pdfPath = basePath + File.separator;
//	        InputStream pis = new FileInputStream(wordPath);
//	        OutputStream pos = new FileOutputStream(pdfPath);
			//WordUtil.wordToPdfByDocx4j(pis, pos);
			//WordUtil.wordToPdfByDocuments4j(pis, pos);
			WordUtil.wordToPdfByLibreOffice(wordPath, pdfPath);
//	        pos.close();
//	        pis.close();
		} catch (Exception e){
			e.printStackTrace();
		} finally {
			if(wos != null){
				try {
					wos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return "success";
	}
}

