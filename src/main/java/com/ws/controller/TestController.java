package com.ws.controller;

import com.ws.bean.MeetingNoticeVo;
import com.ws.bean.Result;
//import com.ws.common.LoggingChannel;
import com.ws.service.ExcelService;
import com.ws.util.RedisUtil;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;

@RestController
@RequestMapping("/test")
public class TestController {

    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private RedisTemplate redisTemplate;
    @Resource
    private RedissonClient redissonClient;

    @RequestMapping("/test")
    public String test(HttpServletResponse response, HttpServletRequest request) {
        return "test";
    }


//    @RequestMapping("/testWebsocket")
//    public String testWebsocket(HttpServletResponse response, HttpServletRequest request) {
//        LoggingChannel.push("888");
//        return "发送成功！";
//    }


    @RequestMapping("/testRedis")
    public void testRedis(HttpServletResponse response, HttpServletRequest request) {
        ValueOperations<Serializable, Object> operations = redisTemplate.opsForValue();
        operations.set("testkey","testvalue");
        System.out.println("----------"+operations.get("testkey"));
        System.out.println("=========="+redisUtil.get("testkey"));

        operations.set("testZW","测试中文");
        System.out.println("----------"+operations.get("testZW"));
        System.out.println("=========="+redisUtil.get("testZW"));
    }

    @ResponseBody
    @GetMapping("/testRedisson")
    public String testRedisson() {
        // 1.获取一把锁，只要锁的名字一样，就是同一把锁
        RLock lock = redissonClient.getLock("my-lock");
        lock.lock(); // 上锁，阻塞式等待。默认加的锁是30s时间
        try {
            // 1、锁的自动续期，运行期间自动给锁续上新的30s，无需担心业务时间长，锁过期会自动被释放
            // 2、加锁的业务只要运行完成，就不会给当前锁续期，即使不手动释放锁，锁默认在30s后自动释放，避免死锁
            System.out.println("加锁成功，执行业务代码..."+Thread.currentThread().getId());
            Thread.sleep(30000);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (lock.isLocked()){ //判断是否还是锁定状态 ----延迟时间
                if(lock.isHeldByCurrentThread()){ // 是当前执行线程的锁
                    System.out.println("释放锁..."+Thread.currentThread().getId());
                    lock.unlock(); //释放锁
                }
            }
        }
        return "testRedisson!";
    }

    @RequestMapping("/blogs")
    public Result list(@RequestBody MeetingNoticeVo meetingNoticeVo) {

        return Result.success("");
    }
}

