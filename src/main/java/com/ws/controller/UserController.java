package com.ws.controller;

import com.ws.bean.po.User;
import com.ws.service.UserService;
import com.ws.util.SpringContextUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping("/findAll")
    public List<User> findAll(){
        return userService.findAll();
    }

    @RequestMapping("/testSpringContext")
    public List<User> testSpringContext(){
        UserService userService = SpringContextUtils.getBeanByClass(UserService.class);
        return userService.findAll();
    }

}
