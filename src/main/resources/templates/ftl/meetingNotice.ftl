<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<?mso-application progid="Word.Document"?>
<w:wordDocument
    xmlns:w="http://schemas.microsoft.com/office/word/2003/wordml"
    xmlns:v="urn:schemas-microsoft-com:vml"
    xmlns:w10="urn:schemas-microsoft-com:office:word"
    xmlns:sl="http://schemas.microsoft.com/schemaLibrary/2003/core"
    xmlns:aml="http://schemas.microsoft.com/aml/2001/core"
    xmlns:wx="http://schemas.microsoft.com/office/word/2003/auxHint"
    xmlns:o="urn:schemas-microsoft-com:office:office"
    xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882" w:macrosPresent="no" w:embeddedObjPresent="no" w:ocxPresent="no" xml:space="preserve"
    xmlns:wpsCustomData="http://www.wps.cn/officeDocument/2013/wpsCustomData">
    <o:DocumentProperties>
        <o:Title>关于调整公司党组理论学习中心组组成人员的通知</o:Title>
        <o:Author>Administrator</o:Author>
        <o:LastAuthor>蓝骑士</o:LastAuthor>
        <o:Revision>3</o:Revision>
        <o:LastPrinted>2019-05-28T09:14:00Z</o:LastPrinted>
        <o:Created>2017-09-24T17:18:00Z</o:Created>
        <o:LastSaved>2021-12-16T07:24:22Z</o:LastSaved>
        <o:TotalTime>1440</o:TotalTime>
        <o:Bytes>0</o:Bytes>
        <o:Pages>1</o:Pages>
        <o:Words>47</o:Words>
        <o:Characters>274</o:Characters>
        <o:Lines>2</o:Lines>
        <o:Paragraphs>1</o:Paragraphs>
        <o:CharactersWithSpaces>320</o:CharactersWithSpaces>
        <o:Version>14</o:Version>
    </o:DocumentProperties>
    <o:CustomDocumentProperties>
        <o:KSOProductBuildVer dt:dt="string">2052-11.1.0.11115</o:KSOProductBuildVer>
        <o:ICV dt:dt="string">A75F13E27D054DD48926B77FBD203C30</o:ICV>
    </o:CustomDocumentProperties>
    <w:fonts>
        <w:defaultFonts w:ascii="Calibri" w:fareast="宋体" w:h-ansi="Calibri" w:cs="Calibri"/>
        <w:font w:name="Times New Roman">
            <w:panose-1 w:val="02020603050405020304"/>
            <w:charset w:val="00"/>
            <w:family w:val="Auto"/>
            <w:pitch w:val="Default"/>
            <w:sig w:usb-0="E0002EFF" w:usb-1="C000785B" w:usb-2="00000009" w:usb-3="00000000" w:csb-0="400001FF" w:csb-1="FFFF0000"/>
        </w:font>
        <w:font w:name="宋体">
            <w:panose-1 w:val="02010600030101010101"/>
            <w:charset w:val="86"/>
            <w:family w:val="Auto"/>
            <w:pitch w:val="Default"/>
            <w:sig w:usb-0="00000003" w:usb-1="288F0000" w:usb-2="00000006" w:usb-3="00000000" w:csb-0="00040001" w:csb-1="00000000"/>
        </w:font>
        <w:font w:name="Wingdings">
            <w:panose-1 w:val="05000000000000000000"/>
            <w:charset w:val="02"/>
            <w:family w:val="Auto"/>
            <w:pitch w:val="Default"/>
            <w:sig w:usb-0="00000000" w:usb-1="00000000" w:usb-2="00000000" w:usb-3="00000000" w:csb-0="80000000" w:csb-1="00000000"/>
        </w:font>
        <w:font w:name="Arial">
            <w:panose-1 w:val="020B0604020202020204"/>
            <w:charset w:val="01"/>
            <w:family w:val="SWiss"/>
            <w:pitch w:val="Default"/>
            <w:sig w:usb-0="E0002EFF" w:usb-1="C000785B" w:usb-2="00000009" w:usb-3="00000000" w:csb-0="400001FF" w:csb-1="FFFF0000"/>
        </w:font>
        <w:font w:name="黑体">
            <w:panose-1 w:val="02010609060101010101"/>
            <w:charset w:val="86"/>
            <w:family w:val="Auto"/>
            <w:pitch w:val="Default"/>
            <w:sig w:usb-0="800002BF" w:usb-1="38CF7CFA" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="00040001" w:csb-1="00000000"/>
        </w:font>
        <w:font w:name="Courier New">
            <w:panose-1 w:val="02070309020205020404"/>
            <w:charset w:val="01"/>
            <w:family w:val="Modern"/>
            <w:pitch w:val="Default"/>
            <w:sig w:usb-0="E0002EFF" w:usb-1="C0007843" w:usb-2="00000009" w:usb-3="00000000" w:csb-0="400001FF" w:csb-1="FFFF0000"/>
        </w:font>
        <w:font w:name="Symbol">
            <w:panose-1 w:val="05050102010706020507"/>
            <w:charset w:val="02"/>
            <w:family w:val="Roman"/>
            <w:pitch w:val="Default"/>
            <w:sig w:usb-0="00000000" w:usb-1="00000000" w:usb-2="00000000" w:usb-3="00000000" w:csb-0="80000000" w:csb-1="00000000"/>
        </w:font>
        <w:font w:name="Calibri">
            <w:panose-1 w:val="020F0502020204030204"/>
            <w:charset w:val="00"/>
            <w:family w:val="SWiss"/>
            <w:pitch w:val="Default"/>
            <w:sig w:usb-0="E4002EFF" w:usb-1="C000247B" w:usb-2="00000009" w:usb-3="00000000" w:csb-0="200001FF" w:csb-1="00000000"/>
        </w:font>
        <w:font w:name="Wingdings">
            <w:panose-1 w:val="05000000000000000000"/>
            <w:charset w:val="00"/>
            <w:family w:val="Auto"/>
            <w:pitch w:val="Default"/>
            <w:sig w:usb-0="00000000" w:usb-1="00000000" w:usb-2="00000000" w:usb-3="00000000" w:csb-0="80000000" w:csb-1="00000000"/>
        </w:font>
        <w:font w:name="Arial">
            <w:panose-1 w:val="020B0604020202020204"/>
            <w:charset w:val="00"/>
            <w:family w:val="Auto"/>
            <w:pitch w:val="Default"/>
            <w:sig w:usb-0="E0002EFF" w:usb-1="C000785B" w:usb-2="00000009" w:usb-3="00000000" w:csb-0="400001FF" w:csb-1="FFFF0000"/>
        </w:font>
        <w:font w:name="Courier New">
            <w:panose-1 w:val="02070309020205020404"/>
            <w:charset w:val="00"/>
            <w:family w:val="Auto"/>
            <w:pitch w:val="Default"/>
            <w:sig w:usb-0="E0002EFF" w:usb-1="C0007843" w:usb-2="00000009" w:usb-3="00000000" w:csb-0="400001FF" w:csb-1="FFFF0000"/>
        </w:font>
        <w:font w:name="Symbol">
            <w:panose-1 w:val="05050102010706020507"/>
            <w:charset w:val="00"/>
            <w:family w:val="Auto"/>
            <w:pitch w:val="Default"/>
            <w:sig w:usb-0="00000000" w:usb-1="00000000" w:usb-2="00000000" w:usb-3="00000000" w:csb-0="80000000" w:csb-1="00000000"/>
        </w:font>
        <w:font w:name="仿宋_GB2312">
            <w:altName w:val="仿宋"/>
            <w:panose-1 w:val="00000000000000000000"/>
            <w:charset w:val="00"/>
            <w:family w:val="Auto"/>
            <w:pitch w:val="Default"/>
            <w:sig w:usb-0="00000001" w:usb-1="080E0000" w:usb-2="00000000" w:usb-3="00000000" w:csb-0="00040000" w:csb-1="00000000"/>
        </w:font>
        <w:font w:name="仿宋">
            <w:panose-1 w:val="02010609060101010101"/>
            <w:charset w:val="86"/>
            <w:family w:val="Auto"/>
            <w:pitch w:val="Default"/>
            <w:sig w:usb-0="800002BF" w:usb-1="38CF7CFA" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="00040001" w:csb-1="00000000"/>
        </w:font>
        <w:font w:name="金山简标宋">
            <w:altName w:val="宋体"/>
            <w:panose-1 w:val="00000000000000000000"/>
            <w:charset w:val="00"/>
            <w:family w:val="Auto"/>
            <w:pitch w:val="Default"/>
            <w:sig w:usb-0="00000000" w:usb-1="00000000" w:usb-2="00000010" w:usb-3="00000000" w:csb-0="00040000" w:csb-1="00000000"/>
        </w:font>
        <w:font w:name="方正仿宋_GBK">
            <w:altName w:val="微软雅黑"/>
            <w:panose-1 w:val="00000000000000000000"/>
            <w:charset w:val="00"/>
            <w:family w:val="Auto"/>
            <w:pitch w:val="Default"/>
            <w:sig w:usb-0="00000001" w:usb-1="080E0000" w:usb-2="00000000" w:usb-3="00000000" w:csb-0="00040000" w:csb-1="00000000"/>
        </w:font>
        <w:font w:name="Arial Unicode MS">
            <w:panose-1 w:val="020B0604020202020204"/>
            <w:charset w:val="86"/>
            <w:family w:val="Auto"/>
            <w:pitch w:val="Default"/>
            <w:sig w:usb-0="FFFFFFFF" w:usb-1="E9FFFFFF" w:usb-2="0000003F" w:usb-3="00000000" w:csb-0="603F01FF" w:csb-1="FFFF0000"/>
        </w:font>
        <w:font w:name="微软雅黑">
            <w:panose-1 w:val="020B0503020204020204"/>
            <w:charset w:val="86"/>
            <w:family w:val="Auto"/>
            <w:pitch w:val="Default"/>
            <w:sig w:usb-0="80000287" w:usb-1="2ACF3C50" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="0004001F" w:csb-1="00000000"/>
        </w:font>
        <w:font w:name="Tahoma">
            <w:panose-1 w:val="020B0604030504040204"/>
            <w:charset w:val="00"/>
            <w:family w:val="Auto"/>
            <w:pitch w:val="Default"/>
            <w:sig w:usb-0="E1002EFF" w:usb-1="C000605B" w:usb-2="00000029" w:usb-3="00000000" w:csb-0="200101FF" w:csb-1="20280000"/>
        </w:font>
    </w:fonts>
    <w:lists>
        <w:listDef w:listDefId="0">
            <w:plt w:val="SingleLevel"/>
            <w:lvl w:ilvl="0">
                <w:start w:val="1"/>
                <w:suff w:val="Nothing"/>
                <w:lvlText w:val="%1."/>
                <w:lvlJc w:val="left"/>
            </w:lvl>
        </w:listDef>
        <w:list w:ilfo="1">
            <w:ilst w:val="0"/>
        </w:list>
    </w:lists>
    <w:styles>
        <w:latentStyles w:defLockedState="off" w:latentStyleCount="260">
            <w:lsdException w:name="Normal"/>
            <w:lsdException w:name="heading 1"/>
            <w:lsdException w:name="heading 2"/>
            <w:lsdException w:name="heading 3"/>
            <w:lsdException w:name="heading 4"/>
            <w:lsdException w:name="heading 5"/>
            <w:lsdException w:name="heading 6"/>
            <w:lsdException w:name="heading 7"/>
            <w:lsdException w:name="heading 8"/>
            <w:lsdException w:name="heading 9"/>
            <w:lsdException w:name="index 1"/>
            <w:lsdException w:name="index 2"/>
            <w:lsdException w:name="index 3"/>
            <w:lsdException w:name="index 4"/>
            <w:lsdException w:name="index 5"/>
            <w:lsdException w:name="index 6"/>
            <w:lsdException w:name="index 7"/>
            <w:lsdException w:name="index 8"/>
            <w:lsdException w:name="index 9"/>
            <w:lsdException w:name="toc 1"/>
            <w:lsdException w:name="toc 2"/>
            <w:lsdException w:name="toc 3"/>
            <w:lsdException w:name="toc 4"/>
            <w:lsdException w:name="toc 5"/>
            <w:lsdException w:name="toc 6"/>
            <w:lsdException w:name="toc 7"/>
            <w:lsdException w:name="toc 8"/>
            <w:lsdException w:name="toc 9"/>
            <w:lsdException w:name="Normal Indent"/>
            <w:lsdException w:name="footnote text"/>
            <w:lsdException w:name="annotation text"/>
            <w:lsdException w:name="header"/>
            <w:lsdException w:name="footer"/>
            <w:lsdException w:name="index heading"/>
            <w:lsdException w:name="caption"/>
            <w:lsdException w:name="table of figures"/>
            <w:lsdException w:name="envelope address"/>
            <w:lsdException w:name="envelope return"/>
            <w:lsdException w:name="footnote reference"/>
            <w:lsdException w:name="annotation reference"/>
            <w:lsdException w:name="line number"/>
            <w:lsdException w:name="page number"/>
            <w:lsdException w:name="endnote reference"/>
            <w:lsdException w:name="endnote text"/>
            <w:lsdException w:name="table of authorities"/>
            <w:lsdException w:name="macro"/>
            <w:lsdException w:name="toa heading"/>
            <w:lsdException w:name="List"/>
            <w:lsdException w:name="List Bullet"/>
            <w:lsdException w:name="List Number"/>
            <w:lsdException w:name="List 2"/>
            <w:lsdException w:name="List 3"/>
            <w:lsdException w:name="List 4"/>
            <w:lsdException w:name="List 5"/>
            <w:lsdException w:name="List Bullet 2"/>
            <w:lsdException w:name="List Bullet 3"/>
            <w:lsdException w:name="List Bullet 4"/>
            <w:lsdException w:name="List Bullet 5"/>
            <w:lsdException w:name="List Number 2"/>
            <w:lsdException w:name="List Number 3"/>
            <w:lsdException w:name="List Number 4"/>
            <w:lsdException w:name="List Number 5"/>
            <w:lsdException w:name="Title"/>
            <w:lsdException w:name="Closing"/>
            <w:lsdException w:name="Signature"/>
            <w:lsdException w:name="Default Paragraph Font"/>
            <w:lsdException w:name="Body Text"/>
            <w:lsdException w:name="Body Text Indent"/>
            <w:lsdException w:name="List Continue"/>
            <w:lsdException w:name="List Continue 2"/>
            <w:lsdException w:name="List Continue 3"/>
            <w:lsdException w:name="List Continue 4"/>
            <w:lsdException w:name="List Continue 5"/>
            <w:lsdException w:name="Message Header"/>
            <w:lsdException w:name="Subtitle"/>
            <w:lsdException w:name="Salutation"/>
            <w:lsdException w:name="Date"/>
            <w:lsdException w:name="Body Text First Indent"/>
            <w:lsdException w:name="Body Text First Indent 2"/>
            <w:lsdException w:name="Note Heading"/>
            <w:lsdException w:name="Body Text 2"/>
            <w:lsdException w:name="Body Text 3"/>
            <w:lsdException w:name="Body Text Indent 2"/>
            <w:lsdException w:name="Body Text Indent 3"/>
            <w:lsdException w:name="Block Text"/>
            <w:lsdException w:name="Hyperlink"/>
            <w:lsdException w:name="FollowedHyperlink"/>
            <w:lsdException w:name="Strong"/>
            <w:lsdException w:name="Emphasis"/>
            <w:lsdException w:name="Document Map"/>
            <w:lsdException w:name="Plain Text"/>
            <w:lsdException w:name="E-mail Signature"/>
            <w:lsdException w:name="Normal (Web)"/>
            <w:lsdException w:name="HTML Acronym"/>
            <w:lsdException w:name="HTML Address"/>
            <w:lsdException w:name="HTML Cite"/>
            <w:lsdException w:name="HTML Code"/>
            <w:lsdException w:name="HTML Definition"/>
            <w:lsdException w:name="HTML Keyboard"/>
            <w:lsdException w:name="HTML Preformatted"/>
            <w:lsdException w:name="HTML Sample"/>
            <w:lsdException w:name="HTML Typewriter"/>
            <w:lsdException w:name="HTML Variable"/>
            <w:lsdException w:name="Normal Table"/>
            <w:lsdException w:name="annotation subject"/>
            <w:lsdException w:name="Table Simple 1"/>
            <w:lsdException w:name="Table Simple 2"/>
            <w:lsdException w:name="Table Simple 3"/>
            <w:lsdException w:name="Table Classic 1"/>
            <w:lsdException w:name="Table Classic 2"/>
            <w:lsdException w:name="Table Classic 3"/>
            <w:lsdException w:name="Table Classic 4"/>
            <w:lsdException w:name="Table Colorful 1"/>
            <w:lsdException w:name="Table Colorful 2"/>
            <w:lsdException w:name="Table Colorful 3"/>
            <w:lsdException w:name="Table Columns 1"/>
            <w:lsdException w:name="Table Columns 2"/>
            <w:lsdException w:name="Table Columns 3"/>
            <w:lsdException w:name="Table Columns 4"/>
            <w:lsdException w:name="Table Columns 5"/>
            <w:lsdException w:name="Table Grid 1"/>
            <w:lsdException w:name="Table Grid 2"/>
            <w:lsdException w:name="Table Grid 3"/>
            <w:lsdException w:name="Table Grid 4"/>
            <w:lsdException w:name="Table Grid 5"/>
            <w:lsdException w:name="Table Grid 6"/>
            <w:lsdException w:name="Table Grid 7"/>
            <w:lsdException w:name="Table Grid 8"/>
            <w:lsdException w:name="Table List 1"/>
            <w:lsdException w:name="Table List 2"/>
            <w:lsdException w:name="Table List 3"/>
            <w:lsdException w:name="Table List 4"/>
            <w:lsdException w:name="Table List 5"/>
            <w:lsdException w:name="Table List 6"/>
            <w:lsdException w:name="Table List 7"/>
            <w:lsdException w:name="Table List 8"/>
            <w:lsdException w:name="Table 3D effects 1"/>
            <w:lsdException w:name="Table 3D effects 2"/>
            <w:lsdException w:name="Table 3D effects 3"/>
            <w:lsdException w:name="Table Contemporary"/>
            <w:lsdException w:name="Table Elegant"/>
            <w:lsdException w:name="Table Professional"/>
            <w:lsdException w:name="Table Subtle 1"/>
            <w:lsdException w:name="Table Subtle 2"/>
            <w:lsdException w:name="Table Web 1"/>
            <w:lsdException w:name="Table Web 2"/>
            <w:lsdException w:name="Table Web 3"/>
            <w:lsdException w:name="Balloon Text"/>
            <w:lsdException w:name="Table Grid"/>
            <w:lsdException w:name="Table Theme"/>
            <w:lsdException w:name="Light Shading"/>
            <w:lsdException w:name="Light List"/>
            <w:lsdException w:name="Light Grid"/>
            <w:lsdException w:name="Medium Shading 1"/>
            <w:lsdException w:name="Medium Shading 2"/>
            <w:lsdException w:name="Medium List 1"/>
            <w:lsdException w:name="Medium List 2"/>
            <w:lsdException w:name="Medium Grid 1"/>
            <w:lsdException w:name="Medium Grid 2"/>
            <w:lsdException w:name="Medium Grid 3"/>
            <w:lsdException w:name="Dark List"/>
            <w:lsdException w:name="Colorful Shading"/>
            <w:lsdException w:name="Colorful List"/>
            <w:lsdException w:name="Colorful Grid"/>
            <w:lsdException w:name="Light Shading Accent 1"/>
            <w:lsdException w:name="Light List Accent 1"/>
            <w:lsdException w:name="Light Grid Accent 1"/>
            <w:lsdException w:name="Medium Shading 1 Accent 1"/>
            <w:lsdException w:name="Medium Shading 2 Accent 1"/>
            <w:lsdException w:name="Medium List 1 Accent 1"/>
            <w:lsdException w:name="Medium List 2 Accent 1"/>
            <w:lsdException w:name="Medium Grid 1 Accent 1"/>
            <w:lsdException w:name="Medium Grid 2 Accent 1"/>
            <w:lsdException w:name="Medium Grid 3 Accent 1"/>
            <w:lsdException w:name="Dark List Accent 1"/>
            <w:lsdException w:name="Colorful Shading Accent 1"/>
            <w:lsdException w:name="Colorful List Accent 1"/>
            <w:lsdException w:name="Colorful Grid Accent 1"/>
            <w:lsdException w:name="Light Shading Accent 2"/>
            <w:lsdException w:name="Light List Accent 2"/>
            <w:lsdException w:name="Light Grid Accent 2"/>
            <w:lsdException w:name="Medium Shading 1 Accent 2"/>
            <w:lsdException w:name="Medium Shading 2 Accent 2"/>
            <w:lsdException w:name="Medium List 1 Accent 2"/>
            <w:lsdException w:name="Medium List 2 Accent 2"/>
            <w:lsdException w:name="Medium Grid 1 Accent 2"/>
            <w:lsdException w:name="Medium Grid 2 Accent 2"/>
            <w:lsdException w:name="Medium Grid 3 Accent 2"/>
            <w:lsdException w:name="Dark List Accent 2"/>
            <w:lsdException w:name="Colorful Shading Accent 2"/>
            <w:lsdException w:name="Colorful List Accent 2"/>
            <w:lsdException w:name="Colorful Grid Accent 2"/>
            <w:lsdException w:name="Light Shading Accent 3"/>
            <w:lsdException w:name="Light List Accent 3"/>
            <w:lsdException w:name="Light Grid Accent 3"/>
            <w:lsdException w:name="Medium Shading 1 Accent 3"/>
            <w:lsdException w:name="Medium Shading 2 Accent 3"/>
            <w:lsdException w:name="Medium List 1 Accent 3"/>
            <w:lsdException w:name="Medium List 2 Accent 3"/>
            <w:lsdException w:name="Medium Grid 1 Accent 3"/>
            <w:lsdException w:name="Medium Grid 2 Accent 3"/>
            <w:lsdException w:name="Medium Grid 3 Accent 3"/>
            <w:lsdException w:name="Dark List Accent 3"/>
            <w:lsdException w:name="Colorful Shading Accent 3"/>
            <w:lsdException w:name="Colorful List Accent 3"/>
            <w:lsdException w:name="Colorful Grid Accent 3"/>
            <w:lsdException w:name="Light Shading Accent 4"/>
            <w:lsdException w:name="Light List Accent 4"/>
            <w:lsdException w:name="Light Grid Accent 4"/>
            <w:lsdException w:name="Medium Shading 1 Accent 4"/>
            <w:lsdException w:name="Medium Shading 2 Accent 4"/>
            <w:lsdException w:name="Medium List 1 Accent 4"/>
            <w:lsdException w:name="Medium List 2 Accent 4"/>
            <w:lsdException w:name="Medium Grid 1 Accent 4"/>
            <w:lsdException w:name="Medium Grid 2 Accent 4"/>
            <w:lsdException w:name="Medium Grid 3 Accent 4"/>
            <w:lsdException w:name="Dark List Accent 4"/>
            <w:lsdException w:name="Colorful Shading Accent 4"/>
            <w:lsdException w:name="Colorful List Accent 4"/>
            <w:lsdException w:name="Colorful Grid Accent 4"/>
            <w:lsdException w:name="Light Shading Accent 5"/>
            <w:lsdException w:name="Light List Accent 5"/>
            <w:lsdException w:name="Light Grid Accent 5"/>
            <w:lsdException w:name="Medium Shading 1 Accent 5"/>
            <w:lsdException w:name="Medium Shading 2 Accent 5"/>
            <w:lsdException w:name="Medium List 1 Accent 5"/>
            <w:lsdException w:name="Medium List 2 Accent 5"/>
            <w:lsdException w:name="Medium Grid 1 Accent 5"/>
            <w:lsdException w:name="Medium Grid 2 Accent 5"/>
            <w:lsdException w:name="Medium Grid 3 Accent 5"/>
            <w:lsdException w:name="Dark List Accent 5"/>
            <w:lsdException w:name="Colorful Shading Accent 5"/>
            <w:lsdException w:name="Colorful List Accent 5"/>
            <w:lsdException w:name="Colorful Grid Accent 5"/>
            <w:lsdException w:name="Light Shading Accent 6"/>
            <w:lsdException w:name="Light List Accent 6"/>
            <w:lsdException w:name="Light Grid Accent 6"/>
            <w:lsdException w:name="Medium Shading 1 Accent 6"/>
            <w:lsdException w:name="Medium Shading 2 Accent 6"/>
            <w:lsdException w:name="Medium List 1 Accent 6"/>
            <w:lsdException w:name="Medium List 2 Accent 6"/>
            <w:lsdException w:name="Medium Grid 1 Accent 6"/>
            <w:lsdException w:name="Medium Grid 2 Accent 6"/>
            <w:lsdException w:name="Medium Grid 3 Accent 6"/>
            <w:lsdException w:name="Dark List Accent 6"/>
            <w:lsdException w:name="Colorful Shading Accent 6"/>
            <w:lsdException w:name="Colorful List Accent 6"/>
            <w:lsdException w:name="Colorful Grid Accent 6"/>
        </w:latentStyles>
        <w:style w:type="paragraph" w:styleId="a1" w:default="on">
            <w:name w:val="Normal"/>
            <w:pPr>
                <w:widowControl w:val="off"/>
                <w:jc w:val="both"/>
            </w:pPr>
            <w:rPr>
                <w:kern w:val="2"/>
                <w:sz w:val="21"/>
                <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
            </w:rPr>
        </w:style>
        <w:style w:type="paragraph" w:styleId="2">
            <w:name w:val="heading 1"/>
            <w:basedOn w:val="a1"/>
            <w:next w:val="a1"/>
            <w:link w:val="a11"/>
            <w:pPr>
                <w:widowControl/>
                <w:spacing w:before="340" w:before-lines="0" w:before-autospacing="off" w:after="330" w:after-autospacing="off" w:line="480" w:line-rule="at-least"/>
                <w:jc w:val="center"/>
                <w:outlineLvl w:val="0"/>
            </w:pPr>
            <w:rPr>
                <w:b/>
                <w:kern w:val="36"/>
                <w:sz w:val="44"/>
            </w:rPr>
        </w:style>
        <w:style w:type="character" w:styleId="a8" w:default="on">
            <w:name w:val="Default Paragraph Font"/>
        </w:style>
        <w:style w:type="table" w:styleId="a7" w:default="on">
            <w:name w:val="Normal Table"/>
            <w:semiHidden/>
            <w:pPr>
                <w:keepNext w:val="off"/>
                <w:keepLines w:val="off"/>
                <w:widowControl/>
                <w:supressLineNumbers w:val="off"/>
                <w:spacing w:before="0" w:before-autospacing="off" w:after="0" w:after-autospacing="off"/>
                <w:ind w:left="0" w:right="0"/>
            </w:pPr>
            <w:rPr>
                <w:rFonts w:ascii="Calibri" w:h-ansi="Calibri" w:cs="Times New Roman" w:hint="default"/>
                <w:sz-cs w:val="20"/>
            </w:rPr>
            <w:tblPr>
                <w:tblBorders>
                    <w:top w:val="nil"/>
                    <w:left w:val="nil"/>
                    <w:bottom w:val="nil"/>
                    <w:right w:val="nil"/>
                    <w:insideH w:val="nil"/>
                    <w:insideV w:val="nil"/>
                </w:tblBorders>
                <w:tblCellMar>
                    <w:top w:w="0" w:type="dxa"/>
                    <w:left w:w="108" w:type="dxa"/>
                    <w:bottom w:w="0" w:type="dxa"/>
                    <w:right w:w="108" w:type="dxa"/>
                </w:tblCellMar>
            </w:tblPr>
        </w:style>
        <w:style w:type="paragraph" w:styleId="a3">
            <w:name w:val="Body Text"/>
            <w:basedOn w:val="a1"/>
            <w:link w:val="a12"/>
            <w:rPr>
                <w:rFonts w:ascii="Calibri" w:h-ansi="Calibri" w:hint="default"/>
                <w:b/>
                <w:kern w:val="2"/>
                <w:sz w:val="36"/>
            </w:rPr>
        </w:style>
        <w:style w:type="paragraph" w:styleId="a4">
            <w:name w:val="footer"/>
            <w:basedOn w:val="a1"/>
            <w:link w:val="a13"/>
            <w:pPr>
                <w:snapToGrid w:val="off"/>
                <w:jc w:val="left"/>
            </w:pPr>
            <w:rPr>
                <w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman" w:fareast="仿宋_GB2312" w:hint="default"/>
                <w:sz w:val="18"/>
            </w:rPr>
        </w:style>
        <w:style w:type="paragraph" w:styleId="a5">
            <w:name w:val="header"/>
            <w:basedOn w:val="a1"/>
            <w:pPr>
                <w:pBdr>
                    <w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="1" w:color="auto"/>
                </w:pBdr>
                <w:snapToGrid w:val="off"/>
                <w:jc w:val="center"/>
            </w:pPr>
            <w:rPr>
                <w:sz w:val="18"/>
            </w:rPr>
        </w:style>
        <w:style w:type="paragraph" w:styleId="a6">
            <w:name w:val="toc 1"/>
            <w:basedOn w:val="a1"/>
            <w:next w:val="a1"/>
            <w:rPr>
                <w:rFonts w:hint="fareast"/>
            </w:rPr>
        </w:style>
        <w:style w:type="character" w:styleId="a9">
            <w:name w:val="page number"/>
        </w:style>
        <w:style w:type="paragraph" w:styleId="a10">
            <w:name w:val="Normal (Web)"/>
            <w:basedOn w:val="a1"/>
            <w:pPr>
                <w:widowControl/>
                <w:spacing w:before="100" w:before-lines="0" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
                <w:jc w:val="left"/>
            </w:pPr>
            <w:rPr>
                <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="default"/>
                <w:kern w:val="0"/>
                <w:sz w:val="24"/>
            </w:rPr>
        </w:style>
        <w:style w:type="character" w:styleId="a11">
            <w:name w:val="标题 1 字符"/>
            <w:link w:val="2"/>
            <w:rPr>
                <w:b/>
                <w:kern w:val="36"/>
                <w:sz w:val="44"/>
            </w:rPr>
        </w:style>
        <w:style w:type="character" w:styleId="a12">
            <w:name w:val="正文文本 字符"/>
            <w:link w:val="a3"/>
            <w:rPr>
                <w:rFonts w:ascii="Calibri" w:h-ansi="Calibri" w:hint="default"/>
                <w:b/>
                <w:kern w:val="2"/>
                <w:sz w:val="36"/>
            </w:rPr>
        </w:style>
        <w:style w:type="character" w:styleId="a13">
            <w:name w:val="页脚 字符"/>
            <w:link w:val="a4"/>
            <w:rPr>
                <w:kern w:val="2"/>
                <w:sz w:val="18"/>
            </w:rPr>
        </w:style>
        <w:style w:type="paragraph" w:styleId="a14">
            <w:name w:val="Char2"/>
            <w:basedOn w:val="a6"/>
            <w:next w:val="a6"/>
            <w:pPr>
                <w:pStyle w:val="a6"/>
                <w:snapToGrid w:val="off"/>
                <w:spacing w:line="360" w:line-rule="auto"/>
            </w:pPr>
            <w:rPr>
                <w:rFonts w:hint="fareast"/>
            </w:rPr>
        </w:style>
        <w:style w:type="paragraph" w:styleId="a15">
            <w:name w:val="Char1 Char Char Char Char Char Char Char Char Char Char Char Char Char Char Char"/>
            <w:basedOn w:val="a1"/>
            <w:pPr>
                <w:spacing w:line="180" w:line-rule="auto"/>
                <w:ind w:first-line="200" w:first-line-chars="200"/>
            </w:pPr>
            <w:rPr>
                <w:rFonts w:hint="fareast"/>
            </w:rPr>
        </w:style>
        <w:style w:type="paragraph" w:styleId="a16">
            <w:name w:val="正文格式"/>
            <w:basedOn w:val="a1"/>
            <w:pPr>
                <w:adjustRightInd/>
                <w:spacing w:line="240" w:line-rule="auto"/>
                <w:ind w:first-line="640" w:first-line-chars="200"/>
            </w:pPr>
            <w:rPr>
                <w:rFonts w:ascii="仿宋_GB2312" w:h-ansi="宋体" w:fareast="仿宋_GB2312" w:hint="fareast"/>
                <w:color w:val="000000"/>
                <w:sz w:val="32"/>
            </w:rPr>
        </w:style>
        <w:style w:type="paragraph" w:styleId="a17">
            <w:name w:val="Char1 Char Char Char"/>
            <w:basedOn w:val="a1"/>
            <w:pPr>
                <w:widowControl/>
                <w:spacing w:after="160" w:after-autospacing="off" w:line="240" w:line-rule="exact"/>
                <w:jc w:val="left"/>
            </w:pPr>
            <w:rPr>
                <w:rFonts w:hint="fareast"/>
            </w:rPr>
        </w:style>
        <w:style w:type="paragraph" w:styleId="a18">
            <w:name w:val="p0"/>
            <w:basedOn w:val="a1"/>
            <w:pPr>
                <w:widowControl/>
                <w:spacing w:line="240" w:line-rule="at-least"/>
            </w:pPr>
            <w:rPr>
                <w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman" w:hint="fareast"/>
                <w:sz w:val="32"/>
            </w:rPr>
        </w:style>
    </w:styles>
    <w:bgPict>
        <w:background w:bgcolor="#FFFFFF"/>
        <v:background id="_x0000_s1025">
            <v:fill on="t" color2="#FFFFFF" focussize="0,0"/>
        </v:background>
    </w:bgPict>
    <w:docPr>
        <w:view w:val="print"/>
        <w:zoom w:percent="100"/>
        <w:characterSpacingControl w:val="CompressPunctuation"/>
        <w:documentProtection w:enforcement="off"/>
        <w:punctuationKerning/>
        <w:doNotEmbedSystemFonts/>
        <w:bordersDontSurroundHeader/>
        <w:bordersDontSurroundFooter/>
        <w:defaultTabStop w:val="420"/>
        <w:drawingGridVerticalSpacing w:val="156"/>
        <w:displayHorizontalDrawingGridEvery w:val="0"/>
        <w:displayVerticalDrawingGridEvery w:val="2"/>
        <w:compat>
            <w:adjustLineHeightInTable/>
            <w:ulTrailSpace/>
            <w:doNotExpandShiftReturn/>
            <w:balanceSingleByteDoubleByteWidth/>
            <w:useFELayout/>
            <w:spaceForUL/>
            <w:wrapTextWithPunct/>
            <w:breakWrappedTables/>
            <w:useAsianBreakRules/>
            <w:dontGrowAutofit/>
            <w:useFELayout/>
        </w:compat>
    </w:docPr>
    <w:body>
        <wx:sect>
            <w:p>
                <w:pPr>
                    <w:spacing w:line="580" w:line-rule="exact"/>
                    <w:jc w:val="center"/>
                    <w:rPr>
                        <w:rFonts w:fareast="金山简标宋" w:hint="fareast"/>
                        <w:b/>
                        <w:sz w:val="44"/>
                    </w:rPr>
                </w:pPr>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:spacing w:line="580" w:line-rule="exact"/>
                    <w:rPr>
                        <w:rFonts w:fareast="金山简标宋" w:hint="fareast"/>
                        <w:b/>
                        <w:sz w:val="44"/>
                    </w:rPr>
                </w:pPr>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:spacing w:line="580" w:line-rule="exact"/>
                    <w:rPr>
                        <w:rFonts w:fareast="金山简标宋" w:hint="fareast"/>
                        <w:b/>
                        <w:sz w:val="44"/>
                    </w:rPr>
                </w:pPr>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:spacing w:line="580" w:line-rule="exact"/>
                    <w:rPr>
                        <w:rFonts w:fareast="金山简标宋" w:hint="default"/>
                        <w:b/>
                        <w:sz w:val="44"/>
                    </w:rPr>
                </w:pPr>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:spacing w:line="540" w:line-rule="exact"/>
                    <w:jc w:val="center"/>
                    <w:textAlignment w:val="baseline"/>
                    <w:rPr>
                        <w:rFonts w:ascii="金山简标宋" w:fareast="金山简标宋" w:hint="fareast"/>
                        <w:b/>
                        <w:sz w:val="44"/>
                        <w:sz-cs w:val="44"/>
                    </w:rPr>
                </w:pPr>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="金山简标宋" w:fareast="金山简标宋" w:cs="Times New Roman" w:hint="fareast"/>
                        <w:b/>
                        <w:sz w:val="44"/>
                        <w:sz-cs w:val="44"/>
                    </w:rPr>
                    <w:t>${meetingNoticeName}</w:t>
                </w:r>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:spacing w:line="560" w:line-rule="exact"/>
                    <w:jc w:val="center"/>
                    <w:textAlignment w:val="baseline"/>
                    <w:outlineLvl w:val="0"/>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="default"/>
                        <w:sz w:val="32"/>
                    </w:rPr>
                </w:pPr>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
                        <w:sz w:val="32"/>
                    </w:rPr>
                    <w:t>（</w:t>
                </w:r>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:cs="宋体" w:hint="fareast"/>
                        <w:sz w:val="32"/>
                        <w:sz-cs w:val="32"/>
                    </w:rPr>
                    <w:t>${yearNum}</w:t>
                </w:r>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="default"/>
                        <w:sz w:val="32"/>
                    </w:rPr>
                    <w:t>年第</w:t>
                </w:r>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:cs="宋体" w:hint="fareast"/>
                        <w:sz w:val="32"/>
                        <w:sz-cs w:val="32"/>
                    </w:rPr>
                    <w:t>${orderNum}</w:t>
                </w:r>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="default"/>
                        <w:sz w:val="32"/>
                    </w:rPr>
                    <w:t>次）</w:t>
                </w:r>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:spacing w:line="560" w:line-rule="exact"/>
                    <w:ind w:first-line="640" w:first-line-chars="200"/>
                    <w:textAlignment w:val="baseline"/>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
                        <w:sz w:val="32"/>
                    </w:rPr>
                </w:pPr>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:listPr>
                        <w:ilvl w:val="0"/>
                        <w:ilfo w:val="1"/>
                    </w:listPr>
                    <w:spacing w:line="520" w:line-rule="exact"/>
                    <w:ind w:right="-874" w:right-chars="-416" w:first-line="640" w:first-line-chars="200"/>
                    <w:textAlignment w:val="baseline"/>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
                        <w:sz w:val="32"/>
                    </w:rPr>
                </w:pPr>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="default"/>
                        <w:sz w:val="32"/>
                    </w:rPr>
                    <w:t>时  </w:t>
                </w:r>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
                        <w:sz w:val="32"/>
                    </w:rPr>
                    <w:t>间：</w:t>
                </w:r>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:cs="宋体" w:hint="fareast"/>
                        <w:sz w:val="32"/>
                        <w:sz-cs w:val="32"/>
                    </w:rPr>
                    <w:t>${meetingTime}</w:t>
                </w:r>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:spacing w:line="520" w:line-rule="exact"/>
                    <w:ind w:right="-874" w:right-chars="-416" w:first-line="640" w:first-line-chars="200"/>
                    <w:textAlignment w:val="baseline"/>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="default"/>
                        <w:color w:val="0000FF"/>
                        <w:sz w:val="32"/>
                    </w:rPr>
                </w:pPr>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="default"/>
                        <w:sz w:val="32"/>
                    </w:rPr>
                    <w:t>2.地  </w:t>
                </w:r>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
                        <w:sz w:val="32"/>
                    </w:rPr>
                    <w:t>点：</w:t>
                </w:r>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:cs="宋体" w:hint="fareast"/>
                        <w:sz w:val="32"/>
                        <w:sz-cs w:val="32"/>
                    </w:rPr>
                    <w:t>${meetingPlace}</w:t>
                </w:r>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:spacing w:line="500" w:line-rule="exact"/>
                    <w:ind w:first-line="640" w:first-line-chars="200"/>
                    <w:textAlignment w:val="baseline"/>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
                        <w:sz w:val="32"/>
                    </w:rPr>
                </w:pPr>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="default"/>
                        <w:color w:val="000000"/>
                        <w:sz w:val="32"/>
                    </w:rPr>
                    <w:t>3.主持人：</w:t>
                </w:r>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:cs="宋体" w:hint="fareast"/>
                        <w:sz w:val="32"/>
                        <w:sz-cs w:val="32"/>
                    </w:rPr>
                    <w:t>${moderator}</w:t>
                </w:r>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:spacing w:line="500" w:line-rule="exact"/>
                    <w:ind w:first-line="640" w:first-line-chars="200"/>
                    <w:textAlignment w:val="baseline"/>
                    <w:rPr>
                        <w:rFonts w:ascii="方正仿宋_GBK" w:h-ansi="方正仿宋_GBK" w:fareast="方正仿宋_GBK" w:hint="fareast"/>
                        <w:sz w:val="32"/>
                    </w:rPr>
                </w:pPr>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="default"/>
                        <w:sz w:val="32"/>
                    </w:rPr>
                    <w:t>4</w:t>
                </w:r>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
                        <w:sz w:val="32"/>
                    </w:rPr>
                    <w:t>.</w:t>
                </w:r>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="default"/>
                        <w:sz w:val="32"/>
                    </w:rPr>
                    <w:t>出  </w:t>
                </w:r>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
                        <w:sz w:val="32"/>
                    </w:rPr>
                    <w:t>席：</w:t>
                </w:r>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:cs="宋体" w:hint="fareast"/>
                        <w:sz w:val="32"/>
                        <w:sz-cs w:val="32"/>
                    </w:rPr>
                    <w:t>${attendees}</w:t>
                </w:r>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:spacing w:line="470" w:line-rule="exact"/>
                    <w:ind w:first-line="640"/>
                    <w:textAlignment w:val="baseline"/>
                    <w:outlineLvl w:val="0"/>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="default"/>
                        <w:color w:val="000000"/>
                        <w:sz w:val="32"/>
                    </w:rPr>
                </w:pPr>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
                        <w:color w:val="000000"/>
                        <w:sz w:val="32"/>
                    </w:rPr>
                    <w:t>5.请  假</w:t>
                </w:r>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="default"/>
                        <w:color w:val="000000"/>
                        <w:sz w:val="32"/>
                    </w:rPr>
                    <w:t>：</w:t>
                </w:r>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:cs="宋体" w:hint="fareast"/>
                        <w:sz w:val="32"/>
                        <w:sz-cs w:val="32"/>
                    </w:rPr>
                    <w:t>${leave}</w:t>
                </w:r>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:spacing w:line="470" w:line-rule="exact"/>
                    <w:ind w:first-line="645"/>
                    <w:textAlignment w:val="baseline"/>
                    <w:outlineLvl w:val="0"/>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
                        <w:color w:val="0000FF"/>
                        <w:sz w:val="32"/>
                        <w:highlight w:val="yellow"/>
                        <w:u w:val="single"/>
                    </w:rPr>
                </w:pPr>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
                        <w:color w:val="000000"/>
                        <w:sz w:val="32"/>
                    </w:rPr>
                    <w:t>6.列  席</w:t>
                </w:r>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="default"/>
                        <w:color w:val="000000"/>
                        <w:sz w:val="32"/>
                    </w:rPr>
                    <w:t>：</w:t>
                </w:r>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:cs="宋体" w:hint="fareast"/>
                        <w:sz w:val="32"/>
                        <w:sz-cs w:val="32"/>
                    </w:rPr>
                    <w:t>${attendances}</w:t>
                </w:r>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:spacing w:after="100" w:after-autospacing="on" w:line="520" w:line-rule="exact"/>
                    <w:ind w:first-line="646"/>
                    <w:textAlignment w:val="baseline"/>
                    <w:outlineLvl w:val="0"/>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="default"/>
                        <w:color w:val="000000"/>
                        <w:sz w:val="32"/>
                    </w:rPr>
                </w:pPr>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
                        <w:color w:val="000000"/>
                        <w:sz w:val="32"/>
                    </w:rPr>
                    <w:t>7.</w:t>
                </w:r>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="default"/>
                        <w:color w:val="000000"/>
                        <w:sz w:val="32"/>
                    </w:rPr>
                    <w:t>会议内容：</w:t>
                </w:r>
            </w:p>
            <w:tbl>
                <w:tblPr>
                    <w:tblW w:w="8060" w:type="dxa"/>
                    <w:jc w:val="center"/>
                    <w:tblInd w:w="0" w:type="dxa"/>
                    <w:tblBorders>
                        <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                        <w:insideH w:val="nil"/>
                        <w:insideV w:val="nil"/>
                    </w:tblBorders>
                    <w:tblLayout w:type="Fixed"/>
                    <w:tblCellMar>
                        <w:top w:w="0" w:type="dxa"/>
                        <w:left w:w="108" w:type="dxa"/>
                        <w:bottom w:w="0" w:type="dxa"/>
                        <w:right w:w="108" w:type="dxa"/>
                    </w:tblCellMar>
                </w:tblPr>
                <w:tblGrid>
                    <w:gridCol w:w="509"/>
                    <w:gridCol w:w="3518"/>
                    <w:gridCol w:w="1217"/>
                    <w:gridCol w:w="1764"/>
                    <w:gridCol w:w="1052"/>
                </w:tblGrid>
                <w:tr>
                    <w:tblPrEx>
                        <w:tblBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:insideH w:val="nil"/>
                            <w:insideV w:val="nil"/>
                        </w:tblBorders>
                        <w:tblCellMar>
                            <w:top w:w="0" w:type="dxa"/>
                            <w:left w:w="108" w:type="dxa"/>
                            <w:bottom w:w="0" w:type="dxa"/>
                            <w:right w:w="108" w:type="dxa"/>
                        </w:tblCellMar>
                    </w:tblPrEx>
                    <w:trPr>
                        <w:cantSplit w:val="on"/>
                        <w:trHeight w:val="764" w:h-rule="atLeast"/>
                        <w:jc w:val="center"/>
                    </w:trPr>
                    <w:tc>
                        <w:tcPr>
                            <w:tcW w:w="4027" w:type="dxa"/>
                            <w:gridSpan w:val="2"/>
                            <w:tcBorders>
                                <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                                <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                                <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                                <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            </w:tcBorders>
                            <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                            <w:noWrap w:val="0"/>
                            <w:vAlign w:val="center"/>
                        </w:tcPr>
                        <w:p>
                            <w:pPr>
                                <w:spacing w:line="460" w:line-rule="exact"/>
                                <w:jc w:val="center"/>
                                <w:rPr>
                                    <w:rFonts w:ascii="宋体" w:hint="default"/>
                                    <w:b/>
                                    <w:sz w:val="30"/>
                                </w:rPr>
                            </w:pPr>
                            <w:r>
                                <w:rPr>
                                    <w:rFonts w:ascii="宋体" w:hint="fareast"/>
                                    <w:b/>
                                    <w:sz w:val="30"/>
                                </w:rPr>
                                <w:t>会 议 内 容</w:t>
                            </w:r>
                        </w:p>
                    </w:tc>
                    <w:tc>
                        <w:tcPr>
                            <w:tcW w:w="1217" w:type="dxa"/>
                            <w:tcBorders>
                                <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                                <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                                <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                                <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            </w:tcBorders>
                            <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                            <w:noWrap w:val="0"/>
                            <w:vAlign w:val="center"/>
                        </w:tcPr>
                        <w:p>
                            <w:pPr>
                                <w:spacing w:line="460" w:line-rule="exact"/>
                                <w:jc w:val="center"/>
                                <w:rPr>
                                    <w:rFonts w:ascii="宋体" w:hint="default"/>
                                    <w:b/>
                                    <w:sz w:val="30"/>
                                </w:rPr>
                            </w:pPr>
                            <w:r>
                                <w:rPr>
                                    <w:rFonts w:ascii="宋体" w:hint="fareast"/>
                                    <w:b/>
                                    <w:sz w:val="30"/>
                                </w:rPr>
                                <w:t>汇报人</w:t>
                            </w:r>
                        </w:p>
                    </w:tc>
                    <w:tc>
                        <w:tcPr>
                            <w:tcW w:w="1764" w:type="dxa"/>
                            <w:tcBorders>
                                <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                                <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                                <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                                <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            </w:tcBorders>
                            <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                            <w:noWrap w:val="0"/>
                            <w:vAlign w:val="center"/>
                        </w:tcPr>
                        <w:p>
                            <w:pPr>
                                <w:spacing w:line="460" w:line-rule="exact"/>
                                <w:jc w:val="center"/>
                                <w:rPr>
                                    <w:rFonts w:ascii="宋体" w:hint="default"/>
                                    <w:b/>
                                    <w:sz w:val="30"/>
                                </w:rPr>
                            </w:pPr>
                            <w:r>
                                <w:rPr>
                                    <w:rFonts w:ascii="宋体" w:hint="fareast"/>
                                    <w:b/>
                                    <w:sz w:val="30"/>
                                </w:rPr>
                                <w:t>列席人</w:t>
                            </w:r>
                        </w:p>
                    </w:tc>
                    <w:tc>
                        <w:tcPr>
                            <w:tcW w:w="1052" w:type="dxa"/>
                            <w:tcBorders>
                                <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                                <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                                <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                                <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            </w:tcBorders>
                            <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                            <w:noWrap w:val="0"/>
                            <w:vAlign w:val="center"/>
                        </w:tcPr>
                        <w:p>
                            <w:pPr>
                                <w:spacing w:line="460" w:line-rule="exact"/>
                                <w:jc w:val="center"/>
                                <w:rPr>
                                    <w:rFonts w:ascii="宋体" w:hint="default"/>
                                    <w:sz w:val="30"/>
                                </w:rPr>
                            </w:pPr>
                            <w:r>
                                <w:rPr>
                                    <w:rFonts w:ascii="宋体" w:hint="fareast"/>
                                    <w:b/>
                                    <w:sz w:val="30"/>
                                </w:rPr>
                                <w:t>时 间</w:t>
                            </w:r>
                        </w:p>
                    </w:tc>
                </w:tr>
				<#list subjectList as subject>
                <w:tr>
                    <w:tblPrEx>
                        <w:tblBorders>
                            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            <w:insideH w:val="nil"/>
                            <w:insideV w:val="nil"/>
                        </w:tblBorders>
                        <w:tblCellMar>
                            <w:top w:w="0" w:type="dxa"/>
                            <w:left w:w="108" w:type="dxa"/>
                            <w:bottom w:w="0" w:type="dxa"/>
                            <w:right w:w="108" w:type="dxa"/>
                        </w:tblCellMar>
                    </w:tblPrEx>
                    <w:trPr>
                        <w:trHeight w:val="1200" w:h-rule="atLeast"/>
                        <w:jc w:val="center"/>
                    </w:trPr>
                    <w:tc>
                        <w:tcPr>
                            <w:tcW w:w="509" w:type="dxa"/>
                            <w:tcBorders>
                                <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                                <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                                <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                                <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            </w:tcBorders>
                            <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                            <w:noWrap w:val="0"/>
                            <w:vAlign w:val="center"/>
                        </w:tcPr>
                        <w:p>
                            <w:pPr>
                                <w:spacing w:line="300" w:line-rule="exact"/>
                                <w:jc w:val="center"/>
                                <w:textAlignment w:val="baseline"/>
                                <w:rPr>
                                    <w:rFonts w:ascii="方正仿宋_GBK" w:h-ansi="宋体" w:fareast="方正仿宋_GBK" w:hint="default"/>
                                    <w:color w:val="000000"/>
                                    <w:sz w:val="24"/>
                                </w:rPr>
                            </w:pPr>
                            <w:r>
                                <w:rPr>
                                    <w:rFonts w:ascii="方正仿宋_GBK" w:h-ansi="宋体" w:fareast="方正仿宋_GBK" w:hint="fareast"/>
                                    <w:color w:val="000000"/>
                                    <w:sz w:val="24"/>
                                </w:rPr>
                                <w:t>${subject.num}</w:t>
                            </w:r>
                        </w:p>
                    </w:tc>
                    <w:tc>
                        <w:tcPr>
                            <w:tcW w:w="3518" w:type="dxa"/>
                            <w:tcBorders>
                                <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                                <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                                <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                                <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            </w:tcBorders>
                            <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                            <w:noWrap w:val="0"/>
                            <w:vAlign w:val="center"/>
                        </w:tcPr>
                        <w:p>
                            <w:pPr>
                                <w:spacing w:line="300" w:line-rule="exact"/>
                                <w:textAlignment w:val="baseline"/>
                                <w:rPr>
                                    <w:rFonts w:ascii="方正仿宋_GBK" w:h-ansi="宋体" w:fareast="方正仿宋_GBK" w:hint="fareast"/>
                                    <w:color w:val="000000"/>
                                    <w:sz w:val="24"/>
                                </w:rPr>
                            </w:pPr>
                            <w:r>
                                <w:rPr>
                                    <w:rFonts w:ascii="方正仿宋_GBK" w:h-ansi="宋体" w:fareast="方正仿宋_GBK" w:hint="fareast"/>
                                    <w:sz w:val="24"/>
                                </w:rPr>
                                <w:t>听取关于</w:t>
                            </w:r>
                            <w:r>
                                <w:rPr>
                                    <w:rFonts w:ascii="方正仿宋_GBK" w:fareast="方正仿宋_GBK" w:hint="fareast"/>
                                    <w:sz w:val="24"/>
                                    <w:sz-cs w:val="24"/>
                                </w:rPr>
                                <w:t>${subject.name}</w:t>
                            </w:r>
                            <w:r>
                                <w:rPr>
                                    <w:rFonts w:ascii="方正仿宋_GBK" w:h-ansi="宋体" w:fareast="方正仿宋_GBK" w:hint="fareast"/>
                                    <w:sz w:val="24"/>
                                </w:rPr>
                                <w:t>的汇报</w:t>
                            </w:r>
                        </w:p>
                    </w:tc>
                    <w:tc>
                        <w:tcPr>
                            <w:tcW w:w="1217" w:type="dxa"/>
                            <w:tcBorders>
                                <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                                <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                                <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                                <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            </w:tcBorders>
                            <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                            <w:noWrap w:val="0"/>
                            <w:vAlign w:val="center"/>
                        </w:tcPr>
                        <w:p>
                            <w:pPr>
                                <w:spacing w:line="300" w:line-rule="exact"/>
                                <w:jc w:val="center"/>
                                <w:textAlignment w:val="baseline"/>
                                <w:rPr>
                                    <w:rFonts w:ascii="方正仿宋_GBK" w:h-ansi="宋体" w:fareast="方正仿宋_GBK" w:hint="fareast"/>
                                    <w:sz w:val="24"/>
                                </w:rPr>
                            </w:pPr>
                            <w:r>
                                <w:rPr>
                                    <w:rFonts w:ascii="方正仿宋_GBK" w:h-ansi="宋体" w:fareast="方正仿宋_GBK" w:hint="fareast"/>
                                    <w:sz w:val="24"/>
                                </w:rPr>
                                <w:t>${subject.approve}</w:t>
                            </w:r>
                        </w:p>
                    </w:tc>
                    <w:tc>
                        <w:tcPr>
                            <w:tcW w:w="1764" w:type="dxa"/>
                            <w:tcBorders>
                                <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                                <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                                <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                                <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            </w:tcBorders>
                            <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                            <w:noWrap w:val="0"/>
                            <w:vAlign w:val="center"/>
                        </w:tcPr>
                        <w:p>
                            <w:pPr>
                                <w:spacing w:line="300" w:line-rule="exact"/>
                                <w:jc w:val="center"/>
                                <w:textAlignment w:val="baseline"/>
                                <w:rPr>
                                    <w:rFonts w:ascii="方正仿宋_GBK" w:h-ansi="宋体" w:fareast="方正仿宋_GBK" w:hint="fareast"/>
                                    <w:color w:val="000000"/>
                                    <w:sz w:val="24"/>
                                    <w:sz-cs w:val="24"/>
                                </w:rPr>
                            </w:pPr>
                            <w:r>
                                <w:rPr>
                                    <w:rFonts w:ascii="方正仿宋_GBK" w:fareast="方正仿宋_GBK" w:hint="fareast"/>
                                    <w:sz w:val="24"/>
                                    <w:sz-cs w:val="24"/>
                                </w:rPr>
                                <w:t>${subject.attendances}</w:t>
                            </w:r>
                        </w:p>
                    </w:tc>
                    <w:tc>
                        <w:tcPr>
                            <w:tcW w:w="1052" w:type="dxa"/>
                            <w:tcBorders>
                                <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                                <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                                <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                                <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
                            </w:tcBorders>
                            <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                            <w:noWrap w:val="0"/>
                            <w:vAlign w:val="center"/>
                        </w:tcPr>
                        <w:p>
                            <w:pPr>
                                <w:spacing w:line="300" w:line-rule="exact"/>
                                <w:jc w:val="center"/>
                                <w:textAlignment w:val="baseline"/>
                                <w:rPr>
                                    <w:rFonts w:ascii="方正仿宋_GBK" w:h-ansi="宋体" w:fareast="方正仿宋_GBK" w:hint="fareast"/>
                                    <w:color w:val="000000"/>
                                    <w:sz w:val="24"/>
                                    <w:sz-cs w:val="24"/>
                                </w:rPr>
                            </w:pPr>
                            <w:r>
                                <w:rPr>
                                    <w:rFonts w:ascii="方正仿宋_GBK" w:fareast="方正仿宋_GBK" w:hint="fareast"/>
                                    <w:sz w:val="24"/>
                                    <w:sz-cs w:val="24"/>
                                </w:rPr>
                                <w:t>${subject.time}</w:t>
                            </w:r>
                        </w:p>
                    </w:tc>
                </w:tr>
				</#list>
            </w:tbl>
            <w:p>
                <w:pPr>
                    <w:spacing w:line="520" w:line-rule="exact"/>
                    <w:ind w:first-line="645"/>
                    <w:textAlignment w:val="baseline"/>
                    <w:outlineLvl w:val="0"/>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="default"/>
                        <w:color w:val="000000"/>
                        <w:sz w:val="28"/>
                    </w:rPr>
                </w:pPr>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
                        <w:color w:val="000000"/>
                        <w:sz w:val="30"/>
                    </w:rPr>
                    <w:t>8.</w:t>
                </w:r>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="default"/>
                        <w:color w:val="000000"/>
                        <w:sz w:val="30"/>
                    </w:rPr>
                    <w:t>会议记录：</w:t>
                </w:r>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:cs="宋体" w:hint="fareast"/>
                        <w:sz w:val="32"/>
                        <w:sz-cs w:val="32"/>
                    </w:rPr>
                    <w:t>${meetingRecord}</w:t>
                </w:r>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:spacing w:line="520" w:line-rule="exact"/>
                    <w:ind w:left="4620" w:first-line="420" w:first-line-chars="0"/>
                    <w:jc w:val="both"/>
                    <w:textAlignment w:val="baseline"/>
                    <w:outlineLvl w:val="0"/>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:cs="宋体" w:hint="fareast"/>
                        <w:sz w:val="32"/>
                        <w:sz-cs w:val="32"/>
                    </w:rPr>
                </w:pPr>
                <w:r>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:cs="宋体" w:hint="fareast"/>
                        <w:sz w:val="32"/>
                        <w:sz-cs w:val="32"/>
                    </w:rPr>
                    <w:t>${time}</w:t>
                </w:r>
            </w:p>
            <w:p>
                <w:pPr>
                    <w:rPr>
                        <w:rFonts w:hint="fareast"/>
                    </w:rPr>
                </w:pPr>
            </w:p>
            <w:sectPr>
                <w:hdr w:type="odd">
                    <w:p>
                        <w:pPr>
                            <w:pStyle w:val="a5"/>
                            <w:pBdr>
                                <w:bottom w:val="nil"/>
                            </w:pBdr>
                            <w:tabs>
                                <w:tab w:val="center" w:pos="4153"/>
                                <w:tab w:val="right" w:pos="8306"/>
                            </w:tabs>
                        </w:pPr>
                    </w:p>
                </w:hdr>
                <w:ftr w:type="odd">
                    <w:p>
                        <w:pPr>
                            <w:pStyle w:val="a4"/>
                            <w:tabs>
                                <w:tab w:val="center" w:pos="4153"/>
                                <w:tab w:val="right" w:pos="8306"/>
                            </w:tabs>
                            <w:jc w:val="center"/>
                        </w:pPr>
                        <w:r>
                            <w:fldChar w:fldCharType="begin"/>
                        </w:r>
                        <w:r>
                            <w:instrText> PAGE   \* MERGEFORMAT </w:instrText>
                        </w:r>
                        <w:r>
                            <w:fldChar w:fldCharType="separate"/>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:lang w:val="ZH-CN"/>
                            </w:rPr>
                            <w:t>17</w:t>
                        </w:r>
                        <w:r>
                            <w:fldChar w:fldCharType="end"/>
                        </w:r>
                    </w:p>
                    <w:p>
                        <w:pPr>
                            <w:pStyle w:val="a4"/>
                            <w:tabs>
                                <w:tab w:val="center" w:pos="4153"/>
                                <w:tab w:val="right" w:pos="8306"/>
                            </w:tabs>
                        </w:pPr>
                    </w:p>
                </w:ftr>
                <w:pgSz w:w="11906" w:h="16838"/>
                <w:pgMar w:top="2097" w:right="1473" w:bottom="1984" w:left="1473" w:header="1417" w:footer="1417" w:gutter="0"/>
                <w:cols w:space="720"/>
                <w:docGrid w:line-pitch="312"/>
            </w:sectPr>
        </wx:sect>
    </w:body>
</w:wordDocument>