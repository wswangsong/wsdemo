<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<?mso-application progid="Word.Document"?>
<w:wordDocument xmlns:w="http://schemas.microsoft.com/office/word/2003/wordml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:sl="http://schemas.microsoft.com/schemaLibrary/2003/core" xmlns:aml="http://schemas.microsoft.com/aml/2001/core" xmlns:wx="http://schemas.microsoft.com/office/word/2003/auxHint" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882" xmlns:wpsCustomData="http://www.wps.cn/officeDocument/2013/wpsCustomData" w:macrosPresent="no" w:embeddedObjPresent="no" w:ocxPresent="no" xml:space="preserve">
  <o:DocumentProperties>
    <o:Author>44166</o:Author>
    <o:LastAuthor>蓝骑士</o:LastAuthor>
    <o:Created>2021-04-01T07:12:00Z</o:Created>
    <o:LastSaved>2021-09-03T09:35:58Z</o:LastSaved>
    <o:TotalTime>1440</o:TotalTime>
    <o:Pages>1</o:Pages>
    <o:Words>0</o:Words>
    <o:Characters>0</o:Characters>
    <o:Lines>0</o:Lines>
    <o:Paragraphs>0</o:Paragraphs>
    <o:CharactersWithSpaces>0</o:CharactersWithSpaces>
    <o:Version>14</o:Version>
  </o:DocumentProperties>
  <o:CustomDocumentProperties>
    <o:KSOProductBuildVer dt:dt="string">2052-11.1.0.10700</o:KSOProductBuildVer>
    <o:ICV dt:dt="string">31F8645A37D340CB809BC7C12A694251</o:ICV>
  </o:CustomDocumentProperties>
  <w:fonts>
    <w:defaultFonts w:ascii="Calibri" w:fareast="宋体" w:h-ansi="Calibri" w:cs="Times New Roman"/>
    <w:font w:name="Times New Roman">
      <w:panose-1 w:val="02020603050405020304"/>
      <w:charset w:val="00"/>
      <w:family w:val="Auto"/>
      <w:pitch w:val="Default"/>
      <w:sig w:usb-0="E0002EFF" w:usb-1="C000785B" w:usb-2="00000009" w:usb-3="00000000" w:csb-0="400001FF" w:csb-1="FFFF0000"/>
    </w:font>
    <w:font w:name="宋体">
      <w:panose-1 w:val="02010600030101010101"/>
      <w:charset w:val="86"/>
      <w:family w:val="Auto"/>
      <w:pitch w:val="Default"/>
      <w:sig w:usb-0="00000003" w:usb-1="288F0000" w:usb-2="00000006" w:usb-3="00000000" w:csb-0="00040001" w:csb-1="00000000"/>
    </w:font>
    <w:font w:name="Wingdings">
      <w:panose-1 w:val="05000000000000000000"/>
      <w:charset w:val="02"/>
      <w:family w:val="Auto"/>
      <w:pitch w:val="Default"/>
      <w:sig w:usb-0="00000000" w:usb-1="00000000" w:usb-2="00000000" w:usb-3="00000000" w:csb-0="80000000" w:csb-1="00000000"/>
    </w:font>
    <w:font w:name="Arial">
      <w:panose-1 w:val="020B0604020202020204"/>
      <w:charset w:val="01"/>
      <w:family w:val="SWiss"/>
      <w:pitch w:val="Default"/>
      <w:sig w:usb-0="E0002EFF" w:usb-1="C000785B" w:usb-2="00000009" w:usb-3="00000000" w:csb-0="400001FF" w:csb-1="FFFF0000"/>
    </w:font>
    <w:font w:name="黑体">
      <w:panose-1 w:val="02010609060101010101"/>
      <w:charset w:val="86"/>
      <w:family w:val="Auto"/>
      <w:pitch w:val="Default"/>
      <w:sig w:usb-0="800002BF" w:usb-1="38CF7CFA" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="00040001" w:csb-1="00000000"/>
    </w:font>
    <w:font w:name="Courier New">
      <w:panose-1 w:val="02070309020205020404"/>
      <w:charset w:val="01"/>
      <w:family w:val="Modern"/>
      <w:pitch w:val="Default"/>
      <w:sig w:usb-0="E0002EFF" w:usb-1="C0007843" w:usb-2="00000009" w:usb-3="00000000" w:csb-0="400001FF" w:csb-1="FFFF0000"/>
    </w:font>
    <w:font w:name="Symbol">
      <w:panose-1 w:val="05050102010706020507"/>
      <w:charset w:val="02"/>
      <w:family w:val="Roman"/>
      <w:pitch w:val="Default"/>
      <w:sig w:usb-0="00000000" w:usb-1="00000000" w:usb-2="00000000" w:usb-3="00000000" w:csb-0="80000000" w:csb-1="00000000"/>
    </w:font>
    <w:font w:name="Calibri">
      <w:panose-1 w:val="020F0502020204030204"/>
      <w:charset w:val="00"/>
      <w:family w:val="SWiss"/>
      <w:pitch w:val="Default"/>
      <w:sig w:usb-0="E4002EFF" w:usb-1="C000247B" w:usb-2="00000009" w:usb-3="00000000" w:csb-0="200001FF" w:csb-1="00000000"/>
    </w:font>
    <w:font w:name="微软雅黑">
      <w:panose-1 w:val="020B0503020204020204"/>
      <w:charset w:val="86"/>
      <w:family w:val="Auto"/>
      <w:pitch w:val="Default"/>
      <w:sig w:usb-0="80000287" w:usb-1="2ACF3C50" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="0004001F" w:csb-1="00000000"/>
    </w:font>
    <w:font w:name="Tahoma">
      <w:panose-1 w:val="020B0604030504040204"/>
      <w:charset w:val="00"/>
      <w:family w:val="Auto"/>
      <w:pitch w:val="Default"/>
      <w:sig w:usb-0="E1002EFF" w:usb-1="C000605B" w:usb-2="00000029" w:usb-3="00000000" w:csb-0="200101FF" w:csb-1="20280000"/>
    </w:font>
  </w:fonts>
  <w:styles>
    <w:latentStyles w:defLockedState="off" w:latentStyleCount="260">
      <w:lsdException w:name="Normal"/>
      <w:lsdException w:name="heading 1"/>
      <w:lsdException w:name="heading 2"/>
      <w:lsdException w:name="heading 3"/>
      <w:lsdException w:name="heading 4"/>
      <w:lsdException w:name="heading 5"/>
      <w:lsdException w:name="heading 6"/>
      <w:lsdException w:name="heading 7"/>
      <w:lsdException w:name="heading 8"/>
      <w:lsdException w:name="heading 9"/>
      <w:lsdException w:name="index 1"/>
      <w:lsdException w:name="index 2"/>
      <w:lsdException w:name="index 3"/>
      <w:lsdException w:name="index 4"/>
      <w:lsdException w:name="index 5"/>
      <w:lsdException w:name="index 6"/>
      <w:lsdException w:name="index 7"/>
      <w:lsdException w:name="index 8"/>
      <w:lsdException w:name="index 9"/>
      <w:lsdException w:name="toc 1"/>
      <w:lsdException w:name="toc 2"/>
      <w:lsdException w:name="toc 3"/>
      <w:lsdException w:name="toc 4"/>
      <w:lsdException w:name="toc 5"/>
      <w:lsdException w:name="toc 6"/>
      <w:lsdException w:name="toc 7"/>
      <w:lsdException w:name="toc 8"/>
      <w:lsdException w:name="toc 9"/>
      <w:lsdException w:name="Normal Indent"/>
      <w:lsdException w:name="footnote text"/>
      <w:lsdException w:name="annotation text"/>
      <w:lsdException w:name="header"/>
      <w:lsdException w:name="footer"/>
      <w:lsdException w:name="index heading"/>
      <w:lsdException w:name="caption"/>
      <w:lsdException w:name="table of figures"/>
      <w:lsdException w:name="envelope address"/>
      <w:lsdException w:name="envelope return"/>
      <w:lsdException w:name="footnote reference"/>
      <w:lsdException w:name="annotation reference"/>
      <w:lsdException w:name="line number"/>
      <w:lsdException w:name="page number"/>
      <w:lsdException w:name="endnote reference"/>
      <w:lsdException w:name="endnote text"/>
      <w:lsdException w:name="table of authorities"/>
      <w:lsdException w:name="macro"/>
      <w:lsdException w:name="toa heading"/>
      <w:lsdException w:name="List"/>
      <w:lsdException w:name="List Bullet"/>
      <w:lsdException w:name="List Number"/>
      <w:lsdException w:name="List 2"/>
      <w:lsdException w:name="List 3"/>
      <w:lsdException w:name="List 4"/>
      <w:lsdException w:name="List 5"/>
      <w:lsdException w:name="List Bullet 2"/>
      <w:lsdException w:name="List Bullet 3"/>
      <w:lsdException w:name="List Bullet 4"/>
      <w:lsdException w:name="List Bullet 5"/>
      <w:lsdException w:name="List Number 2"/>
      <w:lsdException w:name="List Number 3"/>
      <w:lsdException w:name="List Number 4"/>
      <w:lsdException w:name="List Number 5"/>
      <w:lsdException w:name="Title"/>
      <w:lsdException w:name="Closing"/>
      <w:lsdException w:name="Signature"/>
      <w:lsdException w:name="Default Paragraph Font"/>
      <w:lsdException w:name="Body Text"/>
      <w:lsdException w:name="Body Text Indent"/>
      <w:lsdException w:name="List Continue"/>
      <w:lsdException w:name="List Continue 2"/>
      <w:lsdException w:name="List Continue 3"/>
      <w:lsdException w:name="List Continue 4"/>
      <w:lsdException w:name="List Continue 5"/>
      <w:lsdException w:name="Message Header"/>
      <w:lsdException w:name="Subtitle"/>
      <w:lsdException w:name="Salutation"/>
      <w:lsdException w:name="Date"/>
      <w:lsdException w:name="Body Text First Indent"/>
      <w:lsdException w:name="Body Text First Indent 2"/>
      <w:lsdException w:name="Note Heading"/>
      <w:lsdException w:name="Body Text 2"/>
      <w:lsdException w:name="Body Text 3"/>
      <w:lsdException w:name="Body Text Indent 2"/>
      <w:lsdException w:name="Body Text Indent 3"/>
      <w:lsdException w:name="Block Text"/>
      <w:lsdException w:name="Hyperlink"/>
      <w:lsdException w:name="FollowedHyperlink"/>
      <w:lsdException w:name="Strong"/>
      <w:lsdException w:name="Emphasis"/>
      <w:lsdException w:name="Document Map"/>
      <w:lsdException w:name="Plain Text"/>
      <w:lsdException w:name="E-mail Signature"/>
      <w:lsdException w:name="Normal (Web)"/>
      <w:lsdException w:name="HTML Acronym"/>
      <w:lsdException w:name="HTML Address"/>
      <w:lsdException w:name="HTML Cite"/>
      <w:lsdException w:name="HTML Code"/>
      <w:lsdException w:name="HTML Definition"/>
      <w:lsdException w:name="HTML Keyboard"/>
      <w:lsdException w:name="HTML Preformatted"/>
      <w:lsdException w:name="HTML Sample"/>
      <w:lsdException w:name="HTML Typewriter"/>
      <w:lsdException w:name="HTML Variable"/>
      <w:lsdException w:name="Normal Table"/>
      <w:lsdException w:name="annotation subject"/>
      <w:lsdException w:name="Table Simple 1"/>
      <w:lsdException w:name="Table Simple 2"/>
      <w:lsdException w:name="Table Simple 3"/>
      <w:lsdException w:name="Table Classic 1"/>
      <w:lsdException w:name="Table Classic 2"/>
      <w:lsdException w:name="Table Classic 3"/>
      <w:lsdException w:name="Table Classic 4"/>
      <w:lsdException w:name="Table Colorful 1"/>
      <w:lsdException w:name="Table Colorful 2"/>
      <w:lsdException w:name="Table Colorful 3"/>
      <w:lsdException w:name="Table Columns 1"/>
      <w:lsdException w:name="Table Columns 2"/>
      <w:lsdException w:name="Table Columns 3"/>
      <w:lsdException w:name="Table Columns 4"/>
      <w:lsdException w:name="Table Columns 5"/>
      <w:lsdException w:name="Table Grid 1"/>
      <w:lsdException w:name="Table Grid 2"/>
      <w:lsdException w:name="Table Grid 3"/>
      <w:lsdException w:name="Table Grid 4"/>
      <w:lsdException w:name="Table Grid 5"/>
      <w:lsdException w:name="Table Grid 6"/>
      <w:lsdException w:name="Table Grid 7"/>
      <w:lsdException w:name="Table Grid 8"/>
      <w:lsdException w:name="Table List 1"/>
      <w:lsdException w:name="Table List 2"/>
      <w:lsdException w:name="Table List 3"/>
      <w:lsdException w:name="Table List 4"/>
      <w:lsdException w:name="Table List 5"/>
      <w:lsdException w:name="Table List 6"/>
      <w:lsdException w:name="Table List 7"/>
      <w:lsdException w:name="Table List 8"/>
      <w:lsdException w:name="Table 3D effects 1"/>
      <w:lsdException w:name="Table 3D effects 2"/>
      <w:lsdException w:name="Table 3D effects 3"/>
      <w:lsdException w:name="Table Contemporary"/>
      <w:lsdException w:name="Table Elegant"/>
      <w:lsdException w:name="Table Professional"/>
      <w:lsdException w:name="Table Subtle 1"/>
      <w:lsdException w:name="Table Subtle 2"/>
      <w:lsdException w:name="Table Web 1"/>
      <w:lsdException w:name="Table Web 2"/>
      <w:lsdException w:name="Table Web 3"/>
      <w:lsdException w:name="Balloon Text"/>
      <w:lsdException w:name="Table Grid"/>
      <w:lsdException w:name="Table Theme"/>
      <w:lsdException w:name="Light Shading"/>
      <w:lsdException w:name="Light List"/>
      <w:lsdException w:name="Light Grid"/>
      <w:lsdException w:name="Medium Shading 1"/>
      <w:lsdException w:name="Medium Shading 2"/>
      <w:lsdException w:name="Medium List 1"/>
      <w:lsdException w:name="Medium List 2"/>
      <w:lsdException w:name="Medium Grid 1"/>
      <w:lsdException w:name="Medium Grid 2"/>
      <w:lsdException w:name="Medium Grid 3"/>
      <w:lsdException w:name="Dark List"/>
      <w:lsdException w:name="Colorful Shading"/>
      <w:lsdException w:name="Colorful List"/>
      <w:lsdException w:name="Colorful Grid"/>
      <w:lsdException w:name="Light Shading Accent 1"/>
      <w:lsdException w:name="Light List Accent 1"/>
      <w:lsdException w:name="Light Grid Accent 1"/>
      <w:lsdException w:name="Medium Shading 1 Accent 1"/>
      <w:lsdException w:name="Medium Shading 2 Accent 1"/>
      <w:lsdException w:name="Medium List 1 Accent 1"/>
      <w:lsdException w:name="Medium List 2 Accent 1"/>
      <w:lsdException w:name="Medium Grid 1 Accent 1"/>
      <w:lsdException w:name="Medium Grid 2 Accent 1"/>
      <w:lsdException w:name="Medium Grid 3 Accent 1"/>
      <w:lsdException w:name="Dark List Accent 1"/>
      <w:lsdException w:name="Colorful Shading Accent 1"/>
      <w:lsdException w:name="Colorful List Accent 1"/>
      <w:lsdException w:name="Colorful Grid Accent 1"/>
      <w:lsdException w:name="Light Shading Accent 2"/>
      <w:lsdException w:name="Light List Accent 2"/>
      <w:lsdException w:name="Light Grid Accent 2"/>
      <w:lsdException w:name="Medium Shading 1 Accent 2"/>
      <w:lsdException w:name="Medium Shading 2 Accent 2"/>
      <w:lsdException w:name="Medium List 1 Accent 2"/>
      <w:lsdException w:name="Medium List 2 Accent 2"/>
      <w:lsdException w:name="Medium Grid 1 Accent 2"/>
      <w:lsdException w:name="Medium Grid 2 Accent 2"/>
      <w:lsdException w:name="Medium Grid 3 Accent 2"/>
      <w:lsdException w:name="Dark List Accent 2"/>
      <w:lsdException w:name="Colorful Shading Accent 2"/>
      <w:lsdException w:name="Colorful List Accent 2"/>
      <w:lsdException w:name="Colorful Grid Accent 2"/>
      <w:lsdException w:name="Light Shading Accent 3"/>
      <w:lsdException w:name="Light List Accent 3"/>
      <w:lsdException w:name="Light Grid Accent 3"/>
      <w:lsdException w:name="Medium Shading 1 Accent 3"/>
      <w:lsdException w:name="Medium Shading 2 Accent 3"/>
      <w:lsdException w:name="Medium List 1 Accent 3"/>
      <w:lsdException w:name="Medium List 2 Accent 3"/>
      <w:lsdException w:name="Medium Grid 1 Accent 3"/>
      <w:lsdException w:name="Medium Grid 2 Accent 3"/>
      <w:lsdException w:name="Medium Grid 3 Accent 3"/>
      <w:lsdException w:name="Dark List Accent 3"/>
      <w:lsdException w:name="Colorful Shading Accent 3"/>
      <w:lsdException w:name="Colorful List Accent 3"/>
      <w:lsdException w:name="Colorful Grid Accent 3"/>
      <w:lsdException w:name="Light Shading Accent 4"/>
      <w:lsdException w:name="Light List Accent 4"/>
      <w:lsdException w:name="Light Grid Accent 4"/>
      <w:lsdException w:name="Medium Shading 1 Accent 4"/>
      <w:lsdException w:name="Medium Shading 2 Accent 4"/>
      <w:lsdException w:name="Medium List 1 Accent 4"/>
      <w:lsdException w:name="Medium List 2 Accent 4"/>
      <w:lsdException w:name="Medium Grid 1 Accent 4"/>
      <w:lsdException w:name="Medium Grid 2 Accent 4"/>
      <w:lsdException w:name="Medium Grid 3 Accent 4"/>
      <w:lsdException w:name="Dark List Accent 4"/>
      <w:lsdException w:name="Colorful Shading Accent 4"/>
      <w:lsdException w:name="Colorful List Accent 4"/>
      <w:lsdException w:name="Colorful Grid Accent 4"/>
      <w:lsdException w:name="Light Shading Accent 5"/>
      <w:lsdException w:name="Light List Accent 5"/>
      <w:lsdException w:name="Light Grid Accent 5"/>
      <w:lsdException w:name="Medium Shading 1 Accent 5"/>
      <w:lsdException w:name="Medium Shading 2 Accent 5"/>
      <w:lsdException w:name="Medium List 1 Accent 5"/>
      <w:lsdException w:name="Medium List 2 Accent 5"/>
      <w:lsdException w:name="Medium Grid 1 Accent 5"/>
      <w:lsdException w:name="Medium Grid 2 Accent 5"/>
      <w:lsdException w:name="Medium Grid 3 Accent 5"/>
      <w:lsdException w:name="Dark List Accent 5"/>
      <w:lsdException w:name="Colorful Shading Accent 5"/>
      <w:lsdException w:name="Colorful List Accent 5"/>
      <w:lsdException w:name="Colorful Grid Accent 5"/>
      <w:lsdException w:name="Light Shading Accent 6"/>
      <w:lsdException w:name="Light List Accent 6"/>
      <w:lsdException w:name="Light Grid Accent 6"/>
      <w:lsdException w:name="Medium Shading 1 Accent 6"/>
      <w:lsdException w:name="Medium Shading 2 Accent 6"/>
      <w:lsdException w:name="Medium List 1 Accent 6"/>
      <w:lsdException w:name="Medium List 2 Accent 6"/>
      <w:lsdException w:name="Medium Grid 1 Accent 6"/>
      <w:lsdException w:name="Medium Grid 2 Accent 6"/>
      <w:lsdException w:name="Medium Grid 3 Accent 6"/>
      <w:lsdException w:name="Dark List Accent 6"/>
      <w:lsdException w:name="Colorful Shading Accent 6"/>
      <w:lsdException w:name="Colorful List Accent 6"/>
      <w:lsdException w:name="Colorful Grid Accent 6"/>
    </w:latentStyles>
    <w:style w:type="paragraph" w:styleId="a1" w:default="on">
      <w:name w:val="Normal"/>
      <w:pPr>
        <w:widowControl w:val="off"/>
        <w:jc w:val="both"/>
      </w:pPr>
      <w:rPr>
        <w:rFonts w:ascii="Calibri" w:h-ansi="Calibri" w:fareast="宋体" w:cs="Times New Roman" w:hint="default"/>
        <w:kern w:val="2"/>
        <w:sz w:val="21"/>
        <w:sz-cs w:val="24"/>
        <w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA"/>
      </w:rPr>
    </w:style>
    <w:style w:type="character" w:styleId="a4" w:default="on">
      <w:name w:val="Default Paragraph Font"/>
    </w:style>
    <w:style w:type="table" w:styleId="a2" w:default="on">
      <w:name w:val="Normal Table"/>
      <w:semiHidden/>
      <w:tblPr>
        <w:tblCellMar>
          <w:top w:w="0" w:type="dxa"/>
          <w:left w:w="108" w:type="dxa"/>
          <w:bottom w:w="0" w:type="dxa"/>
          <w:right w:w="108" w:type="dxa"/>
        </w:tblCellMar>
      </w:tblPr>
    </w:style>
    <w:style w:type="table" w:styleId="a3">
      <w:name w:val="Table Grid"/>
      <w:basedOn w:val="a2"/>
      <w:pPr>
        <w:pStyle w:val="a2"/>
        <w:widowControl w:val="off"/>
        <w:jc w:val="both"/>
      </w:pPr>
      <w:tblPr>
        <w:tblBorders>
          <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
          <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
          <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
          <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
          <w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
          <w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
        </w:tblBorders>
        <w:tblCellMar>
          <w:top w:w="0" w:type="dxa"/>
          <w:left w:w="108" w:type="dxa"/>
          <w:bottom w:w="0" w:type="dxa"/>
          <w:right w:w="108" w:type="dxa"/>
        </w:tblCellMar>
      </w:tblPr>
    </w:style>
  </w:styles>
  <w:bgPict>
    <w:background/>
    <v:background id="_x0000_s1025">
      <v:fill on="f" focussize="0,0"/>
    </v:background>
  </w:bgPict>
  <w:docPr>
    <w:view w:val="print"/>
    <w:zoom w:percent="100"/>
    <w:characterSpacingControl w:val="CompressPunctuation"/>
    <w:documentProtection w:enforcement="off"/>
    <w:punctuationKerning/>
    <w:doNotEmbedSystemFonts/>
    <w:bordersDontSurroundHeader/>
    <w:bordersDontSurroundFooter/>
    <w:defaultTabStop w:val="420"/>
    <w:drawingGridVerticalSpacing w:val="156"/>
    <w:displayHorizontalDrawingGridEvery w:val="0"/>
    <w:displayVerticalDrawingGridEvery w:val="2"/>
    <w:compat>
      <w:adjustLineHeightInTable/>
      <w:ulTrailSpace/>
      <w:doNotExpandShiftReturn/>
      <w:balanceSingleByteDoubleByteWidth/>
      <w:useFELayout/>
      <w:spaceForUL/>
      <w:wrapTextWithPunct/>
      <w:breakWrappedTables/>
      <w:useAsianBreakRules/>
      <w:dontGrowAutofit/>
      <w:useFELayout/>
    </w:compat>
  </w:docPr>
  <w:body>
    <wx:sect>
      <w:p>
        <w:pPr>
          <w:rPr>
            <w:rFonts w:hint="fareast"/>
            <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
          </w:rPr>
        </w:pPr>
        <w:r>
          <w:rPr>
            <w:rFonts w:hint="fareast"/>
            <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
          </w:rPr>
          <w:pict>
            <w:binData w:name="wordml://1.jpg">/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0a
HBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIy
MjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCAE5AfQDASIA
AhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQA
AAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3
ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWm
p6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEA
AwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSEx
BhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElK
U1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3
uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwDlMD0o
wPSiivKP0QTijj0oooAKKKKAFyaM0lFAC0UlLQAUlLSUDCiiigAooooAKKKKACiiigAooooAKKKK
ACiiigAooooAKKKKACiiigAooooAKKKSgBaSiigAooooAKKKKAFopKKAFxRSUtABRRRQAUUUUAFF
FFABRRRQAUUUUAGKKKKACiiigAooooAQ0UGigB1JRRQIKKKKBhRRRQIKKKKBhRRRQIWkpaSgYUUU
UAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFJRRQAUUUlAC0maKKBBRSZo3UB
cWnxRyTSLHEjySMcBUUkn8BXa+GfhzeaqqXWps9naHlUx+9cfT+EfX8q9T0jQ9L0OER6fZxxHGDJ
jLt9W6mt6eHlLV6Hk4rN6NF8sPef4feePaf8O/EuoAMLEW8Z/iuHCfp1/St+D4Pai6gzaraxn0VG
b/CvVw5pwY1usNBbnj1M6xMn7tl8v8zymT4OXwH7rWLZj6NEy/1NY1/8MfEtkpaO3hulH/PCQZ/I
4r3IMakV6bw8HsKGdYqL1afy/wArHy9dWlzYzGG7t5YJR1SRCp/Woa+ndR0mw1i3MF/aRXEZ7OuS
Poex+leWeKvhZPZq95oTPPCMlrZzl1H+ye/06/WueeHlHVansYXOaVV8tT3X+B5rRSspVirAhgcE
EYINJWB7AUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAhooNFAC0UUUCCiiigAooooAKKKKACiiigBa
SiigYUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUlLSUAFFFJQAUZpCaaTQTcU
mkJpM0KGdgqqWYnAAGSTQJsfHG80qRRIzyOQqqoyST2FeueDvAUOkJHqGrIst/1SI8rD/i38qf4H
8HJoVuuo36BtSkXKqf8AlgD2/wB71/Kuw3FjXdRoW96R8vmeaObdKi9Or7/8AsB8mnrUKCp0FdR4
aJFGamWOkiTJqyBgVIyMR0bKlopDIwSKeCCKRl4qLJBpiOF8e+Ao9Yhk1PTIwmooMui8Ccf/ABXv
3rxZlKsVYEMDggjkGvqYSZHNeQfFPwwtpdLrtomIZ223CqOj9m/H+f1rkr0vtI+iyfMHdYeo/T/L
/I83ooorkPpAooooAKKKKACiiigAooooAKKVVLsFUFmPQAZJre0/wX4h1IBodMmRD0eYeWP1ppN7
EVKsKavNpepz5orv4vhPrDRgy3tlG56qCzY/HFFX7GfY5HmWE/nRwNFFFZncFFFFAgooooAKKAMn
A611OseAdY0XSF1OcwNb7FZ9smGQtjgg9Tz2pqLeqM51qcGoydm9jlqKVVZ3VFBLMcADua6HXvBW
seHbZbq8jjNu2B5iSA4Y9iOtCi2rocqsIyUZOzexztFFFI0CiiigAorp/DvgbUvE2mS31lNbqscp
i2ykgkgA+nuK5qRDHK8ZIJUkEg5BxTcWldmcK0JycIvVbjaKKKRoFFFFABRRRQAUUUUAFFFFABRR
RQAUUUhoAKKKQ0AFITS00mgliE00mgmmk0yWxc16X8OfCihU17UI8n/l0jYf+Pn+n5+lcn4N8Onx
FrSpID9jgw87eo7L+P8ALNe3jaqrHGoVFAVVAwAB2rrw9K/vM8DN8c4L2EHq9/TsSFixpyimKKmW
u0+ZJUFTJUS1IppMpFqI1Yqkr4rwb4mfGzUrXWLnRPDMiQJbOYprzaGZnBwQmeAAeM96kZ9DUV8c
6b8X/HGnXYnGuTXIzkxXIDo3tjt+FfSHw3+Idp4+0Z51jFvf25C3NvnIBPRlPdT+lIZ2p6VA9Ss2
BVd25piDNU9X0+LWNIutPnxsnQrk9j2P4HBqzmgmhq+hUZOLUluj5lmhkt55IJV2yRsUYehBwaZX
W+M9EupPHN7BY2sszTlZgsaE/eHP65q7pfwr1q8CveywWKHsx8x/yHH615ns5XaSPuFjqMaUalSS
V0mcLR3x3r2jT/hTodthruW5vHHZm2L+Q5/WuosfDmi6bj7JpdrEw/iEQLfmea1jhpPc4aueUI/A
m/wPn+z0LVtQI+yabdzA/wASxHH59K6Cz+GXiW6wZLaG2X1mlGfyGa90A4wBxS4rVYaPVnn1M9rP
4IpfieS23wfumAN1q8UfqIoS36kitOH4Q6Yv+u1K7kP+yqr/AI16Pto21aoU10OSWa4uX2/wRwif
Crw8v33vH+soH8hU6fDLwwnW1nf/AHrhv6V2e2jbVeyh2MXj8S/+Xj+8x9L8OaRooP8AZ9hFCT1f
G5v++jk1p4qTbRtq0ktjmnOU3zSd2REUVIRRTJPl2ilxSV5J+jBRRRQAUUUUAdD4J0c614rsoCu6
GNxNL6bVOcH6nA/Gu0+L2tACz0WJv+m8wH5KP5n8q1vhZoDadoj6nOmJ73BTPURjp+Z5/KvKvEF7
caz4mvZ3BaWWcoijngHCgfgAK3fuUrdWeLBrFY9y+zTX4/1+RtfDfQjrHiiKaRc21l++k92/hH58
/hXY/EvTPEGu3drZ6dp0s1lCu9nDKA0h+p7D+ZrS0q2t/h54EkurlVN2VEkg7tIeFT8On5muP/4X
Brn/AD46d/3w/wD8VV2jCHLLqc/NiMTiXiKEU1HRX/MwP+EB8U/9Aeb/AL6X/Gj/AIQHxT/0B5v+
+l/xrppfip4mgj3zaRaRoTjc8MgH6tUA+L+ukgCw08k9tj//ABVZ8tLuzsVXMXtCP3/8E5y48EeJ
LW2luJ9KlSKJS7sWXgAZJ61z9fQHiLVZbX4eXFzqixpdXFr5bRx5ADuMYGSemf0NcX4WuvAkfh21
XWEtTfjd5m+JiepxyB6YpypJOyf3k4fMqsqTqThezt7p0Hglv7I+F8l6SAdk04P5gfyFeJ19H382
g6V4bVLwRQ6S6iMIUO0huQMDmsLSrT4f63dm106zs55ghcqImGFBAzz9RWk6V7RvscWEx/snUrOD
ak73tseeeBda0HR5L063bCcSBPKzCJMYznr07V2f/CbeAf8AoFp/4BLXBePrKz07xle2ljCkMEYj
xGgwASik/wA65qsfaSh7vY9J4KlirV22uZJ7+R7F/wAJt4B/6Baf+AS1raFqHhXxHO8enaIjBB88
j2aqi/U+tea+CvBE3iiZriWTydPifbI4PzOeu1f05rvPEXi7SvBOnjR9Fhia7QYEa8rF7se7e351
tCbtzSskeZicNSU/YYdylP10XqVfihBomn+Hkt7e3s4L6SVWRY41Vyozk8DpXj9WL6+udSvJLu8m
aaeQ5Z2PJqvXPUnzyue5gsM8PSUJO7CiiioOoKKKKACiiigApKU0lABSUGg0CGmmk0pppNBLYhNN
AZ3VEBZmOAB3NBNdf8OdF/tHXGv5Vzb2QDDPQyH7o/Dk/lWkIc0kkcuJrqjTdR9D0bwtoqeHtBht
cD7Q/wC8nYd3Pb8On4VtLzzURYk1KleqkoqyPhqlSVSbnLdkq1KvWogaeDQQTg0uahDU9TmkMlz8
vXFfFHiPT7nSvEmpWN4pE8Ny6vnv8x5/Hr+NfawPFebfFbwZ4c1XRLnXdSd7O6tIuLmEAtJ2VCp+
9kkAdDzSaKTPl6vY/wBnWK5/4TDU7hAwtVsCkh7bi6lR9eG/WjQ/gDqF9aWl1qWrR2izRrI8CQlp
I887TkgZr23wr4W0vwfpC6dpURVM7pJHOXlb+8x/yKVgudG0me9RlqjL00tTsK5JupC1Rl6jaTin
YLkiMvnHAGSOT61bTpWdbtumPtWinSkxokFPApop4pAAFLilApaAExRilooATbRilxRQA0rSbafR
QBEVFFSGikM+V6SiivKP0UKKKKACui8GeGpPEuuxwFSLSLElw/ouen1PSudr1LwV4n0vw94CvLho
0F5FMVKZ+aZiMr+HX8jV00nL3tjjx1WpTov2SvJ6feepQS24ZraFlzAFVkX+DjgflXzvY6quieLz
qLWyXAhuHPlv9TyPf0r0/wCFd7PqVnq97dOXmmuw7sfXaK8d1H/kJ3f/AF2f/wBCNbVp3jGSPLyz
DqnVrUZa6JM6bx14z/4Si6gjtRJHYwqGCPwWcjkn6dP/ANdcjXpnwq8OQXi3uqX1vHNCB5EayqGU
nqxwfwH51xXiiWxm8R3p02BIbRZCkap0OOCfxPNZTTa531PQwtSnCo8LSjpHqeqfFX/kSbb/AK+Y
/wD0Fq8k0G5srPXLO51FJHtYZQ7qgyTjkcfXFet/FX/kSbb/AK+Y/wD0FqxfhXFo+o2V9ZXNjA96
oOZHXLPGwwcZ6Y9vUVtUjzVUjz8FWVHL5TabV3t5mV8RvGFr4h+xWumys9pGDI5KlcueACD6D+dc
dpNmdQ1izsxn99MicehPNWPEWjyaDr11p8mcRv8AIx/iQ8qfyrf+GGn/AG3xnDMRlLWNpj9cbR+p
z+FYu86mp6MfZ4bBt0tkro6n4wXgj07TdPQ43yNKQPRRgfzNZnwdti2s6hdbeEtxHn/eYH/2Ws/4
rXUs/i/ynR1jggVE3AgNnJJH4nH4V1fwytl0jwbe6vOMCVmkyf7iD/HdWy96tfsebJeyytR6y/V3
/I898ZNNf+MdUnSJ2UzlAVUkHb8v9Kwvstx/zwl/74Nei+H/AInWmk6LFZ3OmyzzK8jvIrgBizs3
/s1af/C4NO/6A0//AH8X/Cs+WD1cjsjXxVJezjRulpe66F34To8fhS7Doyn7UxwRj+Fa8gure5e7
mYwykmRiSVPPNfQPhbxLB4m0uW+gtXt1jlMZRiCSQAc8fWuSk+LunxyOh0eYlSRnzF/wrWcYuMU2
cGFr4iOIqyjSu3a6vseS/Zbj/nhJ/wB8Goq9bk+L2nPGyjR5xkEf6xf8K8lY5Yn1Nc84xWzue1hq
1apf2sOX53EoooqDqCiiigAooooASiiigBKQmlphoJYhphNONMJpoiTGMe9e5eENK/sXwraQuu2e
cefL65boPwGBXkXhnS/7a8Tafp5GUlmHmf7g5b9Aa95vW/fEDgdgK7cJHVyPnM7raRpL1GKc1OtV
o+RUwNdrPnkTZpc1Fupd1KwEwanhqr7qcHFKwFnzABXB35/4TLx3Fpn39G0JhPd/3Zro/cj99vUj
14Navi/xC2gaI8sCebfzsLeyhHWSZuF49B1P0qTwnoS+HNAhsmfzbpyZruY9ZZm5ds/Xj6Ciwzov
MpPMqHNG6gCbf70heot1NZqAJC9RPJxTGeq00uAapIRpWALZb1rTSs+wXbAvvV9TWctykTCniowa
eDSGSCikFLQAtFFFIYUUVG0o/h5oAkpKhMjnvj6UeY3rTsK5KaKiMp9AaKVhny3RRRXlH6IFFFFA
wooooA9i+Dn/ACBNR/6+R/6CK8suLeW71ya3hUvLLcsiKO5LYFep/Bz/AJAmo/8AXyP/AEEVxvhX
UdJ0rxtPeaszqkcknlME3KrkkZPfpmuiSvCKZ4lGbhicRKKu1bQ9B1uaPwN8Oks4HAuWjEKEcbpG
+838z+VeH12XxG8SR69riR2koksrVNsbL0Zjyx/kPwrjaitJOVlsjqy2hKnS55/FLVntfxV/5Em2
/wCvmP8A9BavK/C2tPoHiK0v1J8tW2yj1Q8N/j+FeqfFXjwTb/8AX1H/AOgtXidVXdql0c+UwjUw
bhLZtnr/AMVdES+0m2121AZoAFkK/wAUbdD+B/nXl2k6ve6JfpeWE7RSr6dGHoR3Feg6L4/0qHwL
/ZmrpLPOqNbiJF++mODk8DAOPwrzNImnnWKBGdnbCIBkk9hSqtOSlE1y+nUhSlQrLROyvs0e06dq
ug/EnSvsOoQrFqCDOwHDKf7yH09qTx/Mvh3wDDpVkjhJdttuA4CAZJJ9Tj9TTfCPhy18EaLNrWsu
qXTR5fPPlL/dHqxP+FSeHfHemeLWn0rVLaOF5SRHHJykq54HP8X+RW9/dtLRs8dx5arnRTlSg726
X8jxKiup8d+H9O8Pa59n0+68xXG9oDy0PoCf5d65auOUXF2Z9RRqxqwVSOzPafhJ/wAind/9fTf+
gLXjdz/x9Tf77fzr2T4Sf8ind/8AX03/AKAteN3P/H1N/vt/OtqnwRPNwP8Avdf1X6kVFFFYHrBR
RRQAUUUUAFFFIaACkNLSUAIaaacaYaCGNNRsaeajaqRlI9E+DlgLjxLeXrDK21sQDj+Jjj+QNekX
a/vz9a5b4K24XRtWusctOsefYLn/ANmrqrpgZ2+tejhlaJ8fms+bEy8hi8CnZqLeBShxXSeaS7qM
1Fvo30AS7qDIFBJOAO9QmSuR8YX9xfy23hXTZCl5qQJuJV/5d7YcO31PQfjQwG6ET4u8WS+JJQW0
vTi1tpanpI/SSb9MA/4V3G+s+wtbbTbC3sbSMR29ugjjQdgKsebSsO5Y30u+q3mUokp2FcsF+KYW
qLfTGkosFx0kmBVZSZrlIx65NMmmCqSTwKm0iMsWnbq3T6U9gOghG1QPSrANVozxUymsWWicGpFN
QA09WpDLANOqENTw1AD6XNNzSO2EYjsDQBGzb2OD8o/WjFNgIaBCO4pJRuZFOdpJBHrxTEMMhclY
QG9WP3R/jTTFMORcE+oZAR+mKsYAAAHFIaAICkh6ygH2X/GipTjNFAWPl6iiivIP0YKKKKBhRRRQ
B7H8Hf8AkCaj/wBfI/8AQRXk2o/8hO7/AOuz/wDoRqBJJEHyOyj2OKbnPNXKd4qPY46OF9nXnVv8
VvwOt8BaTo+taldWmsDC+TvjfzdmCDz/AD/SvQLbwF4MtLlLg3Ak8s7gsl0Cp+teJUU4VFFWauZ4
jB1as3KNVxT6f0z034peKbHUbe30mwnSfy5fOlkjOVBAIC579TXmNLSVM5ubuzowuGjhqSpxCvSP
hxfeGdLsLvUNQZY9Rt+d8pydh4Hlj17HvXm9FEJcruPE4dV6bpttX7HUeMfGd14ou9i7obCI/uoc
9f8Aab3/AJVzAJVgykgjkEdqSilKTk7sulShSgoQVki3aSJPq0El+zSxvOpnLMcsu4bsn6Zr1qXw
X4BlOVv4o/Zb4f1JrxqiqhNR3VzDE4adVpwqONux7jc+IPDHgrw9JZ6VcwSygExwxSeYzOR95iPw
rw9mLMWY5JOSaSiidRzDCYOOG5ndtvdsKKKKg6wooooAKKKKACkpaSgApKWkoENNMNPNMNBLGGo2
qQ1G1UjGR7T8IJAngzUCOv21v/QErcuJP3pNcd8I7wf2RrNnnlZUmA+oI/oK6i4f5jXqYf4EfGZk
rYmQ4y80okqkZKUSY71ucBamuY4IXlldURAWZmOAAOpJrzTUfjXo9reNDZ2VzdxqcGYMEB+gPJH1
xWr8S5p18AaoYCQSIw5HXYZFBr5yqJya0Lirn0np3xP8PajpN1fJO8T20ZeS2lwsh/3ecNkkDj1q
54S065hiuNa1Rf8Aib6mRJMP+eMY+5EPTAxn3+lfM9ndPZXsF1FtLwyLIoYZGQcjIr6n0+/W+062
u1BAniWUD03AH+tEXzbhJWNUy+9J5tUzJRvqyC6JaeHqkHp4egC3vqN5MCoTLiqs9xtH8hQA5ybm
4WFe55+ldDbII0CjoBisjS7covmv99/0raiHFKQ0W4zUwNVlNSq1ZlFgGng1ADUgNJjJg1PBqAGn
g0hkwal3VFml3UAUUuBYTG3l+4x/dH19qvqhYh3YE9QB0FRTwxXEZjlXcp/Ss/7BfWzZs7zKf3JP
8arRi1Rr0YrLEusgYMUJ994/wpDBqlxxLPHEp/u8n+lFguX3nhRtrSKD9aKojRrfA82SWR+7b8fo
KKNA1Pm6iiivHP0cKKKKACiiigQUopKBQAtFFFABSUtJQMKKKKACiiigAooooAKKKKACiiigAooo
oAKKKKACkpaSgApKWkoENNMNPNMNBLGGomqVqjaqRjI6n4daoNO8UrC7YjvYzAfTd1X9Rj8a9MnY
iQg14QsjwyrLGxV0IZWHUEdK9l03VY9b0qDUI8BnGJVH8LjqP6/jXoYSejifM5zQakqq66MsFuaN
1NJpma7Dwxl5bw31pNa3KeZBMhjkT1UjBrwPxP4A1bQLp3hglvNPJzHcRLuwPRwOVP6ele/5pvIO
QSD6g4qZRuNOx846D4U1XX71Ibe1lSLcPMndCEjHck/0619GWcMdpZw20QPlwxrGmfRRgfyoJZvv
MWx6mjdRGNht3Jt1LuqEGnA81RJOGp2+oAaRpMCgCSWbA602yhNzN5jj5B0HrUEUbXUu3+AdTW7b
QhFCgcUAWoVwtXE4qBBjFTCoZSJQakU1EKetSBODUgNQqakBpDJQacDUQNPBpFD80uaZmjNICTNK
DUeaXNAD80ZpuaM0ADHmimt1ooA+Y6KKK8k/RgooooAK3vCvhefxVfT2tvcRwNFH5haQEgjIHb61
g16N8Hv+Rgv/APr1/wDZxV04qUkmcmNqypYeU4bof/wpzUv+gpaf98NWV4i+HN74c0eTUpr63mRG
VSiKQTk471V8XaxqkHi7VYotSvI41uGCok7AAewzXd+LJHl+D9rJI7O7Q2xZmOSTxyTWvLBqVlse
d7bF05UnOaam107mVotj8PJdDtJr+WJbswKZ1Mz5DYw3A96uCL4W7gqhJGPQKZyTWd8Lr3TruK90
C9tYTJcKWVyo3SLjlSevHUfjXJ6lbzeD/F7LazK7WkweJwwOV6gH3xwRRzWinZCVCU686TqTTWq1
0aPQrp/hzo8nlXGm7JMBtkkEpbB6H5qdY+IPAk1wLfTtDM87AkJFYgscc96p/EZNN1zwvYa/BPCl
ztG1GcBnQ9VA7kH+tHwy0eLStKu/E+oYjQxsImb+GMfeb8SMfh71d3z8qSsc3JT+qutOUua9rX6n
P+Ote0PVobe207Sp7G5t5DvElukeQR04Oew61xKDLqD6itXxLrcviHXbjUJOFc4jT+6g6CstP9Yv
1Fc05Xlc97C0vZUVHb53PddY03wZ4e06C71LSbVY5GEYKwbiWIJ7fQ1h/wBv/DP/AKBsP/gGa6fx
h4Zm8VaFaWcFxHA0cqylnBII2kY4+tcR/wAKc1D/AKCtr/37auuamn7sdD53Cyw0qd69WSl6v/I2
tOv/AId6rqMFjaaZA08zbUDWpAJ+tW9dXwJ4cuY7fUdKtkkkTeoS23cZx2rM8PfDC90XxBZ6jJqN
vIlu+4oqEE8EVq+NvAt14r1G3uoL2GBYovLKupOeSe1CU+W/LqKUsN9YUVVlyW1d3v8AcZH9v/DP
/oGw/wDgGa1dCTwJ4juZbfTtKt3kjTewe228Zx3+tc1/wpzUP+gra/8Aftq6fwR4FufCmo3NzPew
zrND5YVFII5Bzz9KUOdv3oqxeIeEjSbpVpOXTV/5HlfjS0t7HxhqVtawpDBHIAkaDAX5R0rBrpPH
3/I86r/11H/oIrm65Z/Ez6LDNuhBvsvyCiiipNgoopKAA0UUUAFJRRQIaaYaeaYaCWNNRmpDTCKp
GUiIitzwr4hOh3zJMSbKfAlA/hPZh9P5ViMKjIrSEnF3Ry1qUasHCWzPbQyuqujBkYAqwOQR60hr
zbw14rk0nFpd7pLInjHJi+nqPavRbe5gu4Vmt5UliblWU5Br06dVVFofIYrCTw8rS26MfiilorU5
RmKTFPxSGgBuKXNIWpjPQMeXxUaq88mxPxPpTUV5n2IPqfSte1tlhUAD6mgCS1t1iQKBWhGuKiQY
qwlICQVKtRLUoqWNDxTxTBTxUgSKakBqIGnA0DJQacDUYNOzSGSZozUeacDSAkFLmmA0uaBjs0Zp
tLmkAjHmikPWigD5moooryT9GCiiigQV6N8Hv+Rgv/8Ar1/9nFec16N8Hv8AkYL/AP69f/ZxWlH4
0cOZ/wC6T/rqc/4v06+l8X6q8dncOjXDEMsTEH9K7/xWrJ8HrRHUqyw2wIIwQeKZrPxUOk6zd6f/
AGSJfs8hj3+fjdjvjFafi67g1PwBa3l0vlW9zJbSSrnO1WZSefoTW6jH3rM8mdWu3Q9pCyTVtb3O
d0vxb4FtLe1/4lUi3aRqrSJbqDuxgnO7PPNdL4hg8H+HLSG71DRYSs77V8uAMc4zzkisO10f4Z3V
1FBbXHmTyMFRBLLkntXSeMf+EWnW1tvEdwIwMyRLudc9iflqo35XexhVdP20VFT1vfv8jlj4s+Hh
AB0RsDp/oq8f+PVfk+I/g+awFjJZ3LWgAUQmBduB0GN1UP7O+Fv/AD9j/v7LSf2d8LP+fsf9/Zam
8+6N3DDS3hUMzxD4h8E3mhXVvpekmG9dQIpPs6rtOR3B9K8+T/WL9RXp15YfDNbKc210DOI2MY82
X72OP1rzBDh1J9awqXvrb5HrYBw5JKCkv8R7b8TGv08L2B09rlZftC7vs5YHGxuuO3SvKvO8Tf8A
PXV/++pK9Y1n4k6TpemwSafJFqExYI0SuVKjB56eoA/GsL/hcrf9AUf+BH/2NbVORy1keZgfrNOj
yxo39Wl+Zg+DZdfbxfpoupNSMBl+cSmTbjB654rf+KUmrJrVmNPe9WP7P832cuBnceuK3tD8Z614
itJLrT/D8TRI+wl7wLzjPdfesJ/jE8UjI2irlSQcXHp/wGj3FCzluK+IqYn2kaSvFWaujgvO8Tf8
9dX/AO+pK7z4WSas+tXo1B71k+z/AC/aC5Gdw6ZpP+Fyt/0BR/4Ef/Y1raD8U7DU7qWPUYo9OjVN
yyPKW3HPTpShyKSfMaYuWKnRlF0EvRpnnPj7/kedV/66j/0EVzdbvjK8t9Q8XajdWkqywSSAo69C
NoFYVYT+Jns4ZNUIJ9l+QUV6x4Y+H+kWWhrq/iQgl0EnlyPtSJT0z6mtJfDHgPxNHJDpjwJMo+9b
SEMPfaeo/CtFQk0cM82oxk0k2lu0tDxSiuw1/wCHGt6PKWt4WvrX+GSEZYfVeo/lXVeAPA8UenXV
54i02P5yPKS4XlFGcsR2z/SpjSk5cptVzGhCl7VO/pueS0lW9Ult5tWu5LSNY7dpmMSKMALnj9Kq
ZrM7E7pMKQ0E00mgLgaaaXNIaCWNNNIqWOOSaVIokLyOQqqoyST2r0nRPhUs0CS6vdSLIwz5NuQN
vsWOcn6VpCnKexyYnFUsOr1GeXEVGRXsGpfCGzeInTdRmhm7LcgMp/EAEfrXmet6BqXh+8+zalbN
Ex5RuquPVSODVSpyjuY0cXRr6QevYyCKt6bq99pEu+0mKg/eQ8q31FVyKYRSTad0XUpxmrSV0d7p
/jy0lULfRPbv3ZfmU/1FdBb61p10uYL2B/beAfyrx8imEV0xxUlvqeTVymlJ3g7Htv2mLGfMT/vo
U2C4iu7oW1tLHNOQW8tGBbA74rxI5r1P4KaX59/quoFeIo0hU+7HJ/8AQRWscS5O1jhr5aqNNzcr
28jq/wCx7kLumKxj0zk1Wa1y21B+JrsNQhCrisR4wpPHNdKdzy2VYIFiG0D6mrsa4FNRKnRaYhyL
U6imqtSAUhjlFSCmrTxUsY4UoptOFIBwpwNMFOFAEimnZqMGng0DHUoptOFIBwp2aYKXNIY6lptL
mgAY80UhNFAz5noooryD9GCiiigAr0b4O/8AIwX/AP16/wDs615zXo3we/5D9/8A9ev/ALOK0o/G
jgzP/dJ+n6nK+M/+Ry1f/r5avSPFP/JHLP8A64W3/stP1j4eaHqWsXd7ca08U00hd4w6fKT25p/j
oWlt8ORptrdxzmHyYkAcFmAIHQVsoOPM2eXPFU6zoQhumr6ehxXwv0tr/wAXR3JXMVmhkY9txBC/
qc/hXo3irwZpniS7GpX+ozwJBF5fyMoRQCSScjrk/pVHw9ZxeAPAk+oXwAupVErr33EYRP8APqa5
/wCHfjBXubjR9YkV4r2RnjeTpvb7yn2P8/rTioxShLqTiJVq9WeJoPSGnqutix/wr3wf/wBDI3/f
+L/CrFr8LvDd6W+ya1cT7fveXJG2PrgVia98L9Rh1yOLSkEtjcP8rsf9R7N7D1rp9Su7D4a+ERYW
bq+pTqdp/iZz1cj0Hb8KFFa80bJDnWqNRVCs5Sl0svxPKPEGnQ6Tr97p8EjSRQSlFZsZP1xW14I8
PaNr8l6ur372ghCGMrKibs5z94HPQVyskjyyNI7FnYlmY9STTa5k0pXse9OnOVLkUrPTU9c/4V14
M/6D83/gXD/8TSf8K68Gf9B+b/wLh/8Aia8krd8MeFr7xNqCw26Mtup/fTkfKg/qfatFOLdlE4Km
Gq04uc67SXkj3Dw7oum+HtDeCxu3ktGLSmeSRT2wTkADAxXHt8PPBruzHX5sk5P+lw//ABNWviBq
tp4c8Ix6BZkCWeMRKgPKxDqT9en4mvGK0qzjFqNjiy/C1qsZVlUceZ/f5nrn/CuvBn/Qfm/8C4f/
AImlT4b+DpJFSPXLh3Y4VVuoiSf++a8iroPA1v8AavGulR4OBNvP/AQW/pWcZxbtynZVwtenCU/b
vRX2Njx94N07wrBZNZT3UjTswYTspwAB0wo9a4mNgkqMy7grAketel/GObOoaXAG+7E7kfUgf0Ne
ZGpqpKbSOjLpzqYaMqju3f8AM7PxZ4/k8S6TFp6WX2WNJA7Yk3bsDgdK4+GeW2mSaCR45UOVdDgg
+xqOiolJyd2dFGhTow5IKyPQNJ+LWr2UKw31vDegf8tCdj/iRwfyqp4j+Jeqa7ZvZQxR2VtJw4Ri
XcehPp9K4mkJqvazta5gsBhoz9ooK/8AXTYCaaTQTTSag6mxc0maQmmk0yWx2aTNd/4J8Bpq1umq
aqG+yvzDADgyD+8T6fzr0mPw3o0duYU0u0EeMbfJWuiGHlJXPJxOb0qM+RK7W55h8M9NjuNSutQl
UE2qqsYP95s8/kP1r1yCYAiuVj0q08PXlw1jF5MVwQXjB+UEZ5Hp1rUjvFxnNd1KlyQsfOY7FfWK
7qLbS39ep04USpWbqul2mp2T2WoW63Fs/wDC3VT6g9jUdpdSFgSSF9K1gUdME9aGujOeMmndaM8B
8YeA7rw6zXdqzXWmE8SY+aL2cf16fSuYstLv9ScrY2VxcsOohjLY/KvpC/8AL3vb4Vwy4cEZGD2N
OsLSK2tljt41iiQcIg2gfgKweGV7p6Hr084nGHLKN33Pm3UNG1LS8fb7C5tg3AMsRUH8TWeRX1PP
5F3byWd9Ck1vKNro4yCK8E8d+EW8LawFh3Pp9zl7aQ9vVCfUfyxWVSk4anbhMfHEPkkrM5Aivffg
rZCDwbNc45ubpzn1CgL/AENeCEV9K/DKAW3w50kYwWR5D+LsaKPxGeaO1G3dmjqjDeRWI45rU1B9
0jVmHrXoR2PnHuIoqZaiFSrVCJVp60xaeKQEgpwpgp4qRjqUU0UtIB4NKKYKcKAHinCmCng0DHil
FNBpQaQD6KSloGLS5ptKKAA9aKQ0UAfNNFFFeOfowUUUUDCul8GeKU8KajcXT2rXAli8vaH245Bz
09q5qinFuLujOrSjVg4T2Zf1vURq2tXmoLGYxcSmQITnbntmu2+F/h/Tb6ebVb2eJ3s2BS3Y/c77
29vT6V51UsNxNbiQQyvGJFKPsYjcp7H1FOMrS5nqY16DnR9lTly9Pkel+JJtT+IupvY6F5b6dYt8
zvIFDuc/NjqR1A4qfSvhJDb4uNb1EFF5aOH5V/Fz/hXnWi69qHh+6e506bypXQoxKhgR9DS6l4h1
fWCft+oTzL/cL4X/AL5HFae0g/ekrs5Hg8RBKlRmowXlr5nsV78R/Dujzw2MUst0iYRpITvVAP8A
aJ+b8M1BqHhzwr46la/tdRP2pwAzxS5PtlG6fpXiVKjvG4ZHZWHQqcGn7dvSS0JWURp2lRm4y79z
0TVfhPNp1tNdLrNubeJC7tLGUwB9Ca5rw54N1LxOJXspLdYomCu0kmCP+Ajms+bXtXuLJrKfUrqW
2YgmOSUsDjp1pdF17UdAunudOuDFI6lG4BBHuDxUNwbWmh0xp4uNKSc05dNND1HS/hTpOnqLjWb0
3O3llB8qMfU5yfzFP1r4iaJ4fszYaBDFPKg2r5QxCnvkfe/D868q1LXtV1hs39/POOys52j6L0rO
qnWSVoKxzxy2dWXNip83lsv6+4tajqN3qt9JeXszSzyHLMf5D0FdN4K8GW/i23vS1+9tNbsuAIww
IYHB6j0NcfVqz1K+04ubK9uLYvjf5MrJux0zg81nFq95anfWpzdLkovlfQ9Jb4NPn5dbUj3tv/sq
6Lwv4EsPCM0mpXN8J51QgSuojSNe/c/nXkSeLPEMa7V1q/x7zsf5mq17reqaiu291G6uF/uySsR+
WcVqqlOOqjqedPBY2quSpVXK/I1vHWvR+IPE01zbnNtEohib+8B3/Ek1zNBorGTbd2epSpxpQUI7
IKSig0ixCaaTQTTSaCWwJppNBNMJp2M2xSaEUyyLGOrMFH41GTSLIY5FkXqpDD8KpIzcj6WiSKzj
S1hAWOBRGo9gMVYjnA4NYltqKXsSXMbZSVQ4+h5qwHL8A4969fl0Pgm3d33ItYi85CV7c1jWb5lw
/RT0966QxB4cVzN7E1pd7h9wnmrRLN6KcAVciuj/AHulc9bz7gOauwynzOeB6UNAmWFlLOzseWOT
WpaSgrtrDDbSR6VdglIxzSaBMuTjkmsfxFpEfiXw7caZJtE337Zz/DIOn59PxrZOJE9/SoGjKnNQ
0mrM0hOUJKUd0fM00TwytFKhSRCVZWGCCOoNfTvglfL8A6OB/wA+iH8xmvJfih4d8i6XXLZP3c5C
XIA+6/Zvx/n9a9a8Ht/xQOjn/pzj/lXJTg4zaZ7GPrxrYeE49yG9P7xqzz1q7dtmQ1SPWu5bHhii
pFqMVItMCZaetRrTxSAkBp1R5p4NIY6lpopRSAdThTBTgaQDxTxUYpwNAx9OFMBpwNADwaWm5ozS
AdS00U6gBDRQetFAz5qooorxz9GCiiigYUUUUCCiiigYUUUUAFFL2pKACiiigAooooAKKKKACiii
gBKKKDQAlNJpxphNBLENNJpSaYTTIbGk0wmlJpmCzBR1JwKpGMmdP4V8GXXiQm4eT7PYI20y4yXP
cKP69BXfn4b+H4rTZ9nmdsf6xpju/Tj9K6PT7KPS9MtLCFQFt4Vj+px8x/E5NaCYeEqetejToxit
UfJYnMK1Wb5JWXSxxmk2n9kwrp/nu0UJIRm+9t6gGt+GZSBjpVDWLYxuJlHTrVaC6wOtdKStZHnS
k5Nye5vmcBeDVG8jW6jK45xVZJ3nfYnQdWPQVfjUKmB+frRYkxbbMEuxuo6Vqx44IqOaxkun/cL8
w79hV9LZbWLGdz9zRcLFaZSrBh36ilhlw1Ryh5STnCjv6mmo2Dg9aYG3bSDPPQ1akjDDIrHt5SCB
WrDcDbg1m0UjO1Owhv7Ka1uEDxSoUdT6VqaV9mttEgsLZSiW8Sxqp64AxVef52wo5NKieWBg/MOp
pNJ6lczty9CldffNVa1rq1+0IXiGJB1X+9/9esg8Eg8GrTIY8U8VGKeDTAmWnVGpp4NIB4NOBqMG
nA80hkg5paYDTgc0AOFOFMp2aQDgacDTKcDSAkBpwqMGnjpQMfRmm5pc0AOBp1MzSg0gAmig0UAf
NlFFFeOfo4UUUUAFFFFABRRRQMKKKKAFpKWkoAKKKKACiiigAooooAKQ0tJQAUlLSUCENMNONMNB
LENRsac1RsapGUmNY1HvKMGHUHNOY1ExqkYSZ9E2eope20F0pys8aSD/AIEAf61dRizDJwp7VwXg
DURdeH4UdsvbEw49uo/Q4/Cu2WXjNetF80Uz4mtD2dWUOzJ9QtlkgJHpXHzQmK4MZJVc9faur8x7
g7ASF7n1rL1WzJGVHIq0ZMZbOiRBUGAK17GyefEk2VjPQdzVPRdMbYLi6Uqv8CHv7mt1ph2pN9gS
JGVIotkahR6CqE4zx3NWw+4VXmGJEHrzUobKxiHCjoKhksmlPyMFPvWhsFLjincLFGLT7xTyqEeo
cVowWj/xsq/Q5pUGasx/LSbYJD1iVBhR+PeonTByKmzTGqRkSttPWob6yFwvnRD96PvL/e/+vUrU
glKj6UwMUU4GtG+tBKv2mAc9XUd/cVmg1adyWiVTTwahBp4amBLmlqMGnA0gHg08GowaUGkMlFOF
Rg07NIB9OzTKUGgB4NPBqMGnA0gJAaM00UtAx2aXNNooAcTRSGikB834opaK8c/RxKKWjFACUUUU
AFFFFAwpaSloEFJS0lAwooooAKKKKACiiigApKWkoAKSlpKBDTTDTjTTQiGRtUbVI1RmrRjIiao2
NSNUTVRhJnT+BtUNlqz2zNhLheP94c/yzXrFpOblMj/Vjv618/xyvBMk0Zw6MGU+4r23QdSS+0y2
ni4SRASPQ9x+dd+FnePKfN5rR5Zqqup0tu/IAq0qB33uAQOme9VLNNzEnoKtu3YV0M8pD3lLVFkm
kzTgM0hksZ4plwf3sTHpginr0pJozLCVH3hyppdQEzS96rwy7xgjDDgip80wJUPNWFNVA2KmV6kZ
Y7U0mmh6UmgCJ6ryttQmrL4qptMsnT5BQhFm3lKIoJ7VXvbLdmaAe7KP5ipSMU9ZCKBmODTgau3N
qsuXjwr9x2NUDlG2sCCOoq07kkganA1EDTgaYEwNOBqINTgaQEgNPBwaiBpwNICUGnVGDTgaBj80
4GmCnCkA8GlzTAaXNICQUopgNOBoADRQaKBnzlSUtJXjH6QLRSUUALSUUUAFFFFAC0UUUCCkpaSg
YUUUUAFFFFABRRRQAUlLSUAFJS0lAhhpppxppoRDI2qI1K1RtVIxkRGomqVqiarOeRGa7TwBrQgu
G0yVsK5Lw59e4/r+dcW1EcrwSpLExSRCGVh1BFaU58krnHiaKrU3Bn0zZkLZoe7DNKXyawvCusHW
PDlnduoV2Qq4HTcCQf5VsZ5r0lZq58rKLi3F9CUHJqZBxVdKnU0MkkFPU0wGnVLGVrm3y/mxnD/z
qNLnB2yAqfer1MaJHGGANNPuBEJFPQ09W96iazUfdYj6Ugt3H8Zp6AWfNx3oNwB35qEQ+pP51IkY
XoKWgC4eY8/KtTYCgKvQUgz0paQCEUwjFS5phoAZmo5YklGGHPYjrUhpOlMDOlgeLnqvqKYGrTzV
aW1VsmP5W9OxqkwsQA08GocFWwwII7GnBqYicGng1CDTwaQEgNPBqMGlBoAlB4pwNRg04GkMkzS0
wGlBpAPpwNMpaQDyaKYTzRQB87UlFFeMfpIUUUUAFFFFABS0lLQAUUUUCCkpaSgYUUUUAFFFFABR
RRQAUlLSGgApKWkoENNMNPNNNBLI2qJqmaomFUjGSImqJqmaomq0YSRE1RmpTUZqkc8j1j4W3fma
Bc2xPzQXGQPZgP6g13oNePfDLUPs3iGazc4S6hO3/fXkfpur11Wr0KDvBHzOPhyV356lhTUqtjvV
USYqRX5rU4y0GzTwarK1TK2amwyUUtMBp2aQC0YpRTsZoGMxTsUuBS0AA60uM0UUAJtppHrUnWgj
NAEJHFNIqUrTCKAI8U09akIphFMRHJGsq4PXsapupjbB/Orp4pjqHBBHFNCKqmpFNQupifHbsacr
VQE4NPBqFTUgNICTNOBqMGnA0gJBTgajBp9IY6lJxTCcdOTSqO55NACnJ6nH0ooNFID54ooorxj9
ICiiigYUUUtACUtJQKAFooooEFJS0lAwooooAKKKKACiiigAoNFFACUhpaDQA00w1IaYaCWMNRMK
lNMYVSMpIhYVE1TtUTCqRhJEJqM1KwqMiqRzyRY0u+bTNWtL1c5glV/qAeR+Wa99SdXCuhyrAMpH
cHpXzua9d8G6ob7wzbbmzLb5hf8ADp+mK68LLVxPFzWl7qqLpodgJMinh+aoJLmp1krsPEL6PUyt
Wer81YSSlYZdVuKkBqqr1KHpWGTA08GoQ1OBqbATZoqMGn5oGO70Cm5pc0AOzS5puaM80AOzUZGK
dmmk0AMNNIp5ppoQiNhUZFTGmMtUBXkUMuDVTJRtpq8RVaaPI4600IFbNSqapo/rwe9WFamBODTh
UYNPFICQGjdzgdaZnPA/E04AAYFICRQAP604UwGnZpDFP1oppooA+e6KKK8U/SAooooGFFFFABS0
UUCCiiigApKWkoGFFFFABRRRQAUUUUAFFFFACUUGigBKaadTT0oExhFMYVIaYaaM5ETComFTNUTV
SMJETCoyKkNRtVHPIjNdZ4B1H7Pqk1i7YS5TK/76/wD1s1yhrS8N/wDIy6f/ANdhWtJ2mmceKgp0
pJ9j2BJMGrCS1R/iqdOteofKF9JKsI9UY6sJ1oAupJU6yZqknap160hlpWqQPVdOoqQdakCcMKcD
zUI6ipB3pDHg0oNMpw60gHZo3U09aP8AGgY7NJmm96B1oELSUppKAEpCKd3pp6CmBE4xUDjirD1C
1UhFCVSrbh+NPjeln6GoYu1MRcVuKep3dPu+vrUDf6s/Sp06CgZIOnAp1NFO70gHA04GmjoaUUhi
k0UUUgP/2Q==
</w:binData>
            <v:shape id="图片 2" o:spid="_x0000_s1026" o:spt="75" alt="efaa6b56ef1c5a49e30d794169f44d55" type="#_x0000_t75" style="height:234.75pt;width:414pt;" filled="f" o:preferrelative="t" stroked="f" coordsize="21600,21600">
              <v:path/>
              <v:fill on="f" focussize="0,0"/>
              <v:stroke on="f"/>
              <v:imagedata src="wordml://1.jpg" o:title="efaa6b56ef1c5a49e30d794169f44d55"/>
              <o:lock v:ext="edit" aspectratio="t"/>
              <w10:wrap type="none"/>
              <w10:anchorlock/>
            </v:shape>
          </w:pict>
        </w:r>
      </w:p>
      <w:p>
        <w:pPr>
          <w:jc w:val="center"/>
          <w:rPr>
            <w:rFonts w:hint="default"/>
            <w:b/>
            <w:b-cs/>
            <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
          </w:rPr>
        </w:pPr>
        <w:r>
          <w:rPr>
            <w:rFonts w:hint="fareast"/>
            <w:b/>
            <w:b-cs/>
            <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
          </w:rPr>
          <w:t>内容</w:t>
        </w:r>
      </w:p>
      <w:p>
        <w:pPr>
          <w:rPr>
            <w:rFonts w:hint="fareast"/>
            <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
          </w:rPr>
        </w:pPr>
        <w:r>
          <w:rPr>
            <w:rFonts w:hint="fareast"/>
            <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
          </w:rPr>
          <w:t>姓名：${name}，性别：${sex}，出生日期：${birthday}！</w:t>
        </w:r>
      </w:p>
      <w:p>
        <w:pPr>
          <w:rPr>
            <w:rFonts w:hint="fareast"/>
            <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
          </w:rPr>
        </w:pPr>
      </w:p>
      <w:p>
        <w:pPr>
          <w:rPr>
            <w:rFonts w:hint="fareast"/>
            <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
          </w:rPr>
        </w:pPr>
        <w:r>
          <w:rPr>
            <w:rFonts w:hint="fareast"/>
            <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
          </w:rPr>
          <w:t>表格和图片处理：</w:t>
        </w:r>
      </w:p>
      <w:tbl>
        <w:tblPr>
          <w:tblStyle w:val="a3"/>
          <w:tblW w:w="0" w:type="auto"/>
          <w:tblInd w:w="0" w:type="dxa"/>
          <w:tblBorders>
            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            <w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            <w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
          </w:tblBorders>
          <w:tblCellMar>
            <w:top w:w="0" w:type="dxa"/>
            <w:left w:w="108" w:type="dxa"/>
            <w:bottom w:w="0" w:type="dxa"/>
            <w:right w:w="108" w:type="dxa"/>
          </w:tblCellMar>
        </w:tblPr>
        <w:tblGrid>
          <w:gridCol w:w="2840"/>
          <w:gridCol w:w="2841"/>
          <w:gridCol w:w="2841"/>
        </w:tblGrid>
        <w:tr>
          <w:tblPrEx>
            <w:tblBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tblBorders>
            <w:tblCellMar>
              <w:top w:w="0" w:type="dxa"/>
              <w:left w:w="108" w:type="dxa"/>
              <w:bottom w:w="0" w:type="dxa"/>
              <w:right w:w="108" w:type="dxa"/>
            </w:tblCellMar>
          </w:tblPrEx>
          <w:trPr>
            <w:trHeight w:val="502" w:h-rule="atLeast"/>
          </w:trPr>
          <w:tc>
            <w:tcPr>
              <w:tcW w:w="2840" w:type="dxa"/>
              <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
              <w:noWrap w:val="0"/>
              <w:vAlign w:val="top"/>
            </w:tcPr>
            <w:p>
              <w:pPr>
                <w:widowControl w:val="off"/>
                <w:jc w:val="both"/>
                <w:rPr>
                  <w:rFonts w:hint="default"/>
                  <w:vertAlign w:val="baseline"/>
                  <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
                </w:rPr>
              </w:pPr>
              <w:r>
                <w:rPr>
                  <w:rFonts w:hint="fareast"/>
                  <w:vertAlign w:val="baseline"/>
                  <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
                </w:rPr>
                <w:t>姓名</w:t>
              </w:r>
            </w:p>
          </w:tc>
          <w:tc>
            <w:tcPr>
              <w:tcW w:w="2841" w:type="dxa"/>
              <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
              <w:noWrap w:val="0"/>
              <w:vAlign w:val="top"/>
            </w:tcPr>
            <w:p>
              <w:pPr>
                <w:widowControl w:val="off"/>
                <w:jc w:val="both"/>
                <w:rPr>
                  <w:rFonts w:hint="fareast"/>
                  <w:vertAlign w:val="baseline"/>
                  <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
                </w:rPr>
              </w:pPr>
              <w:r>
                <w:rPr>
                  <w:rFonts w:hint="fareast"/>
                  <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
                </w:rPr>
                <w:t>${userObj.name}</w:t>
              </w:r>
            </w:p>
          </w:tc>
          <w:tc>
            <w:tcPr>
              <w:tcW w:w="2841" w:type="dxa"/>
              <w:vmerge w:val="restart"/>
              <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
              <w:noWrap w:val="0"/>
              <w:vAlign w:val="top"/>
            </w:tcPr>
            <w:p>
              <w:pPr>
                <w:widowControl w:val="off"/>
                <w:jc w:val="both"/>
                <w:rPr>
                  <w:rFonts w:hint="fareast"/>
                  <w:vertAlign w:val="baseline"/>
                  <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
                </w:rPr>
              </w:pPr>
              <w:r>
                <w:rPr>
                  <w:rFonts w:hint="fareast"/>
                  <w:vertAlign w:val="baseline"/>
                  <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
                </w:rPr>
                <w:pict>
                  <w:binData w:name="wordml://2.jpg">${userObj.photo}</w:binData>
                  <v:shape id="_x0000_s1026" o:spt="75" alt="7c1ed21b0ef41bd52c2597655bda81cb39db3d28" type="#_x0000_t75" style="height:77.75pt;width:115.8pt;" filled="f" o:preferrelative="t" stroked="f" coordsize="21600,21600">
                    <v:path/>
                    <v:fill on="f" focussize="0,0"/>
                    <v:stroke on="f"/>
                    <v:imagedata src="wordml://2.jpg" o:title=""/>
                    <o:lock v:ext="edit" aspectratio="t"/>
                    <w10:wrap type="none"/>
                    <w10:anchorlock/>
                  </v:shape>
                </w:pict>
              </w:r>
            </w:p>
          </w:tc>
        </w:tr>
        <w:tr>
          <w:tblPrEx>
            <w:tblBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tblBorders>
            <w:tblCellMar>
              <w:top w:w="0" w:type="dxa"/>
              <w:left w:w="108" w:type="dxa"/>
              <w:bottom w:w="0" w:type="dxa"/>
              <w:right w:w="108" w:type="dxa"/>
            </w:tblCellMar>
          </w:tblPrEx>
          <w:trPr>
            <w:trHeight w:val="477" w:h-rule="atLeast"/>
          </w:trPr>
          <w:tc>
            <w:tcPr>
              <w:tcW w:w="2840" w:type="dxa"/>
              <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
              <w:noWrap w:val="0"/>
              <w:vAlign w:val="top"/>
            </w:tcPr>
            <w:p>
              <w:pPr>
                <w:widowControl w:val="off"/>
                <w:jc w:val="both"/>
                <w:rPr>
                  <w:rFonts w:hint="default"/>
                  <w:vertAlign w:val="baseline"/>
                  <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
                </w:rPr>
              </w:pPr>
              <w:r>
                <w:rPr>
                  <w:rFonts w:hint="fareast"/>
                  <w:vertAlign w:val="baseline"/>
                  <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
                </w:rPr>
                <w:t>性别</w:t>
              </w:r>
            </w:p>
          </w:tc>
          <w:tc>
            <w:tcPr>
              <w:tcW w:w="2841" w:type="dxa"/>
              <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
              <w:noWrap w:val="0"/>
              <w:vAlign w:val="top"/>
            </w:tcPr>
            <w:p>
              <w:pPr>
                <w:widowControl w:val="off"/>
                <w:jc w:val="both"/>
                <w:rPr>
                  <w:rFonts w:hint="fareast"/>
                  <w:vertAlign w:val="baseline"/>
                  <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
                </w:rPr>
              </w:pPr>
              <w:r>
                <w:rPr>
                  <w:rFonts w:hint="fareast"/>
                  <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
                </w:rPr>
                <w:t>${userObj.sex}</w:t>
              </w:r>
            </w:p>
          </w:tc>
          <w:tc>
            <w:tcPr>
              <w:tcW w:w="2841" w:type="dxa"/>
              <w:vmerge w:val="continue"/>
              <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
              <w:noWrap w:val="0"/>
              <w:vAlign w:val="top"/>
            </w:tcPr>
            <w:p>
              <w:pPr>
                <w:widowControl w:val="off"/>
                <w:jc w:val="both"/>
                <w:rPr>
                  <w:rFonts w:hint="fareast"/>
                  <w:vertAlign w:val="baseline"/>
                  <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
                </w:rPr>
              </w:pPr>
            </w:p>
          </w:tc>
        </w:tr>
        <w:tr>
          <w:tblPrEx>
            <w:tblBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tblBorders>
            <w:tblCellMar>
              <w:top w:w="0" w:type="dxa"/>
              <w:left w:w="108" w:type="dxa"/>
              <w:bottom w:w="0" w:type="dxa"/>
              <w:right w:w="108" w:type="dxa"/>
            </w:tblCellMar>
          </w:tblPrEx>
          <w:trPr/>
          <w:tc>
            <w:tcPr>
              <w:tcW w:w="2840" w:type="dxa"/>
              <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
              <w:noWrap w:val="0"/>
              <w:vAlign w:val="top"/>
            </w:tcPr>
            <w:p>
              <w:pPr>
                <w:widowControl w:val="off"/>
                <w:jc w:val="both"/>
                <w:rPr>
                  <w:rFonts w:hint="default"/>
                  <w:vertAlign w:val="baseline"/>
                  <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
                </w:rPr>
              </w:pPr>
              <w:r>
                <w:rPr>
                  <w:rFonts w:hint="fareast"/>
                  <w:vertAlign w:val="baseline"/>
                  <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
                </w:rPr>
                <w:t>出生日期</w:t>
              </w:r>
            </w:p>
          </w:tc>
          <w:tc>
            <w:tcPr>
              <w:tcW w:w="2841" w:type="dxa"/>
              <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
              <w:noWrap w:val="0"/>
              <w:vAlign w:val="top"/>
            </w:tcPr>
            <w:p>
              <w:pPr>
                <w:widowControl w:val="off"/>
                <w:jc w:val="both"/>
                <w:rPr>
                  <w:rFonts w:hint="fareast"/>
                  <w:vertAlign w:val="baseline"/>
                  <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
                </w:rPr>
              </w:pPr>
              <w:r>
                <w:rPr>
                  <w:rFonts w:hint="fareast"/>
                  <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
                </w:rPr>
                <w:t>${userObj.birthday}</w:t>
              </w:r>
            </w:p>
          </w:tc>
          <w:tc>
            <w:tcPr>
              <w:tcW w:w="2841" w:type="dxa"/>
              <w:vmerge w:val="continue"/>
              <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
              <w:noWrap w:val="0"/>
              <w:vAlign w:val="top"/>
            </w:tcPr>
            <w:p>
              <w:pPr>
                <w:widowControl w:val="off"/>
                <w:jc w:val="both"/>
                <w:rPr>
                  <w:rFonts w:hint="fareast"/>
                  <w:vertAlign w:val="baseline"/>
                  <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
                </w:rPr>
              </w:pPr>
            </w:p>
          </w:tc>
        </w:tr>
      </w:tbl>
      <w:p>
        <w:pPr>
          <w:rPr>
            <w:rFonts w:hint="fareast"/>
            <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
          </w:rPr>
        </w:pPr>
      </w:p>
      <w:p>
        <w:pPr>
          <w:rPr>
            <w:rFonts w:hint="fareast"/>
            <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
          </w:rPr>
        </w:pPr>
      </w:p>
      <w:p>
        <w:pPr>
          <w:rPr>
            <w:rFonts w:hint="fareast"/>
            <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
          </w:rPr>
        </w:pPr>
        <w:r>
          <w:rPr>
            <w:rFonts w:hint="fareast"/>
            <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
          </w:rPr>
          <w:t>表单处理：</w:t>
        </w:r>
      </w:p>
      <w:tbl>
        <w:tblPr>
          <w:tblStyle w:val="a3"/>
          <w:tblW w:w="0" w:type="auto"/>
          <w:tblInd w:w="0" w:type="dxa"/>
          <w:tblBorders>
            <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            <w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            <w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
          </w:tblBorders>
          <w:tblCellMar>
            <w:top w:w="0" w:type="dxa"/>
            <w:left w:w="108" w:type="dxa"/>
            <w:bottom w:w="0" w:type="dxa"/>
            <w:right w:w="108" w:type="dxa"/>
          </w:tblCellMar>
        </w:tblPr>
        <w:tblGrid>
          <w:gridCol w:w="2840"/>
          <w:gridCol w:w="2841"/>
          <w:gridCol w:w="2841"/>
        </w:tblGrid>
        <w:tr>
          <w:tblPrEx>
            <w:tblBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tblBorders>
            <w:tblCellMar>
              <w:top w:w="0" w:type="dxa"/>
              <w:left w:w="108" w:type="dxa"/>
              <w:bottom w:w="0" w:type="dxa"/>
              <w:right w:w="108" w:type="dxa"/>
            </w:tblCellMar>
          </w:tblPrEx>
          <w:trPr/>
          <w:tc>
            <w:tcPr>
              <w:tcW w:w="2840" w:type="dxa"/>
              <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
              <w:noWrap w:val="0"/>
              <w:vAlign w:val="top"/>
            </w:tcPr>
            <w:p>
              <w:pPr>
                <w:widowControl w:val="off"/>
                <w:jc w:val="both"/>
                <w:rPr>
                  <w:rFonts w:hint="default"/>
                  <w:vertAlign w:val="baseline"/>
                  <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
                </w:rPr>
              </w:pPr>
              <w:r>
                <w:rPr>
                  <w:rFonts w:hint="fareast"/>
                  <w:vertAlign w:val="baseline"/>
                  <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
                </w:rPr>
                <w:t>姓名</w:t>
              </w:r>
            </w:p>
          </w:tc>
          <w:tc>
            <w:tcPr>
              <w:tcW w:w="2841" w:type="dxa"/>
              <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
              <w:noWrap w:val="0"/>
              <w:vAlign w:val="top"/>
            </w:tcPr>
            <w:p>
              <w:pPr>
                <w:widowControl w:val="off"/>
                <w:jc w:val="both"/>
                <w:rPr>
                  <w:rFonts w:hint="default"/>
                  <w:vertAlign w:val="baseline"/>
                  <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
                </w:rPr>
              </w:pPr>
              <w:r>
                <w:rPr>
                  <w:rFonts w:hint="fareast"/>
                  <w:vertAlign w:val="baseline"/>
                  <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
                </w:rPr>
                <w:t>性别</w:t>
              </w:r>
            </w:p>
          </w:tc>
          <w:tc>
            <w:tcPr>
              <w:tcW w:w="2841" w:type="dxa"/>
              <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
              <w:noWrap w:val="0"/>
              <w:vAlign w:val="top"/>
            </w:tcPr>
            <w:p>
              <w:pPr>
                <w:widowControl w:val="off"/>
                <w:jc w:val="both"/>
                <w:rPr>
                  <w:rFonts w:hint="default"/>
                  <w:vertAlign w:val="baseline"/>
                  <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
                </w:rPr>
              </w:pPr>
              <w:r>
                <w:rPr>
                  <w:rFonts w:hint="fareast"/>
                  <w:vertAlign w:val="baseline"/>
                  <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
                </w:rPr>
                <w:t>出生日期</w:t>
              </w:r>
            </w:p>
          </w:tc>
        </w:tr>
		<#list userList as people>
        <w:tr>
          <w:tblPrEx>
            <w:tblBorders>
              <w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
              <w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
            </w:tblBorders>
            <w:tblCellMar>
              <w:top w:w="0" w:type="dxa"/>
              <w:left w:w="108" w:type="dxa"/>
              <w:bottom w:w="0" w:type="dxa"/>
              <w:right w:w="108" w:type="dxa"/>
            </w:tblCellMar>
          </w:tblPrEx>
          <w:trPr/>
          <w:tc>
            <w:tcPr>
              <w:tcW w:w="2840" w:type="dxa"/>
              <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
              <w:noWrap w:val="0"/>
              <w:vAlign w:val="top"/>
            </w:tcPr>
            <w:p>
              <w:pPr>
                <w:widowControl w:val="off"/>
                <w:jc w:val="both"/>
                <w:rPr>
                  <w:rFonts w:hint="default"/>
                  <w:vertAlign w:val="baseline"/>
                  <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
                </w:rPr>
              </w:pPr>
              <w:r>
                <w:rPr>
                  <w:rFonts w:hint="fareast"/>
                  <w:vertAlign w:val="baseline"/>
                  <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
                </w:rPr>
                <w:t>${people.name}</w:t>
              </w:r>
            </w:p>
          </w:tc>
          <w:tc>
            <w:tcPr>
              <w:tcW w:w="2841" w:type="dxa"/>
              <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
              <w:noWrap w:val="0"/>
              <w:vAlign w:val="top"/>
            </w:tcPr>
            <w:p>
              <w:pPr>
                <w:widowControl w:val="off"/>
                <w:jc w:val="both"/>
                <w:rPr>
                  <w:rFonts w:hint="default"/>
                  <w:vertAlign w:val="baseline"/>
                  <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
                </w:rPr>
              </w:pPr>
              <w:r>
                <w:rPr>
                  <w:rFonts w:hint="fareast"/>
                  <w:vertAlign w:val="baseline"/>
                  <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
                </w:rPr>
                <w:t>${people.sex}</w:t>
              </w:r>
            </w:p>
          </w:tc>
          <w:tc>
            <w:tcPr>
              <w:tcW w:w="2841" w:type="dxa"/>
              <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
              <w:noWrap w:val="0"/>
              <w:vAlign w:val="top"/>
            </w:tcPr>
            <w:p>
              <w:pPr>
                <w:widowControl w:val="off"/>
                <w:jc w:val="both"/>
                <w:rPr>
                  <w:rFonts w:hint="default"/>
                  <w:vertAlign w:val="baseline"/>
                  <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
                </w:rPr>
              </w:pPr>
              <w:r>
                <w:rPr>
                  <w:rFonts w:hint="fareast"/>
                  <w:vertAlign w:val="baseline"/>
                  <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
                </w:rPr>
                <w:t>${people.birthday}</w:t>
              </w:r>
            </w:p>
          </w:tc>
        </w:tr>
		</#list>
      </w:tbl>
      <w:p>
        <w:pPr>
          <w:rPr>
            <w:rFonts w:hint="default"/>
            <w:lang w:val="EN-US" w:fareast="ZH-CN"/>
          </w:rPr>
        </w:pPr>
      </w:p>
      <w:sectPr>
        <w:pgSz w:w="11906" w:h="16838"/>
        <w:pgMar w:top="1440" w:right="1800" w:bottom="1440" w:left="1800" w:header="851" w:footer="992" w:gutter="0"/>
        <w:cols w:space="720"/>
        <w:docGrid w:type="lines" w:line-pitch="312"/>
      </w:sectPr>
    </wx:sect>
  </w:body>
</w:wordDocument>